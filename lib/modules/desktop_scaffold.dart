import 'package:flutter/material.dart';

import 'package:roca/components/roca_appbar.dart';
import 'package:roca/components/roca_sidebar.dart';

import 'package:roca/constants.dart';

import '../app_data.dart';


class DesktopScaffold extends StatefulWidget {
  final List<Widget>? tabBar;
  final List<Widget>? tabViews;
  final Widget? body;
  final String? title;
  final Widget? floatingActionButton;
  final bool isCoordenador;
  final bool hasSideMenu;
  final bool hasBackArrow;
  final bool appBarLandTheme;
  final Color backgroundColor;
  

  const DesktopScaffold({
    super.key, 
    this.tabBar, 
    this.tabViews, 
    this.body, 
    this.title, 
    this.floatingActionButton, 
    required this.hasSideMenu,
    required this.hasBackArrow,
    required this.appBarLandTheme, 
    required this.isCoordenador,
    required this.backgroundColor, 
  });

  @override
  State<DesktopScaffold> createState() => _DesktopScaffoldState();
}

class _DesktopScaffoldState extends State<DesktopScaffold> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Stack (
      children: [
        Scaffold(
          key: _scaffoldKey,
          backgroundColor: widget.backgroundColor,
          appBar: widget.title != null 
          ? RocaAppBar(
            title: widget.title ?? '',
            isCoordenador: widget.isCoordenador,
            appBarLandTheme: widget.appBarLandTheme,
            leadingIcon: widget.hasBackArrow
            ? IconButton(
              onPressed: () {Navigator.of(context).pop();},
              icon: Icon(
                Icons.arrow_back,
                size: AppTheme.appbarIconSize,
                color: widget.appBarLandTheme ? AppTheme.vanilla : AppTheme.land,
              ),
            )
            : null
          )
          : null,
          body: Row(
            children: [
              if (widget.hasSideMenu)
                Expanded(
                  flex: 2,
                  child: RocaSidebar(isCoordenador: widget.isCoordenador),
                ),
              Expanded(
                flex: 8,
                child: Padding(
                  padding: EdgeInsets.all(MediaQuery.of(context).size.height * 0.05), 
                  child: widget.tabBar != null
                  ? DefaultTabController(
                      length: widget.tabBar!.length,
                      child: Column(
                        children: [
                          TabBar(
                            tabs: widget.tabBar!,
                            labelStyle: const TextStyle(
                              fontSize: 23,
                              color: AppTheme.land,
                            ),
                            indicatorColor: AppTheme.land,
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 6),
                              child:TabBarView(
                                  children: widget.tabViews!,
                                ),
                              ),
                          ),
                        ],
                      ),
                    )
                  : widget.body,
                ),
              ),
            ],
          ),
          floatingActionButton: widget.floatingActionButton,
        ),
        // Indicador de carregamento
        if (DataManager.getIsLoading())
          Container(
            color: Colors.grey.withOpacity(0.2),
            child: Center(
              child: SizedBox(
                width: 80, 
                height: 80, 
                child: CircularProgressIndicator(
                  strokeWidth: 10,
                ),
              )
            ),
          ),
      ],
    );
  }
}
import 'package:flutter/material.dart';
import 'package:roca/app_data.dart';

import 'package:roca/components/roca_appbar.dart';
import 'package:roca/components/roca_sidebar.dart';
import 'package:roca/constants.dart';

class MobileScaffold extends StatefulWidget {
  final double screenHeight;
  final double screenWidth;
  final List<Widget>? tabBar;
  final List<Widget>? tabViews;
  final Widget? body;
  final String? title;
  final Widget? floatingActionButton;
  final bool isCoordenador;
  final bool hasSideMenu;
  final bool hasBackArrow;
  final bool appBarLandTheme;
  final Color backgroundColor;

  const MobileScaffold(
      {required this.screenHeight,
      required this.screenWidth,
      super.key,
      required this.backgroundColor,
      this.tabBar,
      this.tabViews,
      this.body,
      this.title,
      this.floatingActionButton,
      required this.hasBackArrow,
      required this.hasSideMenu,
      required this.appBarLandTheme,
      required this.isCoordenador});

  @override
  State<MobileScaffold> createState() => _MobileScaffoldState();
}

class _MobileScaffoldState extends State<MobileScaffold> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Scaffold(
        key: _scaffoldKey,
        appBar: widget.title != null
            ? RocaAppBar(
                title: widget.title ?? '',
                isCoordenador: widget.isCoordenador,
                appBarLandTheme: widget.appBarLandTheme,
                leadingIcon: widget.hasSideMenu
                    ? IconButton(
                        onPressed: () {
                          _scaffoldKey.currentState?.openDrawer();
                        },
                        icon: Icon(
                          Icons.menu,
                          size: AppTheme.appbarIconSize,
                          color: widget.appBarLandTheme
                              ? AppTheme.vanilla
                              : AppTheme.land,
                        ),
                      )
                    : widget.hasBackArrow
                        ? IconButton(
                            onPressed: () async {
                              if (await DataManager.getHasUnsavedChanges(
                                  context)) {
                                DataManager.setHasUnsavedChanges(false);
                                Navigator.pop(context);
                              }
                            },
                            icon: Icon(
                              Icons.arrow_back,
                              size: AppTheme.appbarIconSize,
                              color: widget.appBarLandTheme
                                  ? AppTheme.vanilla
                                  : AppTheme.land,
                            ),
                          )
                        : Container())
            : null,
        backgroundColor: widget.backgroundColor,
        drawer: widget.hasSideMenu
            ? RocaSidebar(isCoordenador: widget.isCoordenador)
            : null,
        body: widget.tabBar != null
            ? DefaultTabController(
                length: widget.tabBar!.length,
                child: Column(
                  children: [
                    TabBar(
                      tabs: widget.tabBar!,
                      labelStyle: const TextStyle(
                        fontSize: 23,
                        color: AppTheme.land,
                      ),
                      indicatorColor: AppTheme.land,
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 6),
                        child: TabBarView(
                          children: widget.tabViews!,
                        ),
                      ),
                    ),
                  ],
                ),
              )
            : SizedBox(
                height: widget.title == null
                    ? widget.screenHeight
                    : widget.screenHeight -
                        (AppTheme.appbarHeight + AppTheme.appbarPadding),
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: widget.body,
                ),
              ),
        floatingActionButton: widget.floatingActionButton,
      ),

      // Indicador de carregamento
      if (DataManager.getIsLoading())
        Container(
          color: Colors.white.withOpacity(0.7),
          child: Center(
              child: SizedBox(
            width: 80,
            height: 80,
            child: CircularProgressIndicator(
              strokeWidth: 10,
            ),
          )),
        ),
    ]);
  }
}

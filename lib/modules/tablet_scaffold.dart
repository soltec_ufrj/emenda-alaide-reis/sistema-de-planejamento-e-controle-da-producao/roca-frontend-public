import 'package:flutter/material.dart';

import 'package:roca/components/roca_appbar.dart';
import 'package:roca/components/roca_sidebar.dart';

import 'package:roca/constants.dart';

import '../app_data.dart';

class TabletScaffold extends StatefulWidget {
  final List<Widget>? tabBar;
  final List<Widget>? tabViews;
  final Widget? body;
  final String? title;
  final Widget? floatingActionButton;
  final bool isCoordenador;
  final bool hasSideMenu;
  final bool hasBackArrow;
  final bool appBarLandTheme;

  const TabletScaffold(
      {super.key,
      this.tabBar,
      this.tabViews,
      this.body,
      this.title,
      this.floatingActionButton,
      required this.hasSideMenu,
      required this.hasBackArrow,
      required this.appBarLandTheme,
      required this.isCoordenador});

  @override
  State<TabletScaffold> createState() => _TabletScaffoldState();
}

class _TabletScaffoldState extends State<TabletScaffold> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          key: _scaffoldKey,
          appBar: widget.title != null
              ? RocaAppBar(
                  title: widget.title ?? '',
                  isCoordenador: widget.isCoordenador,
                  appBarLandTheme: widget.appBarLandTheme,
                  leadingIcon: widget.hasSideMenu
                      ? IconButton(
                          onPressed: () {
                            _scaffoldKey.currentState?.openDrawer();
                          },
                          icon: Icon(
                            Icons.menu,
                            size: AppTheme.appbarIconSize,
                            color: widget.appBarLandTheme
                                ? AppTheme.vanilla
                                : AppTheme.land,
                          ),
                        )
                      : widget.hasBackArrow
                          ? IconButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              icon: Icon(
                                Icons.arrow_back,
                                size: AppTheme.appbarIconSize,
                                color: widget.appBarLandTheme
                                    ? AppTheme.vanilla
                                    : AppTheme.land,
                              ),
                            )
                          : Container())
              : null,
          drawer: widget.hasSideMenu
              ? RocaSidebar(isCoordenador: widget.isCoordenador)
              : null,
          backgroundColor: widget.hasSideMenu ? Colors.white : AppTheme.vanilla,
          body: widget.body,
          floatingActionButton: widget.floatingActionButton,
        ),

        // Indicador de carregamento
        if (DataManager.getIsLoading())
          Container(
            color: Colors.grey.withOpacity(0.2),
            child: Center(
                child: SizedBox(
              width: 80,
              height: 80,
              child: CircularProgressIndicator(
                strokeWidth: 10,
              ),
            )),
          ),
      ],
    );
  }
}

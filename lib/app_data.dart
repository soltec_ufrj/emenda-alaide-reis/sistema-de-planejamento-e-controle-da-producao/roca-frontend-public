import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:roca/cookie_manager.dart';
import 'package:roca/models/user_model.dart';

class DataManager extends ChangeNotifier {
  static final CookieManager cookieManager = CookieManager(); // Tornando cookieManager um membro de instância
  static PessoaModel? _loggedUser;
  static bool _isLoading = false;
  static bool _isListView = true;
  static bool _hasUnsavedChanges = false;

  static Future<bool> _showBackDialog(context) async {
    if (!_hasUnsavedChanges) return true;

    return await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Tem certeza?'),
          content: Text('Você tem alterações não salvas. Deseja sair sem salvar?'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.of(context).pop(false),
              child: Text('Não'),
            ),
            TextButton(
              onPressed: () => Navigator.of(context).pop(true),
              child: Text('Sim'),
            ),
          ],
        );
      },
    ) ?? false;
  }

  static void setHasUnsavedChanges(bool hasUnsavedChanges) {
    _hasUnsavedChanges = hasUnsavedChanges;
  }

  static Future<bool> getHasUnsavedChanges(context) {
    return _showBackDialog(context);
  }

  static void setIsLoading(bool loadingStatus) {
    _isLoading = loadingStatus;
  }

  static bool getIsLoading() {
    return _isLoading;
  }

  static void setIsListView(bool isListView) {
    _isListView = isListView;
  }

  static bool getIsListView() {
    return _isListView;
  }

  static void setLoggedUser(PessoaModel user) {
    _loggedUser = user;
  }

  static PessoaModel? getLoggedUser() {
    return _loggedUser;
  }

  static void clearLoggedUser() {
    _loggedUser = null;
  }

  static void setCookies(http.Response cookies) {
    cookieManager.setCookies(cookies); 
  }

  static String getCookies() {
    return cookieManager.getCookies();
  }

  static void clearCookies() {
    cookieManager.clearCookies();
  }
}
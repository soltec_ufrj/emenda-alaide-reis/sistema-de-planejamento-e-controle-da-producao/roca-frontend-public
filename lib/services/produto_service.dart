import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:roca/app_data.dart';
import 'package:roca/constants.dart';
import 'package:roca/models/produto_model.dart';


class ProdutoService {

  Future<Map<String, dynamic>> getTipos() async {
    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.tipoProdutoEndpoint);

    try {
      final response = await http
      .get(
        uri,
        headers: {
          "Content-Type": "application/json",
          "cookie": DataManager.getCookies(),
        },
      )
      .timeout(ApiConstants.timeoutDuration);

      
      final responseData = jsonDecode(response.body);

      if (response.statusCode == 200) {
        List<TipoProdutoModel> tipos = [];

        for (var i = 0; i < responseData.length; i++) {
          tipos.add(TipoProdutoModel.fromJson(responseData[i]));
        }

        return {
          'success': true,
          'data': tipos,
        };
      }
      else {
        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }

    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de tipos: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 
  }

  Future<Map<String, dynamic>> getUnidades() async {
    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.unidadeEndpoint);

    try {
      final response = await http
      .get(
        uri,
        headers: {
          "Content-Type": "application/json",
          "cookie": DataManager.getCookies(),
        },
      )
      .timeout(ApiConstants.timeoutDuration);

      
      final responseData = jsonDecode(response.body);

      if (response.statusCode == 200) {
        List<UnidadeModel> unidades = [];

        for (var i = 0; i < responseData.length; i++) {
          unidades.add(UnidadeModel.fromJson(responseData[i]));
        }

        return {
          'success': true,
          'data': unidades,
        };
      }
      else {
        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }

    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de unidades: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 
  }

  Future<Map<String, dynamic>> getProdutos() async {
    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.produtoEndpoint);

    try {
      final response = await http
      .get(
        uri,
        headers: {
          "Content-Type": "application/json",
          "cookie": DataManager.getCookies(),
        },
      )
      .timeout(ApiConstants.timeoutDuration);

      
      final responseData = jsonDecode(response.body);

      if (response.statusCode == 200) {
        List<ProdutoModel> produtos = [];

        for (var i = 0; i < responseData.length; i++) {
          produtos.add(ProdutoModel.fromJson(responseData[i]));
        }

        return {
          'success': true,
          'data': produtos,
        };
      }
      else {
        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }
    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de produtos: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 
  }

  Future<Map<String, dynamic>> postProduto(ProdutoModel produtoData) async {
    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.produtoEndpoint);

    try {
      final response = await http
      .post(
        uri,
        headers: {
          "Content-Type": "application/json",
          "cookie": DataManager.getCookies(),
        },
        body: produtoModelToJson(produtoData),
      )
      .timeout(ApiConstants.timeoutDuration);

      if (response.statusCode == 201) {
        return {
          'success': true,
        };
      }
      else {
        final Map<String, dynamic> responseData = jsonDecode(response.body);

        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }

    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de cadastro: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 
  }

  Future<Map<String, dynamic>> patchProduto(ProdutoModel produtoData) async {

    String produtoId = produtoData.id!;

    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.produtoEndpoint + produtoId);
    final bodyData = produtoModelToJson(produtoData);

    try {
      final response = await http
      .patch(
          uri,
          headers: {
            "Content-Type": "application/json",
            "cookie": DataManager.getCookies(),
          },
          body: bodyData,
      );
      
      if (response.statusCode == 200) {
        return {
          'success': true,
        };
      }
      else {
        final Map<String, dynamic> responseData = jsonDecode(response.body);
        
        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }
    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de edição: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 
  }

  Future<Map<String, dynamic>> deleteProduto(ProdutoModel produtoData) async {
    String produtoId = produtoData.id!;

    final Uri uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.produtoEndpoint + produtoId);

    try {
      final response = await http.delete(
          uri,
          headers: {
            "Content-Type": "application/json",
            "cookie": DataManager.getCookies(),
          },
      );
      
      if (response.statusCode == 200) {
        return {
          'success': true,
        };
      }
      else {
        final Map<String, dynamic> responseData = jsonDecode(response.body);
        
        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }
    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de remoção: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 

  }

}
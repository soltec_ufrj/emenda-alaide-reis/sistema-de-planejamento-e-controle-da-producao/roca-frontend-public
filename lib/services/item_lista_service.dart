import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:roca/app_data.dart';
import 'package:roca/constants.dart';

import 'package:roca/models/familia_model.dart';
import 'package:roca/models/item_lista_model.dart';


class ItemListaService {

  Future<Map<String, dynamic>> getItensLista(String listaID) async {
    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.itemListaEndpoint + listaID);

    try {
      final response = await http
      .get(
        uri,
        headers: {
          "Content-Type": "application/json",
          "cookie": DataManager.getCookies(),
        },
      )
      .timeout(ApiConstants.timeoutDuration);

      
      final responseData = jsonDecode(response.body);

      if (response.statusCode == 200) {
        List<ItemListaModel> itemListas = [];

        for (var i = 0; i < responseData.length; i++) {
          for (var j = 0; j < responseData[i]["produtos"].length; j++) {
            responseData[i]["produtos"][j]["familia"] = FamiliaModel(sobrenome: responseData[i]["familia"]);
            responseData[i]["produtos"][j]["listaId"] = listaID;
            responseData[i]["produtos"][j]["quantidade_disponivel"] = responseData[i]["produtos"][j]["quantidade"];
            itemListas.add(ItemListaModel.fromJson(responseData[i]["produtos"][j]));
          }
        }

        return {
          'success': true,
          'data': itemListas,
        };
      }
      else {
        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }

    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de itens da lista: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 
  }

  Future<Map<String, dynamic>> getItensListaFamilia(String listaID, String familiaSobrenome) async {
    final url = ApiConstants.baseUrl + ApiConstants.itemListaEndpoint;

    final Map<String, dynamic> parametros = { 
      "familia": familiaSobrenome,
      "lista_id": listaID
    };

    try {
      final uri = Uri.parse(url).replace(queryParameters: parametros);
      final response = await http
      .get( 
        uri,
        headers: {
          "Content-Type": "application/json",
          "cookie": DataManager.getCookies(),
        },
      )
      .timeout(ApiConstants.timeoutDuration);

      
      final responseData = jsonDecode(response.body);

      if (response.statusCode == 200) {
        if (responseData is Map && responseData.containsKey("mensagem") && responseData["mensagem"] == "Nenhum produto foi cadastrado para essa lista.") {
          return {
            'success': true,
            'data': [],
          };
        }
        List<ItemListaModel> itemListas = [];

        FamiliaModel familiaModel = FamiliaModel(sobrenome: familiaSobrenome);

        for (var i = 0; i < responseData.length; i++) {
          responseData[i]["familia"] = familiaModel;
          responseData[i]["listaId"] = listaID;
          itemListas.add(ItemListaModel.fromJson(responseData[i]));
        }

        return {
          'success': true,
          'data': itemListas,
        };
      }
      else {
        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }

    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de itens da lista: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 
  }

  Future<Map<String, dynamic>> postItemLista(ItemListaModel itemListaData) async {
    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.itemListaEndpoint);

    try {
      final response = await http
      .post(
        uri,
        headers: {
          "Content-Type": "application/json",
          "cookie": DataManager.getCookies(),
        },
        body: itemListaModelToJson(itemListaData),
      )
      .timeout(ApiConstants.timeoutDuration);

      if (response.statusCode == 201) {
        return {
          'success': true,
        };
      }
      else {
        final Map<String, dynamic> responseData = jsonDecode(response.body);

        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }

    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de adição de item: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 
  }

  Future<Map<String, dynamic>> patchItemLista(ItemListaModel itemListaData) async {
    String itemId = itemListaData.id!;
  
    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.itemListaEndpoint + itemId);
    final data = itemListaModelToJson(itemListaData);

    try {
      final response = await http
      .patch(
          uri,
          headers: {
            "Content-Type": "application/json",
            "cookie": DataManager.getCookies(),
          },
          body: data,
      )
      .timeout(ApiConstants.timeoutDuration);
      
      if (response.statusCode == 200) {
        return {
          'success': true,
        };
      }
      else {
        final Map<String, dynamic> responseData = jsonDecode(response.body);
        
        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }
    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de edição de item: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 

  }

    Future<Map<String, dynamic>> deleteItemLista(String? itemListaID) async {
      final Uri uri;
      if (itemListaID != null)
        uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.itemListaEndpoint + itemListaID);
      else
        return {
          'success': false,
          'message': 'Erro durante a requisição: item sem ID.',
        };

      try {
        final response = await http
        .delete(
            uri,
            headers: {
              "Content-Type": "application/json",
              "cookie": DataManager.getCookies(),
            },
        )
        .timeout(ApiConstants.timeoutDuration);
        
        if (response.statusCode == 200) {
          return {
            'success': true,
          };
        }
        else {
          final Map<String, dynamic> responseData = jsonDecode(response.body);
          
          return {
            'success': false,
            'message': responseData['mensagem'] ?? responseData['erro'],
          };
        }
      } on TimeoutException catch (e) {
        print('Timeout during API call: $e');
        return {
          'success': false,
          'message': ApiConstants.msgErroTimeout,
        };
      } on http.ClientException catch (e) {
        print('ClientException during API call: $e');
        return {
          'success': false,
          'message': ApiConstants.msgErroSemInternet,
        };
      } catch (e) {
        print('Erro durante a requisição de remoção: $e');
        return {
          'success': false,
          'message': ApiConstants.msgErroInesperado,
        };
      } 
    }

    Future<Map<String, dynamic>> getExportProdutos(String listaID) async {
      final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.exportProdutos + listaID);

      try {
        final response = await http
        .get(
          uri,
          headers: {
            "Content-Type": "application/json",
            "cookie": DataManager.getCookies(),
          },
        )
        .timeout(ApiConstants.timeoutDuration);

        
        final responseData = jsonDecode(response.body);

        if (response.statusCode == 200) {
          List<ItemListaModel> itemListas = [];

          for (var i = 0; i < responseData.length; i++) {
            responseData[i]["listaId"] = listaID;
            responseData[i]["quantidade_disponivel"] = responseData[i]["quantidade"];
            itemListas.add(ItemListaModel.fromJson(responseData[i]));
          }

          return {
            'success': true,
            'data': itemListas,
          };
        }
        else {
          return {
            'success': false,
            'message': responseData['mensagem'] ?? responseData['erro'],
          };
        }

      } on TimeoutException catch (e) {
        print('Timeout during API call: $e');
        return {
          'success': false,
          'message': ApiConstants.msgErroTimeout,
        };
      } on http.ClientException catch (e) {
        print('ClientException during API call: $e');
        return {
          'success': false,
          'message': ApiConstants.msgErroSemInternet,
        };
      } catch (e) {
        print('Erro durante a requisição de produtos: $e');
        return {
          'success': false,
          'message': ApiConstants.msgErroInesperado,
        };
      } 
    }

    // não esta sendo usado
    Future<Map<String, dynamic>> getExportFamilias(String listaID) async {
      final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.exportProdutos + listaID);

      try {
        final response = await http
        .get(
          uri,
          headers: {
            "Content-Type": "application/json",
            "cookie": DataManager.getCookies(),
          },
        )
        .timeout(ApiConstants.timeoutDuration);

        
        final responseData = jsonDecode(response.body);

        if (response.statusCode == 200) {
          List<ItemListaModel> itemListas = [];

          for (var i = 0; i < responseData.length; i++) {
            responseData[i]["listaId"] = listaID;
            responseData[i]["quantidade_disponivel"] = responseData[i]["quantidade"];
            itemListas.add(ItemListaModel.fromJson(responseData[i]));
          }

          return {
            'success': true,
            'data': itemListas,
          };
        }
        else {
          return {
            'success': false,
            'message': responseData['mensagem'] ?? responseData['erro'],
          };
        }

      } on TimeoutException catch (e) {
        print('Timeout during API call: $e');
        return {
          'success': false,
          'message': ApiConstants.msgErroTimeout,
        };
      } on http.ClientException catch (e) {
        print('ClientException during API call: $e');
        return {
          'success': false,
          'message': ApiConstants.msgErroSemInternet,
        };
      } catch (e) {
        print('Erro durante a requisição de produtos: $e');
        return {
          'success': false,
          'message': ApiConstants.msgErroInesperado,
        };
      } 
    }


}
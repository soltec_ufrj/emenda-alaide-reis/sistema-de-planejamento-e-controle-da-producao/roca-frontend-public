import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:roca/app_data.dart';
import 'package:roca/constants.dart';
import 'package:roca/models/familia_model.dart';


class FamiliaService {

  Future<Map<String, dynamic>> getFamilias() async {
    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.familiaEndpoint);

    try {
      final response = await http
      .get(
        uri,
        headers: {
          "Content-Type": "application/json",
          "cookie": DataManager.getCookies(),
        },
      )
      .timeout(ApiConstants.timeoutDuration);

      
      final responseData = jsonDecode(response.body);

      if (response.statusCode == 200) {
        List<FamiliaModel> familias = [];

        for (var i = 0; i < responseData.length; i++) {
          familias.add(FamiliaModel.fromJson(responseData[i]));
        }

        return {
          'success': true,
          'data': familias,
        };
      }
      else {
        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }

    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de famílias: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 
  }

  Future<Map<String, dynamic>> postFamilia(FamiliaModel familiaData) async {
    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.familiaEndpoint);

    try {
      final response = await http
      .post(
        uri,
        headers: {
          "Content-Type": "application/json",
          "cookie": DataManager.getCookies(),
        },
        body: familiaModelToJson(familiaData),
      )
      .timeout(ApiConstants.timeoutDuration);

      if (response.statusCode == 201) {
        return {
          'success': true,
        };
      }
      else {
        final Map<String, dynamic> responseData = jsonDecode(response.body);

        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }

    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de cadastro: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 
  }

  Future<Map<String, dynamic>> patchFamilia(FamiliaModel familiaData) async {

    String familiaSobrenome = familiaData.sobrenome;

    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.familiaEndpoint + familiaSobrenome);
    final data = familiaModelToJson(familiaData);

    try {
      final response = await http
      .patch(
          uri,
          headers: {
            "Content-Type": "application/json",
            "cookie": DataManager.getCookies(),
          },
          body: data,
      )
      .timeout(ApiConstants.timeoutDuration);
      
      if (response.statusCode == 200) {
        return {
          'success': true,
        };
      }
      else {
        final Map<String, dynamic> responseData = jsonDecode(response.body);
        
        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }
    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de edição: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 
  }

  Future<Map<String, dynamic>> deleteFamilia(String familiaSobrenome) async {
    final Uri uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.familiaEndpoint + familiaSobrenome);

    try {
      final response = await http
      .delete(
          uri,
          headers: {
            "Content-Type": "application/json",
            "cookie": DataManager.getCookies(),
          },
      )
      .timeout(ApiConstants.timeoutDuration);
      
      if (response.statusCode == 200) {
        return {
          'success': true,
        };
      }
      else {
        final Map<String, dynamic> responseData = jsonDecode(response.body);
        
        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }
    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de remoção: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 
  }

}
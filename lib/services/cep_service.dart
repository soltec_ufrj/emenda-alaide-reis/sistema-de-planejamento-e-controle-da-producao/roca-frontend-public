import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:roca/app_data.dart';
import 'package:roca/constants.dart';
import 'package:roca/models/assentamento_model.dart';


class CepService {

  Future<Map<String, dynamic>> getEstados() async {
    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.estadoEndpoint);

    try {
      final response = await http
      .get(
        uri,
        headers: {
          "Content-Type": "application/json",
          "cookie": DataManager.getCookies(),
        },
      )
      .timeout(ApiConstants.timeoutDuration);

      
      final responseData = jsonDecode(response.body);

      if (response.statusCode == 200) {
        List<EstadoModel> estados = [];

        for (var i = 0; i < responseData.length; i++) {
          estados.add(EstadoModel.fromJson(responseData[i]));
        }

        return {
          'success': true,
          'data': estados,
        };
      }
      else {
        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }

    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de estados: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    }
  }

  Future<Map<String, dynamic>> getCidadesPorEstado(String estado) async {
    final uri = Uri.parse('${ApiConstants.baseUrl}${ApiConstants.cepEndpoint}estado/$estado');

    try {
      final response = await http
      .get(
        uri,
        headers: {
          "Content-Type": "application/json",
          "cookie": DataManager.getCookies(),
        },
      )
      .timeout(ApiConstants.timeoutDuration);

      final responseData = jsonDecode(response.body);

      if (response.statusCode == 200) {
        List<CepModel> cidades = [];

        for (var i = 0; i < responseData.length; i++) {
          cidades.add(CepModel.fromJson(responseData[i]));
        }

        return {
          'success': true,
          'data': cidades,
        };
      }
      else {
        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }

    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de cidades: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 
  }
}
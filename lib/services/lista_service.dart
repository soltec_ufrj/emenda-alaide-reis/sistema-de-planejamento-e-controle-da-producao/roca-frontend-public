import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:roca/app_data.dart';
import 'package:roca/constants.dart';

import 'package:roca/models/lista_model.dart';


class ListaService {

  Future<Map<String, dynamic>> getListas() async {
    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.listaEndpoint);

    try {
      final response = await http
      .get(
        uri,
        headers: {
          "Content-Type": "application/json",
          "cookie": DataManager.getCookies(),
        },
      )
      .timeout(ApiConstants.timeoutDuration);

      
      final responseData = jsonDecode(response.body);

      if (response.statusCode == 200) {
        List<ListaModel> listas = [];

        for (var i = 0; i < responseData.length; i++) {
          listas.add(ListaModel.fromJson(responseData[i]));
        }

        return {
          'success': true,
          'data': listas,
        };
      }
      else {
        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }

    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de listas: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 
  }

  Future<Map<String, dynamic>> postLista(ListaModel listaData) async {
    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.listaEndpoint);

    try {
      final response = await http
      .post(
        uri,
        headers: {
          "Content-Type": "application/json",
          "cookie": DataManager.getCookies(),
        },
        body: listaModelToJson(listaData),
      )
      .timeout(ApiConstants.timeoutDuration);

      if (response.statusCode == 201) {
        return {
          'success': true,
        };
      }
      else {
        final Map<String, dynamic> responseData = jsonDecode(response.body);

        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }

    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de cadastro: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 
  }

  Future<Map<String, dynamic>> patchLista(ListaModel listaData) async {

    String listaId;
    if (listaData.id != null) {
      listaId = listaData.id!;
    }
    else {
      return {
          'success': false,
          'message': 'Lista sem ID.',
      };
    }

    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.listaEndpoint + listaId);
    final data = listaModelToJson(listaData);

    try {
      final response = await http
      .patch(
          uri,
          headers: {
            "Content-Type": "application/json",
            "cookie": DataManager.getCookies(),
          },
          body: data,
      )
      .timeout(ApiConstants.timeoutDuration);
      
      if (response.statusCode == 200) {
        return {
          'success': true,
        };
      }
      else {
        final Map<String, dynamic> responseData = jsonDecode(response.body);

        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }
    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de edição: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 

  }

}
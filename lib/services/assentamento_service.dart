import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:roca/app_data.dart';
import 'package:roca/constants.dart';
import 'package:roca/models/assentamento_model.dart';


class AssentamentoService {

  Future<Map<String, dynamic>> getAssentamentos() async {
    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.assentamentoEndpoint);

    try {
      final response = await http
      .get(
        uri,
        headers: {
          "Content-Type": "application/json",
          "cookie": DataManager.getCookies(),
        },
      )
      .timeout(ApiConstants.timeoutDuration);

      
      final responseData = jsonDecode(response.body);

      if (response.statusCode == 200) {
        List<AssentamentoModel> assentamentos = [];

        for (var i = 0; i < responseData.length; i++) {
          assentamentos.add(AssentamentoModel.fromJson(responseData[i]));
        }

        return {
          'success': true,
          'data': assentamentos,
        };
      }
      else {
        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }

    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de assentamentos: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 
  }

  Future<Map<String, dynamic>> postAssentamento(AssentamentoModel assentamentoData) async {
    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.assentamentoEndpoint);

    try {
      final response = await http
      .post(
        uri,
        headers: {
          "Content-Type": "application/json",
          "cookie": DataManager.getCookies(),
        },
        body: assentamentoModelToJson(assentamentoData),
      )
      .timeout(ApiConstants.timeoutDuration);

      if (response.statusCode == 201) {
        return {
          'success': true,
        };
      }
      else {
        final Map<String, dynamic> responseData = jsonDecode(response.body);

        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }

    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de cadastro: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 
  }

  Future<Map<String, dynamic>> patchAssentamento(AssentamentoModel assentamentoData) async {

    String assentamentoNome = assentamentoData.nome;

    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.assentamentoEndpoint + assentamentoNome);
    final bodyData = assentamentoModelToJson(assentamentoData);

    try {
      final response = await http
      .patch(
          uri,
          headers: {
            "Content-Type": "application/json",
            "cookie": DataManager.getCookies(),
          },
          body: bodyData,
      )
      .timeout(ApiConstants.timeoutDuration);
      
      if (response.statusCode == 200) {
        return {
          'success': true,
        };
      }
      else {
        final Map<String, dynamic> responseData = jsonDecode(response.body);

        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }
    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de edição: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    }
  }


}
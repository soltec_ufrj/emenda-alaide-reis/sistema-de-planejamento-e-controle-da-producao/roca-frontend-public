import 'dart:convert';
import 'dart:async';

import 'package:http/http.dart' as http;
import 'package:roca/app_data.dart';
import 'package:roca/constants.dart';

import 'package:roca/models/user_model.dart';


class UserService {

  Future<Map<String, dynamic>> logout(UsuarioModel logoutData) async {
    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.logoutEndpoint);

    try {
      final response = await http
      .delete(
        uri,
        headers: {
          "Content-Type": "application/json", 
          "cookie": DataManager.getCookies(),
        },
        body: usuarioModelToJson(logoutData),
      )
      .timeout(ApiConstants.timeoutDuration);

      if (response.statusCode == 200) {
        // Limpar informações de sessão localmente
        DataManager.clearCookies();
        return {
          'success': true,
        };
      }
      else {
        final Map<String, dynamic> responseData = jsonDecode(response.body);

        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }

    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de logout: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 
  }

  Future<Map<String, dynamic>> login(UsuarioModel loginData) async {
    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.loginEndpoint);

    try {
      final response = await http
      .post(
        uri,
        headers: {"Content-Type": "application/json"},
        body: usuarioModelToJson(loginData),
      )
      .timeout(ApiConstants.timeoutDuration);

      final Map<String, dynamic> responseData = jsonDecode(response.body);

      if (response.statusCode == 200) {
        final pessoaData = responseData["pessoa"];
        DataManager.setCookies(response);

        return {
          'success': true,
          'message': responseData['mensagem'],
          'pessoa': PessoaModel.fromJson(pessoaData),
        };
      } else {
        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }
    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de login: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    }
  }


  Future<Map<String, dynamic>> signUp(PessoaModel userData) async {
    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.signUpEndpoint);

    try {
      final response = await http
      .post(
        uri,
        headers: {
          "Content-Type": "application/json",
        },
        body: pessoaModelToJson(userData),
      )
      .timeout(ApiConstants.timeoutDuration);

      if (response.statusCode == 200 || response.statusCode == 201) {
        return {
          'success': true,
        };
      }
      else {
        final Map<String, dynamic> responseData = jsonDecode(response.body);

        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }

    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de cadastro: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 
  }

  Future<Map<String, dynamic>> getUsers() async {
    var uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.userEndpoint);

    try {
      var response = await http
      .get(
        uri,
        headers: {
          "Content-Type": "application/json",
          "cookie": DataManager.getCookies(),
        },
      )
      .timeout(ApiConstants.timeoutDuration);

      
      final usersResponseData = jsonDecode(response.body);



      if (response.statusCode == 200) {
        List<PessoaModel> users = [];

        for (var i = 0; i < usersResponseData.length; i++) {
          uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.userEndpoint + usersResponseData[i]);

          response = await http.get(
            uri,
            headers: {
              "Content-Type": "application/json",
              "cookie": DataManager.getCookies(),
            },
          );

          final responseData = jsonDecode(response.body);

          if (response.statusCode == 200) {
            users.add(PessoaModel.fromJson(responseData));  
          }
          else {
            return {
              'success': false,
              'message': responseData['mensagem'] ?? responseData['erro'],
            };
          }
        }

        return {
          'success': true,
          'data': users,
        };
      }
      else {
        return {
          'success': false,
          'message': usersResponseData['mensagem'] ?? usersResponseData['erro'],
        };
      }
    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('rro durante a requisição de usuarios: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 
  }

  Future<Map<String, dynamic>> postUser(PessoaModel userData) async {
    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.userEndpoint);

    try {
      final response = await http
      .post(
        uri,
        headers: {
          "Content-Type": "application/json",
          "cookie": DataManager.getCookies(),
        },
        body: pessoaModelToJson(userData),
      )
      .timeout(ApiConstants.timeoutDuration);

      if (response.statusCode == 201) {
        return {
          'success': true,
        };
      }
      else {
        final Map<String, dynamic> responseData = jsonDecode(response.body);

        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }

    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de cadastro: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 
  }

  Future<Map<String, dynamic>> patchUser(PessoaModel userData) async {

    String userCpf = userData.usuario.cpf;

    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.userEndpoint + userCpf);

    final bodyData = pessoaModelToJson(userData);

    try {
      final response = await http
      .patch(
          uri,
          headers: {
            "Content-Type": "application/json",
            "cookie": DataManager.getCookies(),
          },
          body: bodyData,
      )
      .timeout(ApiConstants.timeoutDuration);
      
      if (response.statusCode == 200) {
        return {
          'success': true,
        };
      }
      else {
        final Map<String, dynamic> responseData = jsonDecode(response.body);

        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }
    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de edição: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 

  }

  Future<Map<String, dynamic>> deleteUser(String userCPF) async {
    final Uri uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.userEndpoint + userCPF);

    try {
      final response = await http
      .delete(
          uri,
          headers: {
            "Content-Type": "application/json",
            "cookie": DataManager.getCookies(),
          },
      )
      .timeout(ApiConstants.timeoutDuration);
      
      if (response.statusCode == 200) {
        return {
          'success': true,
        };
      }
      else {
        final Map<String, dynamic> responseData = jsonDecode(response.body);
        
        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }
    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de remoção: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    } 

  }

  Future<Map<String, dynamic>> changePassword(UsuarioModel user) async {
    final uri = Uri.parse(ApiConstants.baseUrl + ApiConstants.changePasswordEndpoint);

    try {
      final response = await http
      .post(
        uri,
        headers: {
          "Content-Type": "application/json",
          "cookie": DataManager.getCookies(),
        },
        body: usuarioModelToJson(user),
      )
      .timeout(ApiConstants.timeoutDuration);

      if (response.statusCode == 200) {
        return {
          'success': true,
        };
      }
      else {
        final Map<String, dynamic> responseData = jsonDecode(response.body);

        return {
          'success': false,
          'message': responseData['mensagem'] ?? responseData['erro'],
        };
      }

    } on TimeoutException catch (e) {
      print('Timeout during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroTimeout,
      };
    } on http.ClientException catch (e) {
      print('ClientException during API call: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroSemInternet,
      };
    } catch (e) {
      print('Erro durante a requisição de mudança de senha: $e');
      return {
        'success': false,
        'message': ApiConstants.msgErroInesperado,
      };
    }
  }

}
import 'package:flutter/material.dart';

import '../modules/mobile_scaffold.dart';
import '../modules/tablet_scaffold.dart';
import '../modules/desktop_scaffold.dart';
import 'app_data.dart';
import 'constants.dart';


class Responsive extends StatelessWidget {
  final List<Widget>? tabBar;
  final List<Widget>? tabViews;
  final String? title;
  final Widget? body;
  final Widget? floatingActionButton;
  final bool isCoordenador;
  final bool hasSideMenu;
  final bool hasBackArrow;
  final bool appBarLandTheme;
  final Color backgroundColor;


  const Responsive({
    super.key,
    this.tabBar,
    this.tabViews,
    this.title,
    this.body,
    this.floatingActionButton,
    this.isCoordenador = true,
    this.hasSideMenu = true,
    this.hasBackArrow = true,
    this.appBarLandTheme = true,
    this.backgroundColor = Colors.white,
  });

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        // Calcula antes de sair da root do builder
        final mediaQuery = MediaQuery.of(context);
        final screenHeight = mediaQuery.size.height;
        final screenWidth = mediaQuery.size.width;
        final paddingTop = mediaQuery.padding.top;
        final paddingBottom = mediaQuery.padding.bottom;
        final availableHeight = screenHeight - paddingTop - paddingBottom;

        final deviceType = getDeviceType(mediaQuery);


        switch (deviceType) {
          case DeviceType.mobile:
            return PopScope(
              canPop: false,
              onPopInvoked: ((didpop) async {
                if (didpop) {
                  return;
                }
                if (await DataManager.getIsLoading()) {
                  return;
                }
                if (await DataManager.getHasUnsavedChanges(context)) {
                  DataManager.setHasUnsavedChanges(false);
                  Navigator.pop(context);
                }
              }),
              child: SafeArea(
                child: MobileScaffold(
                  screenHeight: availableHeight,
                  screenWidth: screenWidth,
                  backgroundColor: backgroundColor,
                  tabBar: tabBar,
                  tabViews: tabViews,
                  title: title,
                  body: body,
                  floatingActionButton: floatingActionButton,
                  hasSideMenu: hasSideMenu,
                  hasBackArrow: hasBackArrow,
                  appBarLandTheme: appBarLandTheme,
                  isCoordenador: isCoordenador,
                ),
              ),
            );
          case DeviceType.tablet:
            return SafeArea(
              child: TabletScaffold(
                tabBar: tabBar,
                tabViews: tabViews,
                title: title,
                body: body,
                floatingActionButton: floatingActionButton,
                hasSideMenu: hasSideMenu,
                hasBackArrow: hasBackArrow,
                appBarLandTheme: appBarLandTheme,
                isCoordenador: isCoordenador,
              ),
            );
          case DeviceType.desktop:
            return SafeArea(
              child: DesktopScaffold(
                tabBar: tabBar,
                tabViews: tabViews,
                backgroundColor: backgroundColor,
                title: title,
                body: body,
                floatingActionButton: floatingActionButton,
                hasSideMenu: hasSideMenu,
                hasBackArrow: hasBackArrow,
                appBarLandTheme: appBarLandTheme,
                isCoordenador: isCoordenador,
              ),
            );
          default:
            return Container(); // This shouldn't be reached
        }
      },
    );
  }
}
import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:package_info_plus/package_info_plus.dart';


import '/constants.dart';
import '../responsive.dart';
import '../components/roca_button.dart';

import './login_screen.dart';
import 'signup_screen.dart';


class HomepageScreen extends StatefulWidget {
  static const routeName = '/';

  const HomepageScreen({super.key});

  @override
  _HomepageScreenState createState() => _HomepageScreenState();
}

class _HomepageScreenState extends State<HomepageScreen> {
  late Future<PackageInfo> _packageInfo;

  @override
  void initState() {
    super.initState();
    _packageInfo = PackageInfo.fromPlatform();
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Center(
      child: SingleChildScrollView(
        child: Column(
          children: [
            // Header
            AutoSizeText(
              maxLines: 1, 
              wrapWords: false,
              "Bem vindo à Roça", 
              style: Theme.of(context).textTheme.displaySmall!.land,
            ),

            // Logo
            Image(
              image: AssetImage('assets/img/logo.png', ),
              height: MediaQuery.of(context).size.height * 0.45,
            ),

            // Botões
            RocaButton(
              text: 'Criar uma Conta',
              outline: true,
              buttonColor: AppTheme.land,
              textStyle: Theme.of(context).textTheme.headlineMedium!.land,
              onTap: () {Navigator.of(context).pushNamed(SignupScreen.routeName);}
            ),

            // Log in
            RocaButton(
              text: 'Entrar na Conta',
              outline: false,
              buttonColor: AppTheme.land,
              textStyle: Theme.of(context).textTheme.headlineMedium!.vanilla,
              onTap: () {Navigator.of(context).pushNamed(LoginScreen.routeName);}
            ),

            // Info da Versão
            FutureBuilder<PackageInfo>(
              future: _packageInfo,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasData) {
                    final version = snapshot.data!.version;
                    return Text(
                      'Versão $version',
                      style: Theme.of(context).textTheme.titleLarge!.land,
                    );
                  } else {
                    return Text(
                      'Falha ao obter a versão',
                      style: Theme.of(context).textTheme.titleLarge!.land,
                    );
                  }
                } else {
                  return CircularProgressIndicator();
                }
              },
            ),
          ],
        )
      ),
    );
    
    return Responsive(
      body: body,
      backgroundColor: AppTheme.vanilla,
      hasSideMenu: false,
    );
  }
}

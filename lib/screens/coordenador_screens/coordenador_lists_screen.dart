import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../app_data.dart';
import '../../constants.dart';
import '../../responsive.dart';
import '../../components/roca_radio_button.dart';
import '../../components/roca_list.dart';
import '../../components/roca_grid.dart';

import '../../models/lista_model.dart';
import '../../controllers/lista_controller.dart';
import '../../services/lista_service.dart';

import '../../screens/add_screens/add_list_screen.dart';
import '../../screens/coordenador_screens/coordenador_list_interactions_screen.dart';


class CoordenadorListsScreen extends StatefulWidget {
  static const routeName = '/coordenador/listas';
  static const title = 'Listas';

  const CoordenadorListsScreen({super.key});

  @override
  CoordenadorListsScreenState createState() => CoordenadorListsScreenState();

}

class CoordenadorListsScreenState extends State<CoordenadorListsScreen> {
  bool hasImage = false;
  final listaController = ListaController(ListaService());
  late Future<List<ListaModel>> _futureLists;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    _futureLists = _fetchLists();
  }

  Future<List<ListaModel>> _fetchLists() async {
    setState(() {
      _isLoading = true;
    });

    try {
      final Map<String, dynamic> responseData = await listaController.getListas();
      if (responseData["success"]) {
        return responseData["data"];
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }

      return [];

    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }

  Widget _buildListView(List<ListaModel> listas) {
    return ListView.separated(
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(height: AppTheme.listItemSpacing);
      },
      scrollDirection: Axis.vertical,
      itemCount: listas.length,
      itemBuilder: (context, index) {
        final lista = listas[index];
        final String formattedFim = DateFormat('dd/MM/yy').format(lista.dataFim);
        bool isOpen = lista.isOpen();

        return GestureDetector(
          onTap: () async {
            await Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ListInteractionsScreen(lista: lista)),
            );

            setState(() {
              _futureLists = _fetchLists();
            });
          },
          child: RocaListItem(
            thumbnail: null,
            title: lista.nome,
            subtitle: 'Prazo: $formattedFim',
            color: isOpen ? 'green' : 'gray',
            hasImage: hasImage,
            onlyVisible: false,
          ),
        );
      },
    );
  }

  Widget _buildGridView(List<ListaModel> listas) {
    return LayoutBuilder(builder: (context, constraints) {
      final crossAxisCount = constraints.maxWidth > 700 ? 4 : 2;
      return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: crossAxisCount,
          mainAxisSpacing: 8.0,
          crossAxisSpacing: 8.0,
          childAspectRatio: 6 / 7,
        ),
        itemCount: listas.length,
        itemBuilder: (context, index) {
          final lista = listas[index];
          final String formattedFim = DateFormat('dd/MM/yy').format(lista.dataFim);
          bool isOpen = lista.isOpen();

          return GestureDetector(
            onTap: () async {
              await Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ListInteractionsScreen(lista: lista)),
              );

              setState(() {
                _futureLists = _fetchLists();
              });
            },
            child: RocaGridItem(
              thumbnail: null,
              title: lista.nome,
              subtitle: 'Prazo: $formattedFim',
              color: isOpen ? 'green' : 'gray',
              hasImage: hasImage,
              onlyVisible: false,
            )
          );
        },
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Column(
      children: [
        // View Selector
        Row(
          children: [
            const Spacer(), // Faz o seletor ficar na esquerda da tela
            CustomRadioButton(
              icon:Icons.list,
              value: DataManager.getIsListView(),
              onChanged: (newValue) {
                if (!DataManager.getIsListView()) {
                  setState(() {
                    DataManager.setIsListView(newValue);
                  });
                }
              },
            ),
            CustomRadioButton(
              icon:Icons.grid_view,
              value: !DataManager.getIsListView(),
              onChanged: (newValue) {
                if (DataManager.getIsListView()) {
                  setState(() {
                    DataManager.setIsListView(!newValue);
                  });
                }
              },
            ),
          ],
        ),
        
        // Listas
        Expanded(
          child: FutureBuilder<List<ListaModel>>(
            future: _futureLists,
            builder:  (context, snapshot) {
              if (_isLoading) {
                return const Center(
                  child: CircularProgressIndicator()
                );
              } 
              else if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasData && snapshot.data!.isNotEmpty) {
                  final listas = snapshot.data!;
                  return DataManager.getIsListView() ? _buildListView(listas) : _buildGridView(listas);
                }
                else {
                  return const Text("Nenhuma lista.");
                }
              } else {
                return Container();
              }
            },
          )
        ),
      ],
    );

    Widget floatingActionButton = FloatingActionButton(
      shape: const CircleBorder(),
      backgroundColor: AppTheme.land,
      child: const Icon(Icons.add_sharp, color: AppTheme.vanilla, size: 40),
      onPressed: () async {
        final result = await Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => AddListScreen(hasImage: hasImage)),
        );
                    
        if (result?['changesSaved'] == true) {
          DataManager.setHasUnsavedChanges(false);
        }
      },
    );


    return Responsive(
      title: CoordenadorListsScreen.title,
      body: body,
      floatingActionButton: floatingActionButton,
    );
  }
}
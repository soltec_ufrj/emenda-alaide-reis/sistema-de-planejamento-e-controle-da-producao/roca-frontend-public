
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../constants.dart';
import '../../responsive.dart';
import '../../components/roca_list.dart';


import '../../models/lista_model.dart';
import '../../models/familia_model.dart';
import '../../controllers/familia_controller.dart';
import '../../services/familia_service.dart';

import '../../screens/coordenador_screens/coordenador_family_products_list_screen.dart';


class CoordenadorFamilyListScreen extends StatefulWidget {
  static const routeName = '/coordenador/lista/familias';
  static const title = 'Gerenciar Lista';

  final ListaModel lista;

  const CoordenadorFamilyListScreen({
    super.key,
    required this.lista,
  });

  @override
  CoordenadorFamilyListScreenState createState() => CoordenadorFamilyListScreenState();

}

class CoordenadorFamilyListScreenState extends State<CoordenadorFamilyListScreen> {
  bool hasImage = false;
  final familiaController = FamiliaController(FamiliaService());

  Future<List<FamiliaModel>> _fetchFamilias() async {

    final Map<String, dynamic> responseData = await familiaController.getFamilias();
    
    if (responseData["success"]) {
      return responseData["data"];
    }
    else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            responseData["message"], 
            style: Theme.of(context).textTheme.headlineSmall!.white,
            textAlign: TextAlign.center,
          ),
          duration: AppTheme.messageDuration,
          backgroundColor: AppTheme.danger,
        ),
      );
    }

    return [];
  }

  Widget _buildListView(List<FamiliaModel> familias) {
    return ListView.separated(
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(height: AppTheme.listItemSpacing);
      },
      scrollDirection: Axis.vertical,
      itemCount: familias.length,
      itemBuilder: (context, index) {
        final familia = familias[index];

        return GestureDetector(
          onTap: () async {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => CoordenadorListScreen(lista: widget.lista, familia: familia)),
            ).then((value)=> setState((){}));
          },
          child: RocaListItem(
            thumbnail: null,
            title: familia.sobrenome,
            subtitle: familia.assentamento?.nome ?? 'Sem Assentamento',
            color: 'orange',
            hasImage: hasImage,
            onlyVisible: false,
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Column(
      children: [
        // Info da Lista
        Padding(
          padding: EdgeInsets.symmetric(vertical: 15), 
          child: Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                color: AppTheme.land,
                width: 5,
              ),
              borderRadius: BorderRadius.circular(AppTheme.buttonBorderRadius),
            ),
            padding: const EdgeInsets.all(8.0),
            child: Text(
              '${widget.lista.nome}\n(${DateFormat('dd/MM/yy').format(widget.lista.dataInicio)} - ${DateFormat('dd/MM/yy').format(widget.lista.dataFim)})',
              style: Theme.of(context).textTheme.headlineSmall!.land,
              textAlign: TextAlign.center,
            ),
          ),
        ),

        Expanded(
          child: FutureBuilder<List<FamiliaModel>>(
            future: _fetchFamilias(),
            builder:  (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(
                  child: CircularProgressIndicator()
                );
              }
              else if (snapshot.hasData && snapshot.data!.isNotEmpty) {
                final familias = snapshot.data!;
                return _buildListView(familias);
              }
              else {
                return const Text("Nenhuma família.");
              }
            },
          )
        ),
      ],
    );


    return Responsive(
      title: CoordenadorFamilyListScreen.title,
      body: body,
      hasSideMenu: false,
    );
  }
}
import 'package:flutter/material.dart';

import '../../app_data.dart';
import '../../responsive.dart';
import '../../constants.dart';
import '../../components/roca_radio_button.dart';
import '../../components/roca_list.dart';
import '../../components/roca_grid.dart';

import '../../controllers/user_controller.dart';
import '../../models/user_model.dart';
import '../../services/user_service.dart';

import '../../screens/add_screens/add_user_screen.dart';


class CoordenadorUsersScreen extends StatefulWidget {
  static const routeName = '/coordenador_users';
  static const title = 'Usuários';

  const CoordenadorUsersScreen({super.key});

  @override
  CoordenadorUsersScreenState createState() => CoordenadorUsersScreenState();

}

class CoordenadorUsersScreenState extends State<CoordenadorUsersScreen> {
  bool hasImage = false;
  final userController = UserController(UserService());
  late Future<List<PessoaModel>> _futureUsers;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    _futureUsers = _fetchUsers();
  }

  Future<List<PessoaModel>> _fetchUsers() async {
    setState(() {
      _isLoading = true;
    });

    try {
      final Map<String, dynamic> responseData = await userController.getUsers();

      if (responseData["success"]) {
        final List<PessoaModel> users = responseData["data"];

        // Se usuário não tem família vem primeiro, o resto ordena em ordem alfabética de nome
        users.sort((a, b) {
          if (a.familia?.sobrenome == null && b.familia?.sobrenome != null) {
            return -1;
          } else if (a.familia?.sobrenome != null && b.familia?.sobrenome == null) {
            return 1;
          } else {
            return a.nome.compareTo(b.nome);
          }
        });
        return users;
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }

      return [];
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }



  Widget _buildListView(List<PessoaModel> users) {
    return ListView.separated(
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(height: AppTheme.listItemSpacing);
      },
      scrollDirection: Axis.vertical,
      itemCount: users.length,
      itemBuilder: (context, index) {
        final user = users[index];

        return GestureDetector(
          onTap: () async {
            final result = await Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => AddUserScreen(hasImage: hasImage, edit: true, user: user)
              ),
            );

            if (result == true) {
              DataManager.setHasUnsavedChanges(false);
              _futureUsers = _fetchUsers();
            }
          },
          child: RocaListItem(
            thumbnail: null,
            title: user.nome,
            subtitle: user.familia?.sobrenome ?? 'Sem família',
            color: user.familia?.sobrenome != null ? 'green' : 'orange',
            hasImage: hasImage,
            onlyVisible: false,
          ),
        );
      },
    );
  }

  Widget _buildGridView(List<PessoaModel> users) {
    return LayoutBuilder(builder: (context, constraints) {
      final crossAxisCount = constraints.maxWidth > 700 ? 4 : 2;
      return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: crossAxisCount,
          mainAxisSpacing: 8.0,
          crossAxisSpacing: 8.0,
          childAspectRatio: 6 / 7,
        ),
        itemCount: users.length,
        itemBuilder: (context, index) {
          final user = users[index];

          return GestureDetector(
            onTap: () async {
              final result = await Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AddUserScreen(hasImage: hasImage, edit: true, user: user)),
              );

              if (result == true) {
                DataManager.setHasUnsavedChanges(false);
                _futureUsers = _fetchUsers();
              }

            },
            child: RocaGridItem(
              thumbnail: null,
              title: user.nome,
              subtitle: user.familia?.sobrenome ?? 'Sem família',
              color: user.familia?.sobrenome != null ? 'green' : 'orange',
              hasImage: hasImage,
              onlyVisible: false,
            )
          );
        },
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Column(
      children: [
        Row(
          children: [
            const Spacer(),
            CustomRadioButton(
              icon:Icons.list,
              value: DataManager.getIsListView(),
              onChanged: (newValue) {
                if (!DataManager.getIsListView()) {
                  setState(() {
                    DataManager.setIsListView(newValue);
                  });
                }
              },
            ),
            CustomRadioButton(
              icon:Icons.grid_view,
              value: !DataManager.getIsListView(),
              onChanged: (newValue) {
                if (DataManager.getIsListView()) {
                  setState(() {
                    DataManager.setIsListView(!newValue);
                  });
                }
              },
            ),
          ],
        ),

        Expanded(
          child: FutureBuilder<List<PessoaModel>>(
            future: _futureUsers,
            builder: (context, snapshot) {
              if (_isLoading) {
                return const Center(
                  child: CircularProgressIndicator()
                );
              } 
              else if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasData && snapshot.data!.isNotEmpty) {
                  final users = snapshot.data!;
                  return DataManager.getIsListView() ? _buildListView(users) : _buildGridView(users);
                } else {
                  return const Center(child: Text("Nenhum usuário."));
                }
              } else {
                return Container();
              }
            },
          ),
        ),
      ],
    );

    Widget floatingActionButton = FloatingActionButton(
      shape: const CircleBorder(),
      backgroundColor: AppTheme.land,
      child: const Icon(Icons.add_sharp, color: AppTheme.vanilla, size: 40),
      onPressed: () async {
        final result = await Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => AddUserScreen(hasImage: hasImage)),
        );

        if (result == true) {
          DataManager.setHasUnsavedChanges(false);
          _futureUsers = _fetchUsers();
        }
      },
    );


    return Responsive(
      title: CoordenadorUsersScreen.title,
      body: body,
      floatingActionButton: floatingActionButton,
    );
  }
}
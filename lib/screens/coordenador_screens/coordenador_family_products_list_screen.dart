import 'package:flutter/material.dart';
import 'package:intl/intl.dart';


import '../../app_data.dart';
import '../../constants.dart';
import '../../responsive.dart';
import '../../components/roca_list.dart';

import '../../models/lista_model.dart';
import '../../models/familia_model.dart';
import '../../models/item_lista_model.dart';
import '../../controllers/item_lista_controller.dart';
import '../../services/item_lista_service.dart';

import '../../screens/add_screens/add_list_product_screen.dart';


class CoordenadorListScreen extends StatefulWidget {
  static const routeName = '/coordenador_list';
  final ListaModel lista;
  final FamiliaModel familia;

  const CoordenadorListScreen({
    super.key,
    required this.lista,
    required this.familia,
  });

  @override
  CoordenadorListScreenState createState() => CoordenadorListScreenState();

}

class CoordenadorListScreenState extends State<CoordenadorListScreen> with AutomaticKeepAliveClientMixin {
  final itemListaController = ItemListaController(ItemListaService());
  late Future<List<ItemListaModel>> _futureItensLista;
  bool _isLoading = false;
  bool hasImage = true;

  @override
  void initState() {
    super.initState();
    _futureItensLista = _fetchItensLista();
  }

  Future<List<ItemListaModel>> _fetchItensLista() async {
    setState(() {
      _isLoading = true;
    });

    try {
      final Map<String, dynamic> responseData = await itemListaController.getItensListaFamilia(widget.lista.id!, widget.familia.sobrenome);
      if (responseData["success"]) {
        return responseData["data"];
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }
      
      return [];
      
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }


  Widget _buildProductsListView(List<ItemListaModel> itens) {
    return ListView.separated(
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(height: AppTheme.listItemSpacing);
      },
      scrollDirection: Axis.vertical,
      itemCount: itens.length,
      itemBuilder: (context, index) {
        final item = itens[index];

        return GestureDetector(
          onTap: () async {
            final result = await Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => AddListProductScreen(itemLista: item, listId: widget.lista.id!, isEditable: true)),
            );

            if (result == true) {
              DataManager.setHasUnsavedChanges(false);
              _futureItensLista = _fetchItensLista();
            }
          },
          child: RocaListItem(
            thumbnail: item.produto.imagemUrl,
            title: item.produto.nome,
            subtitle: '${item.quantidade} ${item.produto.unidade.nome}',
            color: 'green',
            hasImage: hasImage,
            onlyVisible: false,
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    String title = '${widget.familia.sobrenome}';


    Widget body = Column(
      children: [
        // Info da Lista
        Padding(
          padding: EdgeInsets.symmetric(vertical: 15), 
          child: Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                color: AppTheme.land,
                width: 5,
              ),
              borderRadius: BorderRadius.circular(AppTheme.buttonBorderRadius),
            ),
            padding: const EdgeInsets.all(8.0),
            child: Text(
              '${widget.lista.nome}\n(${DateFormat('dd/MM/yy').format(widget.lista.dataInicio)} - ${DateFormat('dd/MM/yy').format(widget.lista.dataFim)})',
              style: Theme.of(context).textTheme.headlineSmall!.land,
              textAlign: TextAlign.center,
            ),
          ),
        ),

        Expanded(
          child: FutureBuilder<List<ItemListaModel>>(
            future: _futureItensLista,
            builder:  (context, snapshot) {
              if (_isLoading) {
                return const Center(
                  child: CircularProgressIndicator()
                );
              } 
              else if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasData && snapshot.data!.isNotEmpty) {
                  final itens = snapshot.data!;
                  return _buildProductsListView(itens);
                }
                else {
                  return const Text("Nada na lista.");
                }
              } else {
                return Container();
              }
            },
          )
        ),
      ],
    );

    Widget floatingActionButton = FloatingActionButton(
      shape: const CircleBorder(),
      backgroundColor: AppTheme.land,
      child: const Icon(Icons.add_sharp, color: AppTheme.vanilla, size: 40),
      onPressed: () async {
        final result = await Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => AddListProductScreen(listId: widget.lista.id!, isEditable: true)),
        );

        if (result == true) {
          DataManager.setHasUnsavedChanges(false);
          _futureItensLista = _fetchItensLista();
        }

      },
    );


    return Responsive(
      title: title,
      body: body,
      floatingActionButton: floatingActionButton,
      hasSideMenu: false,
    );
  }
  
  @override
  bool get wantKeepAlive => true;
}
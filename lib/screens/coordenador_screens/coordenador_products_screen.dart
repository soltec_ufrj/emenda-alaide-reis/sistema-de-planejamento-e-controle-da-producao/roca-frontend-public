import 'package:diacritic/diacritic.dart';
import 'package:flutter/material.dart';

import '../../app_data.dart';
import '../../constants.dart';
import '../../responsive.dart';
import '../../components/roca_list.dart';
import '../../components/roca_grid.dart';
import '../../components/roca_radio_button.dart';

import '../../models/produto_model.dart';
import '../../controllers/produto_controller.dart';
import '../../services/produto_service.dart';

import '../../screens/add_screens/add_product_screen.dart';


class CoordenadorProductsScreen extends StatefulWidget {
  static const routeName = '/coordenador_products';
  static const title = 'Produtos';


  const CoordenadorProductsScreen({super.key});

  @override
  CoordenadorProductsScreenState createState() => CoordenadorProductsScreenState();

}

class CoordenadorProductsScreenState extends State<CoordenadorProductsScreen> {
  bool hasImage = true;
  final searchbarController = TextEditingController();
  final produtoController = ProdutoController(ProdutoService());
  late List<ProdutoModel> _allProducts;
  List<ProdutoModel> _filteredProducts = [];
  bool _isLoading = false;
  FocusNode searchFocus = FocusNode();

  @override
  void initState() {
    super.initState();
    _fetchProducts();
  }

  Future<void> _fetchProducts() async {
    setState(() {
      _isLoading = true;
    });

    try {
      final Map<String, dynamic> responseData = await produtoController.getProdutos();

      if (responseData["success"]) {
        setState(() {
          _allProducts = responseData["data"];
          _filteredProducts = _allProducts;
        });
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }


  void searchProduct(String query) {
    final suggestions = _allProducts.where((product) {
      final productName = removeDiacritics(product.nome.toLowerCase());
      final input = removeDiacritics(query.toLowerCase());
      return productName.contains(input);
    }).toList();

    setState(() {
      _filteredProducts = suggestions;
    });
  }

  Widget _buildListView(List<ProdutoModel> produtos) {
    return ListView.separated(
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(height: AppTheme.listItemSpacing);
      },
      scrollDirection: Axis.vertical,
      itemCount: produtos.length,
      itemBuilder: (context, index) {
        final produto = produtos[index];

        return GestureDetector(
          onTap: () async {
            searchFocus.unfocus();
            searchbarController.clear();
            setState(() {
              _filteredProducts = _allProducts;
            });

            final result = await Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => AddProductScreen(hasImage: hasImage, edit: true, produto: produto)),
            );
            if (result == true) {
              DataManager.setHasUnsavedChanges(false);
              _fetchProducts();
            }
          },
          child: RocaListItem(
            thumbnail: produto.imagemUrl,
            title: produto.nome,
            subtitle: produto.unidade.nome,
            color: 'green',
            hasImage: hasImage,
            onlyVisible: false,
          ),
        );
      },
    );
  }

  Widget _buildGridView(List<ProdutoModel> produtos) {
    return LayoutBuilder(builder: (context, constraints) {
      final crossAxisCount = constraints.maxWidth > 700 ? 4 : 2;
      return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: crossAxisCount,
          mainAxisSpacing: 8.0,
          crossAxisSpacing: 8.0,
          childAspectRatio: crossAxisCount == 2 ? 4 / 7 : 6 / 7,
        ),
        itemCount: produtos.length,
        itemBuilder: (context, index) {
          final produto = produtos[index];

          return GestureDetector(
            onTap: () async {
              searchFocus.unfocus();
              searchbarController.clear();
              setState(() {
                _filteredProducts = _allProducts;
              });

              final result = await Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AddProductScreen(hasImage: hasImage, edit: true, produto: produto)),
              );
              if (result == true) {
                DataManager.setHasUnsavedChanges(false);
                _fetchProducts();
              }
            },
            child: RocaGridItem(
              thumbnail: produto.imagemUrl,
              title: produto.nome,
              subtitle: produto.unidade.nome,
              color: 'green',
              hasImage: hasImage,
              onlyVisible: false,
            ),
          );
        },
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Column(
      children: [
        Row(
          children: [
            Expanded(
              child: TextField(
                controller: searchbarController,
                decoration: InputDecoration(
                  prefixIcon: const Icon(Icons.search),
                  hintText: 'Nome do Produto',
                  // hintStyle: Theme.of(context).textTheme.titleLarge!.land,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: const BorderSide(color: AppTheme.land),
                  ),
                ),
                onChanged: searchProduct,
                autofocus: false,
                focusNode: searchFocus,
              ),
            ),
            CustomRadioButton(
              icon: Icons.list,
              value: DataManager.getIsListView(),
              onChanged: (newValue) {
                if (!DataManager.getIsListView()) {
                  setState(() {
                    DataManager.setIsListView(newValue);
                  });
                }
              },
            ),
            CustomRadioButton(
              icon: Icons.grid_view,
              value: !DataManager.getIsListView(),
              onChanged: (newValue) {
                if (DataManager.getIsListView()) {
                  setState(() {
                    DataManager.setIsListView(!newValue);
                  });
                }
              },
            ),
          ],
        ),

        Expanded(
          child: _isLoading
              ? const Center(child: CircularProgressIndicator())
              : _filteredProducts.isNotEmpty
                  ? DataManager.getIsListView()
                      ? _buildListView(_filteredProducts)
                      : _buildGridView(_filteredProducts)
                  : const Text("Nenhum produto."),
        ),
      ],
    );

    Widget floatingActionButton = FloatingActionButton(
      shape: const CircleBorder(),
      backgroundColor: AppTheme.land,
      child: const Icon(Icons.add_sharp, color: AppTheme.vanilla, size: 40),
      onPressed: () async {
        final result = await Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => AddProductScreen(hasImage: hasImage)),
        );

        searchbarController.clear();
        searchFocus.unfocus();
        setState(() {
          _filteredProducts = _allProducts;
        });


        if (result == true) {
          DataManager.setHasUnsavedChanges(false);
          _fetchProducts();
        }
      },
    );

    return Responsive(
      title: CoordenadorProductsScreen.title,
      body: body,
      floatingActionButton: floatingActionButton,
    );
  }
}
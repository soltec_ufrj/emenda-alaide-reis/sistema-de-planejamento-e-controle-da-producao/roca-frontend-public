import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:collection/collection.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:pdf/pdf.dart';
import 'package:printing/printing.dart';
import 'package:roca/app_data.dart';
import 'package:syncfusion_flutter_xlsio/xlsio.dart' as xcel;
import 'package:open_file/open_file.dart';
import 'dart:io';

import '../../constants.dart';
import '../../responsive.dart';
import '../../fileStorage.dart';
import '../../components/roca_button.dart';
import '../../components/roca_dropdown.dart';

import '../../models/lista_model.dart';
import '../../models/item_lista_model.dart';
import '../../controllers/item_lista_controller.dart';
import '../../services/item_lista_service.dart';


class ListExportScreen extends StatefulWidget {
  static const routeName = '/lista/exportar';
  static const title = 'Exportar Lista';

  final ListaModel lista;

  const ListExportScreen({
    super.key, 
    required this.lista,
  });

  @override
  ListExportScreenState createState() => ListExportScreenState();
}

class ListExportScreenState extends State<ListExportScreen> {
  final _listExportFormKey = GlobalKey<FormState>();
  final itemListaController = ItemListaController(ItemListaService());

  Map<String, String> formatoOptions = {
    'pdf': 'PDF',
    'excel': 'Excel',
    'clipboard': 'Copiar Texto',
  };
  String? _formatoValue;

  Map<String, String> visualizacaoOptions = {
    'porProduto': 'Por Produto',
    'porFamilia': 'Por Família',
  };
  String? _visualizacaoValue;



  void _exportList(BuildContext context) async {
    // salva os valores dos campos do formulário para atualizar seu status
    _listExportFormKey.currentState!.save();

    if (_listExportFormKey.currentState!.validate()) {
      if (_formatoValue == 'pdf') {
        downloadPDFDialog(context);
      }
      else if (_formatoValue == 'excel') {
        downloadExcelDialog(context);
      }
      else if (_formatoValue == 'clipboard') {
        copyToClipboardDialog(context);
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              'Opção inválida.', 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }
    }
    else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Por favor selecione um tipo de exportação.', 
            style: Theme.of(context).textTheme.headlineSmall!.white,
            textAlign: TextAlign.center,
          ),
          duration: AppTheme.messageDuration,
          backgroundColor: AppTheme.danger,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Form(
      key: _listExportFormKey,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [

            // Info da Lista
            Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(
                  color: AppTheme.land,
                  width: 5,
                ),
                borderRadius: BorderRadius.circular(AppTheme.buttonBorderRadius),
              ),
              padding: const EdgeInsets.all(8.0),
              child: Text(
                '${widget.lista.nome}\n(${DateFormat('dd/MM/yy').format(widget.lista.dataInicio)} - ${DateFormat('dd/MM/yy').format(widget.lista.dataFim)})',
                style: Theme.of(context).textTheme.headlineSmall!.land,
                textAlign: TextAlign.center,
              ),
            ),


            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // Formato de Exportação
                  RocaDropdown(
                    hintText: 'Formato de exportação',
                    options: (f, cs) => formatoOptions.keys.toList(),
                    value: _formatoValue,
                    getLabel: (value) {
                      return formatoOptions[value] ?? "";
                    },
                    onChanged: (newValue) {
                      setState(() {
                        _formatoValue = newValue;
                      });
                    },
                    validator: (value) {
                      if (value == null && _formatoValue == null) {
                        return 'Formato de exportação é obrigatório';
                      }
                      return null;
                    },
                  ),
                  // Visualização
                  RocaDropdown(
                    hintText: 'Tipo de Visualização',
                    options: (f, cs) => visualizacaoOptions.keys.toList(),
                    value: _visualizacaoValue,
                    getLabel: (value) {
                      return visualizacaoOptions[value] ?? "";
                    },
                    onChanged: (newValue) {
                      setState(() {
                        _visualizacaoValue = newValue;
                      });
                    },
                    validator: (value) {
                      if (value == null && _visualizacaoValue == null) {
                        return 'Tipo de visualização é obrigatório';
                      }
                      return null;
                    },
                  ),
                  RocaButton(
                    text: 'Exportar', 
                    outline: false,
                    buttonColor: AppTheme.aspargus,
                    textStyle: const TextStyle(
                      color: Colors.white, 
                      fontSize: AppTheme.buttonFontSize
                    ), 
                    onTap: () { _exportList(context);}
                  ),
                ],
              ),
            ),
          ],
        ),
      )
    );

    return Responsive(
      title: ListExportScreen.title,
      body: body,
      backgroundColor: AppTheme.vanilla,
      hasSideMenu: false,
      appBarLandTheme: false,
    );
  }


  void downloadExcelDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        content: Text('Você esta prestes a gerar um excel da lista ${widget.lista.nome}. Continuar?'),
        actions: [
          TextButton(
            onPressed: () => Navigator.of(context).pop(),
            child: const Text('Não'),
          ),
          TextButton(
            onPressed: () {
              generateAndSaveExcel();
              Navigator.of(context).pop();
            },
            child: const Text('Sim'),
          ),
        ],
      ),
    );
  }

  Future<void> generateAndSaveExcel() async {
    setState(() {
      DataManager.setIsLoading(true);
    });
    final xcel.Workbook workbook = xcel.Workbook(); // create a new excel workbook
    final xcel.Worksheet sheet = workbook.worksheets[0]; // the sheet we will be populating (only the first sheet)
    String excelFile = 'lista-$_visualizacaoValue-${widget.lista.nome}-${DateFormat('ddMMyyyy').format(widget.lista.dataInicio)}-${DateFormat('ddMMyyyy').format(widget.lista.dataFim)}'; // the name of the excel

    // Style for header row
    final xcel.Style headerStyle = workbook.styles.add('HeaderStyle');
    headerStyle.bold = true;
    headerStyle.backColor = '#654833';
    headerStyle.fontColor = '#ffffff';
    headerStyle.wrapText = true;
    headerStyle.hAlign = xcel.HAlignType.center;

    // Style for all cells
    final xcel.Style allCellsStyle = workbook.styles.add('AllCellsStyle');
    allCellsStyle.wrapText = true;
    allCellsStyle.hAlign = xcel.HAlignType.center;

    // Style for odd row
    final xcel.Style oddRowStyle = workbook.styles.add('OddRowStyle');
    oddRowStyle.backColor = '#d9d9d9';
    oddRowStyle.wrapText = true;
    oddRowStyle.hAlign = xcel.HAlignType.center;

    // Freeze the first row
    sheet.getRangeByIndex(2, 1).freezePanes();

    if (_visualizacaoValue == 'porProduto') {
      final Map<String, dynamic> responseData = await itemListaController.getExportProdutos(widget.lista.id!);

      if (!responseData["success"]) {
        setState(() {
          DataManager.setIsLoading(false);
        });
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );

        workbook.dispose();

        return;
      }


      final itens = responseData["data"];

      /// design how the data in the excel sheet will be presented
      /// you can get the cell to populate by index e.g., (2, 1) or by name e.g., (A2)
      sheet.getRangeByIndex(1, 1).setText('Produto');
      sheet.getRangeByIndex(1, 2).setText('Quantidade');
      sheet.getRangeByIndex(1, 3).setText('Unidade');

      // Apply style to header row
      sheet.getRangeByIndex(1, 1).cellStyle = headerStyle;
      sheet.getRangeByIndex(1, 2).cellStyle = headerStyle;
      sheet.getRangeByIndex(1, 3).cellStyle = headerStyle;


      for (var i = 1; i < itens.length; i++) {
        // comecando na segunda linha para pular o cabeçalho
        var line = i+1;
        // sheet.getRangeByName('A$line:C$line').merge();
        sheet.getRangeByIndex(line, 1).setText(itens[i].produto.nome);
        sheet.getRangeByIndex(line, 2).setText(itens[i].quantidade.toString());
        sheet.getRangeByIndex(line, 3).setText(itens[i].produto.unidade.nome);

        if (i%2 == 0) {
          // Apply style to odd row
          sheet.getRangeByIndex(line, 1).cellStyle = oddRowStyle;
          sheet.getRangeByIndex(line, 2).cellStyle = oddRowStyle;
          sheet.getRangeByIndex(line, 3).cellStyle = oddRowStyle;
        }
        else {
          // Apply style to all rows
          sheet.getRangeByIndex(line, 1).cellStyle = allCellsStyle;
          sheet.getRangeByIndex(line, 2).cellStyle = allCellsStyle;
          sheet.getRangeByIndex(line, 3).cellStyle = allCellsStyle;
        }
      }

    }
    else if (_visualizacaoValue == 'porFamilia') {
      final Map<String, dynamic> responseData = await itemListaController.getItensLista(widget.lista.id!);

      if (!responseData["success"]) {
        setState(() {
          DataManager.setIsLoading(false);
        });
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );

        workbook.dispose();

        return;
      }


      final itens = responseData["data"];
      // final Map<String, List<ItemListaModel>> itensPorFamilia = groupBy(itens, (item) => item.familia!.sobrenome);
      
      // var line = 1;
      // for (var i = 0; i < itensPorFamilia.length; i++) {
      //   var familia = itensPorFamilia.keys.elementAt(i);
      //   var itensDaFamilia = itensPorFamilia[familia]!;
      //   sheet.getRangeByName('A$line:C$line').merge();
      //   sheet.getRangeByIndex(line, 1).setText(familia);
      //   line++;
      //   for (var j = 0; j < itensDaFamilia.length; j++) { 
      //     sheet.getRangeByIndex(line, 1).setText(itensDaFamilia[j].produto.nome);
      //     sheet.getRangeByIndex(line, 2).setText(itensDaFamilia[j].quantidade.toString());
      //     sheet.getRangeByIndex(line, 3).setText(itensDaFamilia[j].produto.unidade);
      //     line++;
      //   }
      // }

      /// design how the data in the excel sheet will be presented
      /// you can get the cell to populate by index e.g., (2, 1) or by name e.g., (A2)
      sheet.getRangeByIndex(1, 1).setText('Produto');
      sheet.getRangeByIndex(1, 2).setText('Quantidade');
      sheet.getRangeByIndex(1, 3).setText('Unidade');
      sheet.getRangeByIndex(1, 4).setText('Família');

      // Apply style to header row
      sheet.getRangeByIndex(1, 1).cellStyle = headerStyle;
      sheet.getRangeByIndex(1, 2).cellStyle = headerStyle;
      sheet.getRangeByIndex(1, 3).cellStyle = headerStyle;
      sheet.getRangeByIndex(1, 4).cellStyle = headerStyle;


      for (var i = 1; i < itens.length; i++) {
        // comecando na segunda linha para pular o cabeçalho
        var line = i+1;
        // sheet.getRangeByName('A$line:C$line').merge();
        sheet.getRangeByIndex(line, 1).setText(itens[i].produto.nome);
        sheet.getRangeByIndex(line, 2).setText(itens[i].quantidade.toString());
        sheet.getRangeByIndex(line, 3).setText(itens[i].produto.unidade.nome);
        sheet.getRangeByIndex(line, 4).setText(itens[i].familia?.sobrenome);

        if (i%2 == 0) {
          // Apply style to odd row
          sheet.getRangeByIndex(line, 1).cellStyle = oddRowStyle;
          sheet.getRangeByIndex(line, 2).cellStyle = oddRowStyle;
          sheet.getRangeByIndex(line, 3).cellStyle = oddRowStyle;
          sheet.getRangeByIndex(line, 4).cellStyle = oddRowStyle;
        }
        else {
          // Apply style to all rows
          sheet.getRangeByIndex(line, 1).cellStyle = allCellsStyle;
          sheet.getRangeByIndex(line, 2).cellStyle = allCellsStyle;
          sheet.getRangeByIndex(line, 3).cellStyle = allCellsStyle;
          sheet.getRangeByIndex(line, 4).cellStyle = allCellsStyle;
        }
      }

    }

    final List<int> bytes = workbook.saveAsStream();

    FileStorage.writeCounter(bytes, '$excelFile.xlsx');
    // File('/storage/emulated/0/Download/$excelFile.xlsx').writeAsBytes(bytes);

    setState(() {
      DataManager.setIsLoading(false);
    });
    
    // message to user
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text('Excel baixado com sucesso na pasta de Download.', style: Font.apply(Colors.white, FontStyle.regular, FontSize.h6)),
      ),
    );

    //dispose the workbook
    workbook.dispose();
  }


  void downloadPDFDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        content: Text('Você esta prestes a gerar um pdf da lista ${widget.lista.nome}. Continuar?'),
        actions: [
          TextButton(
            onPressed: () => Navigator.of(context).pop(),
            child: const Text('Não'),
          ),
          TextButton(
            onPressed: () {
              generateAndSavePDF();
              Navigator.of(context).pop();
            },
            child: const Text('Sim'),
          ),
        ],
      ),
    );
  }

  Future<void> generateAndSavePDF() async {
    setState(() {
      DataManager.setIsLoading(true);
    });
    final image = await imageFromAssetBundle('assets/img/logo.png');

    String pdfFile = 'lista-$_visualizacaoValue-${widget.lista.nome}-${DateFormat('ddMMyyyy').format(widget.lista.dataInicio)}-${DateFormat('ddMMyyyy').format(widget.lista.dataFim)}'; // the name of the excel
    final pdf = pw.Document();

    if (_visualizacaoValue == 'porProduto') {
      final Map<String, dynamic> responseData = await itemListaController.getExportProdutos(widget.lista.id!);

      if (!responseData["success"]) {

        setState(() {
          DataManager.setIsLoading(false);
        });
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );

        return;
      }

      final itens = responseData["data"];

      pdf.addPage(
        pw.MultiPage(
          pageFormat: PdfPageFormat.a4,
          margin: const pw.EdgeInsets.all(18.0),
          build: (pw.Context context) {
            return [
                pw.Column(
                  children: [
                    // Add the logo image at the top center
                    pw.Align(
                      alignment: pw.Alignment.topCenter,
                      child: pw.Image(image, width: 100, height: 100),
                    ),
                    // Add the list name and date range
                    pw.Text(
                      '${widget.lista.nome}\n(${DateFormat('dd/MM/yy').format(widget.lista.dataInicio)} - ${DateFormat('dd/MM/yy').format(widget.lista.dataFim)})',
                      style: const pw.TextStyle(fontSize: 17.0),
                      textAlign: pw.TextAlign.center,
                    ),
                    pw.SizedBox(height: 10.0),
                    pw.Divider(),
                    pw.SizedBox(height: 15.0),
                    // Add the table with product, quantity, and unit columns
                    pw.Table(
                      // border: pw.TableBorder.all(color: PdfColors.black, width: 1),
                      children: [
                        pw.TableRow(
                          decoration: pw.BoxDecoration(color: PdfColor.fromHex("#654833")),
                          children: [
                            pw.Container(
                              alignment: pw.Alignment.center,
                              padding: const pw.EdgeInsets.all(5.0),
                              child: pw.Text(
                                'Produto',
                                style: pw.TextStyle(fontSize: 15.0, fontWeight: pw.FontWeight.bold, color: PdfColors.white),
                              ),
                            ),
                            pw.Container(
                              alignment: pw.Alignment.center,
                              padding: const pw.EdgeInsets.all(5.0),
                              child: pw.Text(
                                'Quantidade',
                                style: pw.TextStyle(fontSize: 15.0, fontWeight: pw.FontWeight.bold, color: PdfColors.white),
                              ),
                            ),
                            pw.Container(
                              alignment: pw.Alignment.center,
                              padding: const pw.EdgeInsets.all(5.0),
                              child: pw.Text(
                                'Unidade',
                                style: pw.TextStyle(fontSize: 15.0, fontWeight: pw.FontWeight.bold, color: PdfColors.white),
                              ),
                            ),
                          ],
                        ),
                        // Add the table rows
                        for (var i = 0; i < itens.length; i++)
                          pw.TableRow(
                            decoration: i % 2 == 0 ? pw.BoxDecoration(color: PdfColor.fromHex('#D9D9D9')) : null,
                            children: [
                              pw.Container(
                                alignment: pw.Alignment.center,
                                padding: const pw.EdgeInsets.all(5.0),
                                child: pw.Text(
                                  itens[i].produto.nome,
                                  style: pw.TextStyle(fontSize: 15.0),
                                ),
                              ),
                              pw.Container(
                                alignment: pw.Alignment.center,
                                padding: const pw.EdgeInsets.all(5.0),
                                child: pw.Text(
                                  itens[i].quantidade.toString(),
                                  style: pw.TextStyle(fontSize: 15.0),
                                ),
                              ),
                              pw.Container(
                                alignment: pw.Alignment.center,
                                padding: const pw.EdgeInsets.all(5.0),
                                child: pw.Text(
                                  itens[i].produto.unidade.nome,
                                  style: pw.TextStyle(fontSize: 15.0),
                                ),
                              ),
                            ],
                          ),
                      ],
                    ),
                  ],
                ),
            ];
          },
        ),
      );
    }
    else if (_visualizacaoValue == 'porFamilia') {
      final Map<String, dynamic> responseData =  await itemListaController.getItensLista(widget.lista.id!);


      if (!responseData["success"]) {
        setState(() {
          DataManager.setIsLoading(false);
        });
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );

        return;
      }


      final itens = responseData["data"];

      final Map<String, List<ItemListaModel>> itensPorFamilia = groupBy(itens, (item) => item.familia!.sobrenome);

      pdf.addPage(
        pw.MultiPage(
          pageFormat: PdfPageFormat.a4,
          margin: const pw.EdgeInsets.all(18.0),
          build: (pw.Context context) {
            return [
              pw.Column(
                children: [
                  // Add the logo image at the top center
                  pw.Align(
                    alignment: pw.Alignment.topCenter,
                    child: pw.Image(image, width: 100, height: 100),
                  ),
                  // Add the list name and date range
                  pw.Text(
                    '${widget.lista.nome}\n(${DateFormat('dd/MM/yy').format(widget.lista.dataInicio)} - ${DateFormat('dd/MM/yy').format(widget.lista.dataFim)})',
                    style: const pw.TextStyle(fontSize: 17.0),
                    textAlign: pw.TextAlign.center,
                  ),
                  pw.SizedBox(height: 10.0),
                  pw.Divider(),
                  pw.SizedBox(height: 15.0),
                  // Add the table with product, quantity, and unit columns
                  for (var familia in itensPorFamilia.keys)
                    pw.Column(
                      crossAxisAlignment: pw.CrossAxisAlignment.start,
                      children: [
                        pw.Container(
                          color: PdfColor.fromHex("#654833"),
                          padding: const pw.EdgeInsets.all(8.0),
                          child: pw.Row(
                            mainAxisAlignment: pw.MainAxisAlignment.center,
                            children: [
                              pw.Expanded(
                                child: pw.Text(
                                  textAlign: pw.TextAlign.center,
                                  familia,
                                  style: pw.TextStyle(fontSize: 15.0, fontWeight: pw.FontWeight.bold, color: PdfColors.white),
                                ),
                              ),
                            ],
                          ),
                        ),
                        pw.Table(
                          columnWidths: {
                            0: const pw.FlexColumnWidth(1), // Product column width
                            1: pw.FlexColumnWidth(1), // Quantity column width
                            2: pw.FlexColumnWidth(1), // Unit column width
                          },
                          children: [
                            for (var i = 0; i < itensPorFamilia[familia]!.length; i++)
                              pw.TableRow(
                                decoration: i % 2 == 0 ? pw.BoxDecoration(color: PdfColor.fromHex('#D9D9D9')) : null,
                                children: [
                                  pw.Container(
                                    alignment: pw.Alignment.center,
                                    padding: const pw.EdgeInsets.all(5.0),
                                    child: pw.Text(
                                      itensPorFamilia[familia]![i].produto.nome,
                                      style: pw.TextStyle(fontSize: 15.0),
                                    ),
                                  ),
                                  pw.Container(
                                    alignment: pw.Alignment.center,
                                    padding: const pw.EdgeInsets.all(5.0),
                                    child: pw.Text(
                                      itensPorFamilia[familia]![i].quantidade.toString(),
                                      style: pw.TextStyle(fontSize: 15.0),
                                    ),
                                  ),
                                  pw.Container(
                                    alignment: pw.Alignment.center,
                                    padding: const pw.EdgeInsets.all(5.0),
                                    child: pw.Text(
                                      itensPorFamilia[familia]![i].produto.unidade.nome,
                                      style: pw.TextStyle(fontSize: 15.0),
                                    ),
                                  ),
                                ],
                              ),
                          ],
                        ),
                      ],
                    ),
                ],
              ),
            ];
          },
        ),
      );
    }

    final bytes = await pdf.save();

    // Get the external storage directory
    final directory = Directory("/storage/emulated/0/Download");

    // Define the file path
    final path = '${directory.path}/$pdfFile.pdf';

    final file = File(path);
    await file.writeAsBytes(bytes);

    setState(() {
      DataManager.setIsLoading(false);
    });

    final result = await OpenFile.open(path);

    if (result.type == ResultType.error) {
      SnackBar(
        content: Text(
          'Falha ao abrir o PDF', 
          style: Theme.of(context).textTheme.headlineSmall!.white,
          textAlign: TextAlign.center,
        ),
        duration: AppTheme.messageDuration,
        backgroundColor: AppTheme.danger,
      );
    }
  }

  void copyToClipboardDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        content: Text('Você esta prestes a copia a lista ${widget.lista.nome}. Continuar?'),
        actions: [
          TextButton(
            onPressed: () => Navigator.of(context).pop(),
            child: const Text('Não'),
          ),
          TextButton(
            onPressed: () {
              generateTextAndCopytoClipboard();
              Navigator.of(context).pop();
            },
            child: const Text('Sim'),
          ),
        ],
      ),
    );
  }

  Future<void> generateTextAndCopytoClipboard() async {
    setState(() {
      DataManager.setIsLoading(true);
    });
    String listaText = '${widget.lista.nome} (${DateFormat('dd/MM/yyyy').format(widget.lista.dataInicio)} - ${DateFormat('dd/MM/yyyy').format(widget.lista.dataFim)}):\n';

    if (_visualizacaoValue == 'porProduto') {
      final Map<String, dynamic> responseData = await itemListaController.getExportProdutos(widget.lista.id!);

      if (!responseData["success"]) {
        setState(() {
          DataManager.setIsLoading(false);
        });
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }

      final itens = responseData["data"];
      
      for (var i = 0; i < itens.length; i++) {
        listaText += '${itens[i].produto.nome}: ${itens[i].quantidade} ${itens[i].produto.unidade.nome}\n';
      }
    }
    else if (_visualizacaoValue == 'porFamilia') {
      final Map<String, dynamic> responseData =  await itemListaController.getItensLista(widget.lista.id!);

      if (!responseData["success"]) {
        setState(() {
          DataManager.setIsLoading(false);
        });
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );

        return;
      }

      final itens = responseData["data"];

      final Map<String, List<ItemListaModel>> itensPorFamilia = groupBy(itens, (item) => item.familia!.sobrenome);

      for (var familia in itensPorFamilia.keys) {
        listaText += '$familia:\n';

        for (var i = 0; i < itensPorFamilia[familia]!.length; i++) {
          listaText += '${itensPorFamilia[familia]![i].produto.nome}: ${itensPorFamilia[familia]![i].quantidade} ${itensPorFamilia[familia]![i].produto.unidade.nome}\n';
        }

        listaText += '\n';
      }

    }

    Clipboard.setData(ClipboardData(text: listaText))
                .then((_) {
                  setState(() {
                    DataManager.setIsLoading(false);
                  });
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text('Lista copiada com sucesso.', style: Font.apply(Colors.white, FontStyle.regular, FontSize.h6)),
                    ),
                  );
                });
  }


}
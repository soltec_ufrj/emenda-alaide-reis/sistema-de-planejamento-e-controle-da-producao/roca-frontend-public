
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../constants.dart';
import '../../responsive.dart';
import '../../components/roca_list.dart';

import '../../models/item_lista_model.dart';
import '../../models/produto_model.dart';
import '../../models/lista_model.dart';
import '../../controllers/item_lista_controller.dart';
import '../../services/item_lista_service.dart';


class CoordenadorCompleteListScreen extends StatefulWidget {
  static const routeName = '/coordenador/lista/completa';
  static const title = 'Lista Completa';

  final ListaModel lista;

  const CoordenadorCompleteListScreen({
    super.key,
    required this.lista,
  });

  @override
  CoordenadorCompleteListScreenState createState() => CoordenadorCompleteListScreenState();

}

class CoordenadorCompleteListScreenState extends State<CoordenadorCompleteListScreen> {
  bool hasImage = true;
  final itemListaController = ItemListaController(ItemListaService());


  Future<List<ItemListaModel>> _fetchItensLista() async {
    final Map<String, dynamic> responseData = await itemListaController.getItensLista(widget.lista.id!);

    if (responseData["success"]) {
      return responseData["data"];
    }
    else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            responseData["message"], 
            style: Theme.of(context).textTheme.headlineSmall!.white,
            textAlign: TextAlign.center,
          ),
          duration: AppTheme.messageDuration,
          backgroundColor: AppTheme.danger,
        ),
      );
    }

    return [];

  }

  Widget _buildCompleteListView(List<ItemListaModel> itens) {
    // Mapa para rastrear a quantidade de cada produto
    final Map<ProdutoModel, Map<String, dynamic>> infosPorProduto = {};
    
    // Loop para somar as quantidades de cada produto
    for (final item in itens) {
      final produto = item.produto;
      final familia = item.familia?.sobrenome ?? 'Sem Família';
      final quantidade = item.quantidade;

      bool produtoJaAdicionado = false;

      // Verifica se o mesmo produto já foi adicionado ao mapa
      for (final chaveProduto in infosPorProduto.keys) {
        if (chaveProduto.nome == produto.nome) {
          infosPorProduto[chaveProduto]!['total'] += quantidade;
          infosPorProduto[chaveProduto]!['familias'][familia] = 
              (infosPorProduto[chaveProduto]!['familias'][familia] ?? 0) + quantidade;
          produtoJaAdicionado = true;
          break;
        }
      }

      if (!produtoJaAdicionado) {
        infosPorProduto[produto] = {
          'total': quantidade,
          'familias': <String, int>{familia: quantidade},
        };
      }
    }


    return ListView.separated(
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(height: AppTheme.listItemSpacing);
      },
      scrollDirection: Axis.vertical,
      itemCount: infosPorProduto.length,
      itemBuilder: (context, index) {
        final produto = infosPorProduto.keys.elementAt(index);
        final quantidade = infosPorProduto[produto]!['total'];

        return RocaListItem(
          thumbnail: produto.imagemUrl,
          title: produto.nome,
          subtitle: '${quantidade.toString()} ${produto.unidade.nome}',
          color: 'orange',
          hasImage: hasImage,
          onlyVisible: true,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(vertical: AppTheme.listItemSpacing),
              child: ListView.separated(
                separatorBuilder: (BuildContext context, int index) {
                  return SizedBox(height: AppTheme.listItemSpacing);
                },
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: infosPorProduto[produto]!['familias'].length,
                itemBuilder: (context, familiaIndex) {
                  final familiaSobrenome = infosPorProduto[produto]!['familias'].keys.elementAt(familiaIndex);
                  final familiaQuantidade = infosPorProduto[produto]!['familias'][familiaSobrenome];

                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: AppTheme.listItemSpacing),
                    child: RocaListItem(
                      thumbnail: null,
                      hasImage: false,
                      title: familiaSobrenome,
                      subtitle: '${familiaQuantidade.toString()} ${produto.unidade.nome}',
                      color: 'white',
                      onlyVisible: true,
                    ),
                  );
                }
              ),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Column(
      children: [
        // Info da Lista
        Padding(
          padding: EdgeInsets.symmetric(vertical: 15), 
          child: Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                color: AppTheme.land,
                width: 5,
              ),
              borderRadius: BorderRadius.circular(AppTheme.buttonBorderRadius),
            ),
            padding: const EdgeInsets.all(8.0),
            child: Text(
              '${widget.lista.nome}\n(${DateFormat('dd/MM/yy').format(widget.lista.dataInicio)} - ${DateFormat('dd/MM/yy').format(widget.lista.dataFim)})',
              style: Theme.of(context).textTheme.headlineSmall!.land,
              textAlign: TextAlign.center,
            ),
          ),
        ),

        Expanded(
          child: FutureBuilder<List<ItemListaModel>>(
            future: _fetchItensLista(),
            builder:  (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(
                  child: CircularProgressIndicator()
                );
              }
              else if (snapshot.hasData && snapshot.data!.isNotEmpty) {
                final itens = snapshot.data!;
                return _buildCompleteListView(itens);
              }
              else {
                return const Text("Nada na lista.");
              }
            },
          )
        ),
      ],
    );


    return Responsive(
      title: CoordenadorCompleteListScreen.title,
      body: body,
      hasSideMenu: false,
    );
  }
}
import 'package:flutter/material.dart';
import 'package:roca/app_data.dart';

import '../../constants.dart';
import '../../responsive.dart';
import '../../components/roca_list.dart';
import '../../components/roca_grid.dart';
import '../../components/roca_radio_button.dart';

import '../../models/assentamento_model.dart';
import '../../controllers/assentamento_controller.dart';
import '../../services/assentamento_service.dart';

import '../../screens/add_screens/add_assentamento_screen.dart';


class CoordenadorAssentamentosScreen extends StatefulWidget {
  static const routeName = '/coordenador_assentamentos';
  static const title = 'Assentamentos';

  const CoordenadorAssentamentosScreen({super.key});

  @override
  CoordenadorAssentamentosScreenState createState() => CoordenadorAssentamentosScreenState();

}

class CoordenadorAssentamentosScreenState extends State<CoordenadorAssentamentosScreen> {
  bool hasImage = false;
  final assentamentoController = AssentamentoController(AssentamentoService());
  late Future<List<AssentamentoModel>> _futureAssentamentos;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    _futureAssentamentos = _fetchAssentamentos();
  }

  Future<List<AssentamentoModel>> _fetchAssentamentos() async {
    setState(() {
      _isLoading = true;
    });

    try {
      final Map<String, dynamic> responseData = await assentamentoController.getAssentamentos();

      if (responseData["success"]) {
        return responseData["data"];
      }
      else { // pode tirar esse else
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }

      return [];
      
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }

  Widget _buildListView(List<AssentamentoModel> assentamentos) {
    return ListView.separated(
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(height: AppTheme.listItemSpacing);
      },
      scrollDirection: Axis.vertical,
      itemCount: assentamentos.length,
      itemBuilder: (context, index) {
        final assentamento = assentamentos[index];
        final cidade = assentamento.cep?.cidade ?? '';

        return GestureDetector(
          onTap: () async {
            final result = await Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => AddAssentamentoScreen(hasImage: hasImage, edit: true, assentamento: assentamento)),
            );

            if (result == true) {
              DataManager.setHasUnsavedChanges(false);
              _futureAssentamentos = _fetchAssentamentos();
            }

          },
          child: RocaListItem(
            thumbnail: null,
            title: assentamento.nome,
            subtitle: cidade,
            color: 'green',
            hasImage: hasImage,
            onlyVisible: false,
          ),
        );
      },
    );
  }

  Widget _buildGridView(List<AssentamentoModel> assentamentos) {
    return LayoutBuilder(builder: (context, constraints) {
      final crossAxisCount = constraints.maxWidth > 700 ? 4 : 2;
      return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: crossAxisCount,
          mainAxisSpacing: 8.0,
          crossAxisSpacing: 8.0,
          childAspectRatio: 6 / 7,
        ),
        itemCount: assentamentos.length,
        itemBuilder: (context, index) {
          final assentamento = assentamentos[index];
          final cidade = assentamento.cep?.cidade ?? '';

          return GestureDetector(
            onTap: () async {
              final result = await Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AddAssentamentoScreen(hasImage: hasImage, edit: true, assentamento: assentamento)),
              );

              if (result == true) {
                DataManager.setHasUnsavedChanges(false);
                _futureAssentamentos = _fetchAssentamentos();
              }

            },
            child: RocaGridItem(
              thumbnail: null,
              title: assentamento.nome,
              subtitle: cidade,
              color: 'green',
              hasImage: hasImage,
              onlyVisible: false,
            )
          );
        },
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Column(
      children: [
        Row(
          children: [
            const Spacer(),
            CustomRadioButton(
              icon:Icons.list,
              value: DataManager.getIsListView(),
              onChanged: (newValue) {
                if (!DataManager.getIsListView()) {
                  setState(() {
                    DataManager.setIsListView(newValue);
                  });
                }
              },
            ),
            CustomRadioButton(
              icon:Icons.grid_view,
              value: !DataManager.getIsListView(),
              onChanged: (newValue) {
                if (DataManager.getIsListView()) {
                  setState(() {
                    DataManager.setIsListView(!newValue);
                  });
                }
              },
            ),
          ],
        ),

        // Assentamentos
        Expanded(
          child: FutureBuilder<List<AssentamentoModel>>(
            future: _futureAssentamentos,
            builder: (context, snapshot) {
              if (_isLoading) {
                return const Center(
                  child: CircularProgressIndicator()
                );
              } 
              else if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasData && snapshot.data!.isNotEmpty) {
                  final users = snapshot.data!;
                  return DataManager.getIsListView() ? _buildListView(users) : _buildGridView(users);
                } else {
                  return const Text("Nenhum assentamento.");
                }
              } else {
                return Container();
              }
            },
          )
        ),
      ],
    );

    Widget floatingActionButton = FloatingActionButton(
      shape: const CircleBorder(),
      backgroundColor: AppTheme.land,
      child: const Icon(Icons.add_sharp, color: AppTheme.vanilla, size: 40),
      onPressed: () async {
        final result = await Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => AddAssentamentoScreen(hasImage: hasImage)),
        );

        if (result == true) {
          DataManager.setHasUnsavedChanges(false);
          _futureAssentamentos = _fetchAssentamentos();
        }
      },
    );


    return Responsive(
      title: CoordenadorAssentamentosScreen.title,
      body: body,
      floatingActionButton: floatingActionButton,
    );
  }
}
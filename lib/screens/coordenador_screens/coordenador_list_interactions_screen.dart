import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../app_data.dart';
import '../../constants.dart';
import '../../responsive.dart';

import '../../models/lista_model.dart';

import '../../screens/add_screens/add_list_screen.dart';
import '../../screens/coordenador_screens/coordenador_list_export.dart';
import '../../screens/coordenador_screens/coordenador_family_list.dart';
import '../../screens/coordenador_screens/coordenador_complete_list.dart';


class InteractionButton extends StatelessWidget {
  final IconData icon;
  final String text;
  final VoidCallback onTap;

  const InteractionButton({
    super.key, 
    required this.icon,
    required this.text,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          color: AppTheme.aspargus,
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Card(
          color: Colors.transparent,
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(icon, size: 50.0, color: AppTheme.land),
              const SizedBox(height: 10.0),
              AutoSizeText(
                text, 
                maxLines: 2,
                wrapWords: false,
                style: Theme.of(context).textTheme.headlineSmall!.land,
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
  


class ListInteractionsScreen extends StatefulWidget {
  static const routeName = '/list_interactions';
  final ListaModel lista;

  const ListInteractionsScreen({
    super.key,
    required this.lista,
  });

  @override
  ListInteractionsScreenState createState() => ListInteractionsScreenState();

}

class ListInteractionsScreenState extends State<ListInteractionsScreen> {
  late ListaModel _lista;

  @override
  void initState() {
    super.initState();
    _lista = widget.lista;
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Padding(
      padding: EdgeInsets.only(top: 15),
      child: LayoutBuilder(builder: (context, constraints) {
        final crossAxisCount = constraints.maxWidth > 700 ? 4 : 2;
        return GridView.count(
          crossAxisCount: crossAxisCount,
          children: <Widget>[
            InteractionButton(
              icon: Icons.edit,
              text: 'Editar dados da lista',
              onTap: () async {
                final result = await Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddListScreen(hasImage: false, edit: true, lista: _lista)),
                );
                if (result?['lista'] != null) {
                  setState(() {
                    _lista = result?['lista'];
                  });
                }
                if (result?['changesSaved'] == true) {
                  DataManager.setHasUnsavedChanges(false);
                }
              },
            ),
            InteractionButton(
              icon: Icons.file_download,
              text: 'Exportar Lista',
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ListExportScreen(lista: _lista)),
                ).then((value)=> setState((){}));
              },
            ),
            InteractionButton(
              icon: Icons.grass,
              text: 'Gerenciar produtos da lista',
              // TODO: TALVEZ TENHA QUE RETORNAR A LISTA EDITADA
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => CoordenadorFamilyListScreen(lista: _lista)),
                ).then((value)=> setState((){}));
              },
            ),
            InteractionButton(
              icon: Icons.notes,
              text: 'Mostrar lista completa',
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => CoordenadorCompleteListScreen(lista: _lista)),
                ).then((value)=> setState((){}));
              },
            ),
          ],
        );
      })
    );

    return Responsive(
      title: '${_lista.nome}\n(${DateFormat('dd/MM/yy').format(_lista.dataInicio)} - ${DateFormat('dd/MM/yy').format(_lista.dataFim)})',
      body: body,
      hasSideMenu: false,
    );
  }
}
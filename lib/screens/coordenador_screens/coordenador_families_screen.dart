import 'package:flutter/material.dart';

import '../../app_data.dart';
import '../../constants.dart';
import '../../responsive.dart';
import '../../components/roca_list.dart';
import '../../components/roca_grid.dart';
import '../../components/roca_radio_button.dart';

import '../../models/familia_model.dart';
import '../../controllers/familia_controller.dart';
import '../../services/familia_service.dart';

import '../../screens/add_screens/add_family_screen.dart';


class CoordenadorFamiliesScreen extends StatefulWidget {
  static const routeName = '/coordenador_families';
  static const title = 'Famílias';

  const CoordenadorFamiliesScreen({super.key});

  @override
  CoordenadorFamiliesScreenState createState() => CoordenadorFamiliesScreenState();

}

class CoordenadorFamiliesScreenState extends State<CoordenadorFamiliesScreen> {
  bool hasImage = false;
  final familiaController = FamiliaController(FamiliaService());
  late Future<List<FamiliaModel>> _futureFamilies;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    _futureFamilies = _fetchFamilies();
  }

  Future<List<FamiliaModel>> _fetchFamilies() async {
    setState(() {
      _isLoading = true;
    });

    try {
      final Map<String, dynamic> responseData = await familiaController.getFamilias();
      if (responseData["success"]) {
        return responseData["data"];
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }

      return [];
      
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }


  Widget _buildListView(List<FamiliaModel> familias) {
    return ListView.separated(
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(height: AppTheme.listItemSpacing);
      },
      scrollDirection: Axis.vertical,
      itemCount: familias.length,
      itemBuilder: (context, index) {
        final familia = familias[index];

        return GestureDetector(
          onTap: () async {
            final result = await Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => AddFamilyScreen(hasImage: hasImage, edit: true, familia: familia)),
            );

            if (result == true) {
              DataManager.setHasUnsavedChanges(false);
              _futureFamilies = _fetchFamilies();
            }
          },
          child: RocaListItem(
            thumbnail: null,
            title: familia.sobrenome,
            subtitle: familia.assentamento?.nome ?? 'Sem Assentamento',
            color: 'green',
            hasImage: hasImage,
            onlyVisible: false,
          ),
        );
      },
    );
  }

  Widget _buildGridView(List<FamiliaModel> familias) {
    return LayoutBuilder(builder: (context, constraints) {
      final crossAxisCount = constraints.maxWidth > 700 ? 4 : 2;
      return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: crossAxisCount,
          mainAxisSpacing: 8.0,
          crossAxisSpacing: 8.0,
          childAspectRatio: 6 / 7,
        ),
        itemCount: familias.length,
        itemBuilder: (context, index) {
          final familia = familias[index];

          return GestureDetector(
            onTap: () async {
              final result = Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AddFamilyScreen(hasImage: hasImage, edit: true, familia: familia)),
              );

              if (result == true) {
                DataManager.setHasUnsavedChanges(false);
                _futureFamilies = _fetchFamilies();
              }
            },
            child: RocaGridItem(
              thumbnail: null,
              title: familia.sobrenome,
              subtitle: familia.assentamento?.nome ?? 'Sem Assentamento',
              color: 'green',
              hasImage: hasImage,
              onlyVisible: false,
            )
          );
        },
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Column(
      children: [
        Row(
          children: [
            const Spacer(),
            CustomRadioButton(
              icon:Icons.list,
              value: DataManager.getIsListView(),
              onChanged: (newValue) {
                if (!DataManager.getIsListView()) {
                  setState(() {
                    DataManager.setIsListView(newValue);
                  });
                }
              },
            ),
            CustomRadioButton(
              icon:Icons.grid_view,
              value: !DataManager.getIsListView(),
              onChanged: (newValue) {
                if (DataManager.getIsListView()) {
                  setState(() {
                    DataManager.setIsListView(!newValue);
                  });
                }
              },
            ),
          ],
        ),

        // Famílias
        Expanded(
          child: FutureBuilder<List<FamiliaModel>>(
            future: _futureFamilies,
            builder: (context, snapshot) {
              if (_isLoading) {
                return const Center(
                  child: CircularProgressIndicator()
                );
              } 
              else if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasData && snapshot.data!.isNotEmpty) {
                  final users = snapshot.data!;
                  return DataManager.getIsListView() ? _buildListView(users) : _buildGridView(users);
                } else {
                  return const Text("Nenhuma família.");
                }
              } else {
                return Container();
              }
            },
          )
        ),
      ],
    );

    Widget floatingActionButton = FloatingActionButton(
      shape: const CircleBorder(),
      backgroundColor: AppTheme.land,
      child: const Icon(Icons.add_sharp, color: AppTheme.vanilla, size: 40),
      onPressed: () async {
        final result = Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => AddFamilyScreen(hasImage: hasImage)),
        );

        if (result == true) {
          DataManager.setHasUnsavedChanges(false);
          _futureFamilies = _fetchFamilies();
        }
      },
    );


    return Responsive(
      title: CoordenadorFamiliesScreen.title,
      body: body,
      floatingActionButton: floatingActionButton,
    );
  }
}
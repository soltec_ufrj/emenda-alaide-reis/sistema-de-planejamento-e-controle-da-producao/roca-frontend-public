import 'package:flutter/material.dart';
import 'package:roca/app_data.dart';
import 'package:roca/components/roca_text_input.dart';
import 'package:roca/models/user_model.dart';
import 'package:roca/responsive.dart';
import 'package:roca/constants.dart';
import 'package:roca/screens/homepage_screen.dart';
import 'package:roca/services/user_service.dart';
import 'package:roca/controllers/user_controller.dart';
import 'package:roca/components/roca_button.dart';

class SignupScreen extends StatefulWidget {
  static const routeName = '/signup';

  const SignupScreen({super.key});

  @override
  SignupScreenState createState() => SignupScreenState();
}

class SignupScreenState extends State<SignupScreen> {
  final userController = UserController(UserService());

  final _signUpFormKey = GlobalKey<FormState>();
  final TextEditingController _nomeController = TextEditingController();
  final TextEditingController _telefoneController = TextEditingController();
  final TextEditingController _cpfController = TextEditingController();
  final TextEditingController _senhaController = TextEditingController();
  final TextEditingController _senhaConfirmController = TextEditingController();

  bool _isObscureSenha = true;
  bool _isObscureConfirmarSenha = true;

  Future<void> _signUp(BuildContext context) async {
    if (_signUpFormKey.currentState!.validate()) {
      setState(() {
        DataManager.setIsLoading(true);
      });
      final String nome = _nomeController.text;
      final String telefone = _telefoneController.text;
      final String cpf = _cpfController.text;
      final String senha = _senhaController.text;


      final unmaskedCPF = cpf.replaceAll(RegExp(r'\D'), '');
      final unmaskedTelefone = telefone.replaceAll(RegExp(r'\D'), '');


      final usuario = UsuarioModel(cpf: unmaskedCPF, senha: senha);
      final user = PessoaModel(
        usuario: usuario,
        nome: nome,
        telefone: unmaskedTelefone,
      );

      final Map<String, dynamic> responseData = await userController.signUp(user);

      setState(() {
        DataManager.setIsLoading(false);
      });


      if (responseData["success"]) {
        await showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(child: Text('Atenção')),
              content: Text('Seu usuário está sendo avaliado pelo coordenador. Quando autorizado você poderá entrar.'),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).pushNamedAndRemoveUntil(HomepageScreen.routeName, (Route<dynamic> route) => false);
                  },
                  child: Text('Ok'),
                ),
              ],
            );
          },
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"] ?? ApiConstants.msgErroInesperado,
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Form(
              key: _signUpFormKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RocaTextField(
                    controller: _nomeController,
                    labelText: 'Nome',
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Nome é obrigatório';
                      }
                      return null;
                    },
                  ),
                  RocaTextField(
                    keyboardType: TextInputType.number,
                    controller: _telefoneController,
                    labelText: 'Telefone',
                    inputFormatters: [AppTheme.phoneMask],
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Telefone é obrigatório';
                      }
                      return null;
                    },
                  ),
                  RocaTextField(
                    keyboardType: TextInputType.number,
                    controller: _cpfController,
                    labelText: 'CPF',
                    inputFormatters: [AppTheme.cpfMask],
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'CPF é obrigatório';
                      }
                      return null;
                    },
                  ),
                  RocaTextField(
                    obscureText: _isObscureSenha,
                    controller: _senhaController,
                    labelText: 'Senha',
                    icon: IconButton(
                      icon: Icon(
                        _isObscureSenha ? Icons.visibility_off : Icons.visibility ,
                      ),
                      onPressed: () {
                        setState(() {
                          _isObscureSenha = !_isObscureSenha;
                        });
                      },
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Senha é obrigatório';
                      }
                      return null;
                    },
                  ),
                  RocaTextField(
                    obscureText: _isObscureConfirmarSenha,
                    controller: _senhaConfirmController,
                    labelText: 'Confirmar Senha',
                    icon: IconButton(
                      icon: Icon(
                        _isObscureConfirmarSenha ? Icons.visibility_off : Icons.visibility ,
                      ),
                      onPressed: () {
                        setState(() {
                          _isObscureConfirmarSenha = !_isObscureConfirmarSenha;
                        });
                      },
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Confirmar Senha é obrigatório';
                      } else if (value != _senhaController.text) {
                        return 'Senha precisa ser igual acima';
                      }
                      return null;
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 30),
                    child: Container(
                      padding: const EdgeInsets.only(top: 3, left: 3),
                      child: RocaButton(
                        text: 'Criar',
                        outline: false,
                        buttonColor: AppTheme.aspargus,
                        textStyle: const TextStyle(
                          color: Colors.white,
                          fontSize: AppTheme.buttonFontSize,
                        ),
                        onTap: () => _signUp(context),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );

    return Responsive(
      title: 'Criar Conta',
      body: body,
      backgroundColor: AppTheme.vanilla,
      hasSideMenu: false,
      appBarLandTheme: false,
    );
  }
}

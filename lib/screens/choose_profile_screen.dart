import 'package:flutter/material.dart';

import '../constants.dart';
import '../app_data.dart';
import '../responsive.dart';
import '../components/roca_button.dart';

import '../controllers/user_controller.dart';
import '../services/user_service.dart';

import 'package:roca/screens/homepage_screen.dart';
import 'package:roca/screens/nucleado_screens/nucleado_list_screen.dart';
import 'package:roca/screens/coordenador_screens/coordenador_lists_screen.dart';


// TODO: TALVEZ SEJA UM STATEFUL WIDGET POR CONTA DO USER
class ChooseProfileScreen extends StatelessWidget {
  static const routeName = '/escolher_perfil';
  static const title = 'Escolher Perfil';

  final userController = UserController(UserService());

  ChooseProfileScreen({ super.key });

  // TODO: Logout quando usuario volta nessa tela
  void _logout (BuildContext context) async {
    final Map<String, dynamic> responseData = await userController.logout(DataManager.getLoggedUser()!.usuario);

    if (responseData["success"]) {
      Navigator.of(context).pushNamedAndRemoveUntil(HomepageScreen.routeName, (Route<dynamic> route) => false);
    }
    else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            responseData["message"], 
            style: Theme.of(context).textTheme.headlineSmall!.white,
            textAlign: TextAlign.center,
          ),
          duration: AppTheme.messageDuration,
          backgroundColor: AppTheme.danger,
        ),
      );
    }
  }

  // TODO: Ver se da pra colocar center singlescrollview que nem nos outros
  @override
  Widget build(BuildContext context) {
    Widget body = Container(
      height: MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top - (AppTheme.appbarHeight + AppTheme.appbarPadding),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          // Perfil de Nucleade
          RocaButton(
            text: 'Nucleado/a', 
            outline: false,
            buttonColor: AppTheme.land,
            textStyle: Theme.of(context).textTheme.headlineMedium!.white,
            onTap: () {Navigator.of(context).pushNamed(NucleadoListsScreen.routeName);}
          ),

          // Perfil de Coordenadore
          RocaButton(
            text: 'Coordenador/a', 
            outline: false,
            buttonColor: AppTheme.land,
            textStyle: Theme.of(context).textTheme.headlineMedium!.white,
            onTap: () {Navigator.of(context).pushNamed(CoordenadorListsScreen.routeName);}
          ),

          // Logout
          RocaButton(
            text: 'Sair', 
            outline: false,
            buttonColor: AppTheme.danger,
            textStyle: Theme.of(context).textTheme.headlineMedium!.white,
            onTap: () {_logout(context);}
          ),
        ],
      )
    );

    return WillPopScope(
      onWillPop: () async => false,
      child: Responsive(
        title: title,
        body: body,
        backgroundColor: AppTheme.vanilla,
        hasSideMenu: false,
        hasBackArrow: false,
        isCoordenador: false,
        appBarLandTheme: false,
      ),
    );
  }
}
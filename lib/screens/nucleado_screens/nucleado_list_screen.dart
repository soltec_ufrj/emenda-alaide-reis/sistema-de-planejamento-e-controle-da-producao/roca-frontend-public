import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:roca/app_data.dart';

import '../../constants.dart';
import '../../responsive.dart';
import '../../components/roca_radio_button.dart';
import '../../components/roca_list.dart';
import '../../components/roca_grid.dart';

import '../../models/lista_model.dart';
import '../../controllers/lista_controller.dart';
import '../../services/lista_service.dart';

import '../../screens/nucleado_screens/nucleado_products_screen.dart';


class NucleadoListsScreen extends StatefulWidget {
  static const routeName = '/nucleado/listas';
  static const title = 'Listas';

  const NucleadoListsScreen({super.key});

  @override
  NucleadoListsScreenState createState() => NucleadoListsScreenState();
}

class NucleadoListsScreenState extends State<NucleadoListsScreen> {
  final listaController = ListaController(ListaService());
  bool hasImage = false;

  Future<List<ListaModel>> _fetchLists() async {
    final Map<String, dynamic> responseData = await listaController.getListas();
    if (responseData["success"]) {
      return responseData["data"];
    }
    else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            responseData["message"], 
            style: Theme.of(context).textTheme.headlineSmall!.white,
            textAlign: TextAlign.center,
          ),
          duration: AppTheme.messageDuration,
          backgroundColor: AppTheme.danger,
        ),
      );
    }

    return [];
  }

  Widget _buildListView(List<ListaModel> listas) {
    return ListView.separated(
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(height: AppTheme.listItemSpacing);
      },
      scrollDirection: Axis.vertical,
      itemCount: listas.length,
      itemBuilder: (context, index) {
        final lista = listas[index];
        final String formattedFim = DateFormat('dd/MM/yy').format(lista.dataFim);
        bool isOpen = lista.isOpen();

        return GestureDetector(
          onTap: () async {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => NucleadoProductsScreen(list: lista, isOpen: isOpen)),
            ).then((value)=> setState((){}));
          },
          child: RocaListItem(
            thumbnail: null,
            title: lista.nome,
            subtitle: 'Prazo: $formattedFim',
            color: isOpen ? 'green' : 'gray',
            hasImage: hasImage,
            onlyVisible: !isOpen,
          ),
        );
      },
    );
  }

  Widget _buildGridView(List<ListaModel> listas) {
    return LayoutBuilder(builder: (context, constraints) {
      final crossAxisCount = constraints.maxWidth > 700 ? 4 : 2;
      return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: crossAxisCount,
          mainAxisSpacing: 8.0,
          crossAxisSpacing: 8.0,
          childAspectRatio: 6 / 7,
        ),
        itemCount: listas.length,
        itemBuilder: (context, index) {
          final lista = listas[index];
          final String formattedFim = DateFormat('dd/MM/yy').format(lista.dataFim);
          bool isOpen = lista.isOpen();


          return GestureDetector(
            onTap: () async {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => NucleadoProductsScreen(list: lista, isOpen:isOpen)),
              ).then((value)=> setState((){}));
            },
            child: RocaGridItem(
              thumbnail: null,
              title: lista.nome,
              subtitle: 'Prazo: $formattedFim',
              color: isOpen ? 'green' : 'gray',
              hasImage: hasImage,
              onlyVisible: !isOpen,
            )
          );
        },
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Column(
      children: [
        // View Selector
        Row(
          children: [
            const Spacer(), // Faz o seletor ficar na esquerda da tela
            CustomRadioButton(
              icon:Icons.list,
              value: DataManager.getIsListView(),
              onChanged: (newValue) {
                if (!DataManager.getIsListView()) {
                  setState(() {
                    DataManager.setIsListView(newValue);
                  });
                }
              },
            ),
            CustomRadioButton(
              icon:Icons.grid_view,
              value: !DataManager.getIsListView(),
              onChanged: (newValue) {
                if (DataManager.getIsListView()) {
                  setState(() {
                    DataManager.setIsListView(!newValue);
                  });
                }
              },
            ),
          ],
        ),

        // Listas
        Expanded(
          child: FutureBuilder<List<ListaModel>>(
            future: _fetchLists(),
            builder:  (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(
                  child: CircularProgressIndicator()
                );
              }
              else if (snapshot.hasData && snapshot.data!.isNotEmpty) {
                final listas = snapshot.data!;
                return DataManager.getIsListView() ? _buildListView(listas) : _buildGridView(listas);
              }
              else {
                return const Text("Nenhuma lista.");
              }
            },
          )
        ),
      ],
    );



    return WillPopScope(
      onWillPop: () async => (DataManager.getLoggedUser()?.funcao?.codigo == "0"),
      child: Responsive(
        title: NucleadoListsScreen.title,
        body: body,
        isCoordenador: false,
        appBarLandTheme: false,
      ),
    );
  }
}
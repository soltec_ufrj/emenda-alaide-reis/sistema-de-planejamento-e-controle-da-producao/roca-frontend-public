import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:collection/collection.dart';

import '../../responsive.dart';
import 'package:roca/app_data.dart';
import 'package:roca/constants.dart';

import 'package:roca/components/roca_dropdown.dart';
import 'package:roca/components/roca_list.dart';
import 'package:roca/components/roca_grid.dart';
import 'package:roca/components/roca_radio_button.dart';

import 'package:roca/controllers/item_lista_controller.dart';
import 'package:roca/services/item_lista_service.dart';

import 'package:roca/models/item_lista_model.dart';
import 'package:roca/models/lista_model.dart';
import 'package:roca/models/produto_model.dart';

import 'package:roca/screens/add_screens/add_list_product_screen.dart';


class NucleadoProductsScreen extends StatefulWidget {
  static const routeName = '/nucleado/listas/itens';
  final ListaModel list;
  final bool isOpen;

  const NucleadoProductsScreen({
    super.key, 
    required this.list,
    required this.isOpen,
  });

  @override
  NucleadoProductsScreenState createState() => NucleadoProductsScreenState();

}

class NucleadoProductsScreenState extends State<NucleadoProductsScreen> {
  bool hasImage = true;
  final itemListaController = ItemListaController(ItemListaService());
  late Future<List<ItemListaModel>> _futureItensLista;
  bool _isLoading = false;
  int? totalProdutos = null;

  @override
  void initState() {
    super.initState();
    _futureItensLista = _fetchItensLista().then((value) {
      setState(() {
        totalProdutos = value.length;
      });
      return value;
    });
  }

  Future<List<ItemListaModel>> _fetchAllItensLista() async {
    final Map<String, dynamic> responseData = await itemListaController.getItensLista(widget.list.id!);

    if (responseData["success"]) {
      return responseData["data"];
    }
    else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            responseData["message"], 
            style: Theme.of(context).textTheme.headlineSmall!.white,
            textAlign: TextAlign.center,
          ),
          duration: AppTheme.messageDuration,
          backgroundColor: AppTheme.danger,
        ),
      );
    }

    return [];
  }

  Future<List<ItemListaModel>> _fetchItensLista() async {
    setState(() {
      _isLoading = true;
    });

    try {
      final Map<String, dynamic> responseData = await itemListaController.getItensListaFamilia(widget.list.id!, DataManager.getLoggedUser()!.familia!.sobrenome);

      if (responseData["success"]) {
        if (responseData["data"].isEmpty)
          setState(() {
            totalProdutos = null;
          });
        return responseData["data"];
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }

      return [];
      
      
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }

  String _selectedSortOption = 'Por Família';

  void _sortProducts(dynamic option) {
    setState(() {
      _selectedSortOption = option!;
    });
  }

  Widget _buildFamilyProductsListView(List<ItemListaModel> itens) {
    return ListView.separated(
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(height: AppTheme.listItemSpacing);
      },
      scrollDirection: Axis.vertical,
      itemCount: itens.length,
      itemBuilder: (context, index) {
        final item = itens[index];

        return widget.isOpen
        ? GestureDetector(
          onTap: () async {
            final result = await Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => AddListProductScreen(itemLista: item, listId: widget.list.id!, isEditable: true)),
            );

            if (result == true) {
              DataManager.setHasUnsavedChanges(false);
              _futureItensLista = _fetchItensLista().then((value) {
                setState(() {
                  totalProdutos = value.length;
                });
                return value;
              });
            }
          },
          child: RocaListItem(
            thumbnail: item.produto.imagemUrl,
            title: item.produto.nome,
            subtitle: '${item.quantidade} ${item.produto.unidade.nome}',
            color: 'green',
            hasImage: hasImage,
            onlyVisible: false,
          ),
        )
        : RocaListItem(
          thumbnail: item.produto.imagemUrl,
          title: item.produto.nome,
          subtitle: '${item.quantidade} ${item.produto.unidade.nome}',
          color: 'orange',
          hasImage: hasImage,
          onlyVisible: !widget.isOpen,
        );
      },
    );
  }

  Widget _buildFamilyProductsGridView(List<ItemListaModel> itens) {
    return LayoutBuilder(builder: (context, constraints) {
      final crossAxisCount = constraints.maxWidth > 700 ? 4 : 2;

      return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: crossAxisCount,
          mainAxisSpacing: 8.0,
          crossAxisSpacing: 8.0,
          childAspectRatio: crossAxisCount == 2 ? 4 / 7 : 6 / 7,
        ),
        itemCount: itens.length,
        itemBuilder: (context, index) {
          final item = itens[index];

          return  widget.isOpen
          ? GestureDetector(
            onTap: () async {
              final result = await Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AddListProductScreen(itemLista: item, listId: widget.list.id!, isEditable: true)),
              );

              if (result == true) {
                DataManager.setHasUnsavedChanges(false);
                _futureItensLista = _fetchItensLista().then((value) {
                  setState(() {
                    totalProdutos = value.length;
                  });
                  return value;
                });
              }
            },
            child: RocaGridItem(
              thumbnail: item.produto.imagemUrl,
              title: item.produto.nome,
              subtitle: '${item.quantidade} ${item.produto.unidade.nome}',
              color: 'green',
              onlyVisible: false,
              hasImage: hasImage,
            )
          )
          : RocaGridItem(
            thumbnail: item.produto.imagemUrl,
            title: item.produto.nome,
            subtitle: '${item.quantidade} ${item.produto.unidade.nome}',
            color: 'orange',
            onlyVisible: !widget.isOpen,
            hasImage: hasImage,
          );
        },
      );
    });
  }

  Widget _buildAllProductsListView(List<ItemListaModel> itens) {

    if (_selectedSortOption == 'Por Família') {
      final Map<String, List<ItemListaModel>> itensPorFamilia = groupBy(itens, (item) => item.familia!.sobrenome);
      return ListView.separated(
        separatorBuilder: (BuildContext context, int index) {
          return SizedBox(height: AppTheme.listItemSpacing);
        },
        scrollDirection: Axis.vertical,
        itemCount: itensPorFamilia.length,
        itemBuilder: (context, index) {
          final familia = itensPorFamilia.keys.elementAt(index);
          final listaDeItensDaFamilia = itensPorFamilia[familia]!;

          return Container(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            decoration: BoxDecoration(
              color: AppTheme.sunrise, // Background color for individual families
              borderRadius: BorderRadius.circular(10.0), // Border radius
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                // Nome da Família
                ListTile(
                  title: Text(
                    familia,
                    style: Theme.of(context).textTheme.headlineLarge!.white,
                    textAlign: TextAlign.center,
                  ),
                ),

                ListView.separated(
                  separatorBuilder: (BuildContext context, int index) {
                    return SizedBox(height: AppTheme.listItemSpacing);
                  },
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: listaDeItensDaFamilia.length,
                  itemBuilder: (context, productIndex) {
                    final item = listaDeItensDaFamilia[productIndex];
                    final productName = item.produto.nome;
                    final productUnity = item.produto.unidade.nome;

                    // return ListTile(
                    //   title: Text('Product: ${entry.key} Quantity: ${entry.value}'),
                    // );
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: AppTheme.listItemSpacing),
                      child: RocaListItem(
                        thumbnail: item.produto.imagemUrl,
                        title: productName,
                        subtitle: '${item.quantidade} $productUnity',
                        color: 'white',
                        hasImage: hasImage,
                        onlyVisible: true,
                      ),
                    );
                  }
                ),
              ]
            ),
          );
        },
      );
    }
    else {
      // Mapa para rastrear a quantidade de cada produto
      final Map<ProdutoModel, int> quantidadesPorProduto = {};

      // Loop para somar as quantidades de cada produto
      for (final item in itens) {
        final produto = item.produto;
        final quantidade = item.quantidade;
        
        bool produtoJaAdicionado = false;
  
        // Verifica se o mesmo produto já foi adicionado ao mapa
        for (final chaveProduto in quantidadesPorProduto.keys) {
          if (chaveProduto.nome == produto.nome) {
            quantidadesPorProduto[chaveProduto] = quantidadesPorProduto[chaveProduto]! + quantidade;
            produtoJaAdicionado = true;
            break;
          }
        }
        
        if (!produtoJaAdicionado) {
          quantidadesPorProduto[produto] = quantidade;
        }
      }

      return ListView.separated(
        separatorBuilder: (BuildContext context, int index) {
          return SizedBox(height: AppTheme.listItemSpacing);
        },
        scrollDirection: Axis.vertical,
        itemCount: quantidadesPorProduto.length,
        itemBuilder: (context, index) {
          final produto = quantidadesPorProduto.keys.elementAt(index);
          final quantidade = quantidadesPorProduto[produto]!;

          return RocaListItem(
            thumbnail: produto.imagemUrl, // Adicione a imagem do produto, se necessário
            title: produto.nome, // Nome do produto
            subtitle: '${quantidade.toString()} ${produto.unidade.nome}', // Quantidade total do produto
            color: 'orange', // Cor, se necessário
            hasImage: hasImage, // Se o produto tem imagem
            onlyVisible: true,
          );
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {

    List<Widget> tabBar = const <Widget>[
                Tab(text: 'Sua Lista',),
                Tab(text: 'Toda Lista'),
              ];

    Widget floatingActionButton = FloatingActionButton(
      shape: const CircleBorder(),
      backgroundColor: AppTheme.land,
      child: const Icon(Icons.add_sharp, color: AppTheme.vanilla, size: 40),
      onPressed: () async {
        final result = await Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => AddListProductScreen(listId: widget.list.id!, isEditable: true)),
        );

        if (result == true) {
          DataManager.setHasUnsavedChanges(false);
          _futureItensLista = _fetchItensLista().then((value) {
            setState(() {
              totalProdutos = value.length;
            });
            return value;
          });
        }

      },
    );

    Widget familyListProductsTab = Stack(
      children: [
        Column(
          children: [
            Row(
              children: [
                AutoSizeText(
                  totalProdutos != null && !_isLoading?
                    ("Total: " +
                      totalProdutos.toString() +
                      (totalProdutos! > 1 ?  ' produtos' : ' produto'))
                  : '' ,
                  maxLines: 2,
                  wrapWords: false,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 24,
                  ),
                ),
                const Spacer(),
                CustomRadioButton(
                  icon:Icons.list,
                  value: DataManager.getIsListView(),
                  onChanged: (newValue) {
                    if (!DataManager.getIsListView()) {
                      setState(() {
                        DataManager.setIsListView(newValue);
                      });
                    }
                  },
                ),
                CustomRadioButton(
                  icon:Icons.grid_view,
                  value: !DataManager.getIsListView(),
                  onChanged: (newValue) {
                    if (DataManager.getIsListView()) {
                      setState(() {
                        DataManager.setIsListView(!newValue);
                      });
                    }
                  },
                ),
              ],
            ),
            Expanded(
              child: FutureBuilder<List<ItemListaModel>>(
                future: _futureItensLista,
                builder:  (context, snapshot) {
                  if (_isLoading) {
                    return const Center(
                      child: CircularProgressIndicator()
                    );
                  } 
                  else if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasData && snapshot.data!.isNotEmpty) {
                      final itens = snapshot.data!;
                      return DataManager.getIsListView() ? _buildFamilyProductsListView(itens) : _buildFamilyProductsGridView(itens);
                    }
                    else {
                      return const Text("Nada na sua lista.");
                    }
                  } else {
                    return Container();
                  }
                },
              )
            ),
          ],
        ),
        if (widget.isOpen) ...{
          Positioned(
            bottom: 16.0,
            right: 16.0,
            child: floatingActionButton,
          ),
        }
      ],
    );

    Widget allListProductsTab = Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0), 
          child: Row(
            children: [
              Expanded(
                child: RocaDropdown(
                  hintText: 'Ordenar',
                  options: (f, cs) => const ['Por Produto', 'Por Família'],
                  value: _selectedSortOption,
                  onChanged: _sortProducts,
                  getLabel: (option) => option,
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: FutureBuilder<List<ItemListaModel>>(
            future: _fetchAllItensLista(),
            builder:  (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(
                  child: CircularProgressIndicator()
                );
              }
              else if (snapshot.hasData && snapshot.data!.isNotEmpty) {
                final itens = snapshot.data!;
                return _buildAllProductsListView(itens);
              }
              else {
                return const Text("Nada na lista.");
              }
            },
          )
        ),
      ],
    );

    List<Widget> tabViews = [
              familyListProductsTab,
              allListProductsTab,
            ];


    return Responsive(
      tabBar: tabBar,
      title: '${widget.list.nome}\n(${DateFormat('dd/MM/yy').format(widget.list.dataInicio)} - ${DateFormat('dd/MM/yy').format(widget.list.dataFim)})',
      tabViews: tabViews,
      hasSideMenu: false,
      isCoordenador: false,
      appBarLandTheme: false,
    );
  }
}
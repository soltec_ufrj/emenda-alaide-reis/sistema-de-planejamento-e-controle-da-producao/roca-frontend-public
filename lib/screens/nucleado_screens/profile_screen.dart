import 'package:flutter/material.dart';
import 'package:roca/app_data.dart';

import '../../constants.dart';
import '../../responsive.dart';
import '../../components/roca_dropdown.dart';
import '../../components/roca_text_input.dart';
import '../../components/roca_button.dart';

import '../../controllers/familia_controller.dart';
import '../../controllers/user_controller.dart';
import '../../models/familia_model.dart';
import '../../models/user_model.dart';
import '../../services/familia_service.dart';
import '../../services/user_service.dart';
import '../add_screens/change_password_screen.dart';


class ProfileScreen extends StatefulWidget {
  static const routeName = '/nucleado/perfil';
  final PessoaModel user;

  const ProfileScreen({
    super.key, 
    required this.user,
  });

  @override
  ProfileScreenState createState() => ProfileScreenState();
}

class ProfileScreenState extends State<ProfileScreen> {
  final userController = UserController(UserService());
  final familiaController = FamiliaController(FamiliaService());

  final _userFormKey = GlobalKey<FormState>();
  late TextEditingController _nomeController;
  int? _familiaValue;
  late TextEditingController _telefoneController;
  late TextEditingController _cpfController;
  late TextEditingController _familiaController;
  late TextEditingController _senhaController;
  late TextEditingController _confirmSenhaController;
  bool? _adminValue;
  List<String> familiasOptions = [];

  @override
  void initState() {
    super.initState();

    _nomeController = TextEditingController(text: widget.user.nome);
    _telefoneController = TextEditingController(text: widget.user.telefone);
    _cpfController = TextEditingController(text: widget.user.usuario.cpf);
    _familiaController = TextEditingController(text: widget.user.familia!.sobrenome);
    _senhaController = TextEditingController();
    _confirmSenhaController = TextEditingController();
    _adminValue = widget.user.funcao?.codigo == "0";

    fetchFamilias();

    _addFormListeners();
  }

  void _addFormListeners() {
    _telefoneController.addListener(_onFormChanged);
    _senhaController.addListener(_onFormChanged);
    _confirmSenhaController.addListener(_onFormChanged);
  }

  @override
  void dispose() {
    _removeFormListeners() ;
    _telefoneController.dispose();
    _senhaController.dispose();
    _confirmSenhaController.dispose();
    super.dispose();
  }

  void _removeFormListeners() {
    _telefoneController.removeListener(_onFormChanged);
    _senhaController.removeListener(_onFormChanged);
    _confirmSenhaController.removeListener(_onFormChanged);
  }

  void fetchFamilias() async {
    setState(() {
      DataManager.setIsLoading(true);
    });

    familiaController.getFamilias().then((responseData) {
      setState(() {
        DataManager.setIsLoading(false);
      });

      if (!mounted) return;

      if (responseData["success"]) {
        final List<FamiliaModel> familias = responseData["data"];

        List<String> sobrenomes = familias.map((familia) => familia.sobrenome).toList();

        setState(() {
          familiasOptions = sobrenomes;
          if (widget.user.familia?.sobrenome != null) {
            _familiaValue = familiasOptions.indexOf(widget.user.familia!.sobrenome);
          }
        });
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }
    }).catchError((error) {

      setState(() {
        DataManager.setIsLoading(false);
      });

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            "Erro ao buscar famílias",
            style: Theme.of(context).textTheme.headlineSmall!.white,
            textAlign: TextAlign.center,
          ),
          duration: AppTheme.messageDuration,
          backgroundColor: AppTheme.danger,
        ),
      );
    });
  }

  void _onFormChanged() {
    final hasUnsavedChanges = _hasFormChanged();
    DataManager.setHasUnsavedChanges(hasUnsavedChanges);
  }

  bool _hasFormChanged() {
    return _telefoneController.text != widget.user.telefone
      || _adminValue != (widget.user.funcao?.codigo == "0");
  }

  void _addItem(BuildContext context) async {
    if (_userFormKey.currentState!.validate()) {
      setState(() {
        DataManager.setIsLoading(true);
      });

      final unmaskedCPF = _cpfController.text.replaceAll(RegExp(r'\D'), '');
      final unmaskedTelefone = _telefoneController.text.replaceAll(RegExp(r'\D'), '');
      final String senha = _senhaController.text;

      final usuario = UsuarioModel(
        cpf: unmaskedCPF,
        senha: senha,
      );

      final familiaModel = FamiliaModel(sobrenome: familiasOptions[_familiaValue!]);

      final funcaoModel = FuncaoModel(
        codigo: _adminValue! ? '0' : '1',
        nome: _adminValue! ? 'Coordenadora' : 'Nucleada',
      );

      final user = PessoaModel(
        usuario: usuario, 
        nome: _nomeController.text, 
        telefone: unmaskedTelefone, 
        familia: familiaModel,
        funcao: funcaoModel,
      );

      final Map<String, dynamic> responseData = await userController.patchUser(user);

      setState(() {
        DataManager.setIsLoading(false);
      });

      if (responseData["success"]) {
        DataManager.setLoggedUser(user);
        Navigator.pop(context, true);
      } else {
        _showSnackBar(context, responseData["message"], AppTheme.danger);
      }
    }
  }

  void _showSnackBar(BuildContext context, String message, Color backgroundColor) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          message, 
          style: Theme.of(context).textTheme.headlineSmall!.white,
          textAlign: TextAlign.center,
        ),
        duration: AppTheme.messageDuration,
        backgroundColor: backgroundColor,
      ),
    );
  }

  Future<bool> _showSelfModificationAlert(context) async {
    if (widget.user.usuario.cpf != DataManager.getLoggedUser()?.usuario.cpf) return true;

    return await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Modificação em si mesmo'),
          content: Text('Você não pode alterar coordenação de si mesmo nem se remover.\nSe realmente deseja fazer isso solicite um outro coordenador.'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text('OK'),
            ),
          ],
        );
      },
    ) ?? false;
  }

  final MaterialStateProperty<Icon?> switchIcon = MaterialStateProperty.resolveWith<Icon?>(
    (Set<MaterialState> states) {
      if (states.contains(MaterialState.selected)) {
        return const Icon(Icons.check);
      }
      return const Icon(Icons.close);
    },
  );

  @override
  Widget build(BuildContext context) {
    Widget body = Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [ 
            Form(
            key: _userFormKey,
              child: Column(
                children: [
                  RocaTextField(
                    enabled: true,
                    readOnly: true,
                    controller: _nomeController, 
                    labelText: 'Nome',
                  ),
                  RocaTextField(
                    enabled: true,
                    readOnly: true,
                    controller: _familiaController,
                    labelText: 'Família',
                  ),
                  RocaTextField(
                    enabled: true,
                    keyboardType: TextInputType.number,
                    controller: _telefoneController, 
                    labelText: 'Telefone',
                    inputFormatters: [AppTheme.phoneMask],
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Telefone é obrigatório';
                      }
                      return null;
                    },
                  ),
                  RocaTextField(
                    enabled: true,
                    readOnly: true,
                    keyboardType: TextInputType.number,
                    controller: _cpfController, 
                    labelText: 'CPF',
                    inputFormatters: [AppTheme.cpfMask],
                  ),
                  
                  RocaButton(
                    text: 'Alterar Senha', 
                    outline: false,
                    buttonColor: AppTheme.land,
                    textStyle: Theme.of(context).textTheme.headlineMedium!.copyWith(color: Colors.white),
                    onTap: () async {
                      if (await DataManager.getHasUnsavedChanges(context)){
                        DataManager.setHasUnsavedChanges(false);
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => ChangePasswordScreen(user: widget.user!.usuario)),
                        ).then((value)=> setState((){}));
                      }
                    }
                  ),
                  RocaButton(
                    text: 'Salvar Alterações', 
                    outline: false,
                    buttonColor: AppTheme.aspargus,
                    textStyle: Theme.of(context).textTheme.headlineMedium!.copyWith(color: Colors.white),
                    onTap: () { _addItem(context); }
                  ),
                ],
              ),
            ),
          ],
        ),
      )
    );

    return Responsive(
      title: 'Meu Perfil',
      body: body,
      backgroundColor: AppTheme.vanilla,
      hasSideMenu: false,
      appBarLandTheme: false,
    );
  }
}

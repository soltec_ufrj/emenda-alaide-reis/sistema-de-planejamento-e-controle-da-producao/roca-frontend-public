import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:diacritic/diacritic.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image/image.dart' as img;
import 'package:roca/models/imagem_model.dart';

import '../../app_data.dart';
import '../../components/roca_image_edit.dart';
import '../../constants.dart';
import '../../responsive.dart';
import '../../components/roca_text_input.dart';
import '../../components/roca_dropdown.dart';
import '../../components/roca_button.dart';

import '../../models/produto_model.dart';
import '../../controllers/produto_controller.dart';
import '../../services/produto_service.dart';

class AddProductScreen extends StatefulWidget {
  final bool hasImage;
  final bool? edit;
  final ProdutoModel? produto;

  const AddProductScreen({
    super.key,
    required this.hasImage,
    this.edit,
    this.produto,
  });

  @override
  AddProductScreenState createState() => AddProductScreenState();
}

class AddProductScreenState extends State<AddProductScreen> {
  final produtoController = ProdutoController(ProdutoService());

  final _productFormKey = GlobalKey<FormState>();
  late TextEditingController _nomeController = TextEditingController();
  late Map<String, String> tiposOptions = {};
  String? _tipoValue;
  late Map<String, String> unidadesOptions = {};
  String? _unidadeValue;

  File _imagem = File('assets/img/placeholder.jpg');
  bool imagemAlterada = false;
  final picker = ImagePicker();
  XFile? _imagemEscolhida;

  void fetchTipoProdutos() async {
    setState(() {
      DataManager.setIsLoading(true);
    });
    

    Map<String, String> tipos = {};

    produtoController.getTipos().then((responseData) {
      setState(() {
        DataManager.setIsLoading(false);
      });

      if (!mounted) return;


      if (responseData["success"]) {
        final fetchedTipos = responseData["data"];
        for (TipoProdutoModel tipo in fetchedTipos) {
          tipos[tipo.codigo] = tipo.nome; 
        }

        setState(() {
          tiposOptions = tipos;
          if (widget.produto?.tipoProduto != null) {
            _tipoValue = widget.produto!.tipoProduto?.codigo;
          }
        });
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }
    }).catchError((error) {

      setState(() {
        DataManager.setIsLoading(false);
      });

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            "Erro ao buscar tipos de produto.",
            style: Theme.of(context).textTheme.headlineSmall!.white,
            textAlign: TextAlign.center,
          ),
          duration: AppTheme.messageDuration,
          backgroundColor: AppTheme.danger,
        ),
      );
    });    
  }

  void fetchUnidades() async {
    setState(() {
      DataManager.setIsLoading(true);
    });
    

    Map<String, String> unidades = {};

    produtoController.getUnidades().then((responseData) {
      setState(() {
        DataManager.setIsLoading(false);
      });

      if (!mounted) return;



      if (responseData["success"]) {
        final fetchedUnidades = responseData["data"];

        for (UnidadeModel unidade in fetchedUnidades) {
          unidades[unidade.codigo] = unidade.nome; 
        }

        setState(() {
          unidadesOptions = unidades;
        if (widget.produto?.unidade != null) {
            _unidadeValue = widget.produto!.unidade.codigo;
        }
        });
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }  
    }).catchError((error) {

      setState(() {
        DataManager.setIsLoading(false);
      });

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            "Erro ao buscar unidades.",
            style: Theme.of(context).textTheme.headlineSmall!.white,
            textAlign: TextAlign.center,
          ),
          duration: AppTheme.messageDuration,
          backgroundColor: AppTheme.danger,
        ),
      );
    });   
  }

  @override
  void initState() {
    super.initState();

    fetchTipoProdutos();
    fetchUnidades();

    _nomeController = TextEditingController(text: widget.produto?.nome);

    _addFormListeners();
  }

  void _addFormListeners() {
    _nomeController.addListener(_onFormChanged);
  }

  @override
  void dispose() {
    _removeFormListeners();
    _nomeController.dispose();
    super.dispose();
  }

  void _removeFormListeners() {
    _nomeController.removeListener(_onFormChanged);
  }

  void _onFormChanged() {
    final hasUnsavedChanges = _hasFormChanged();
    DataManager.setHasUnsavedChanges(hasUnsavedChanges);
  }

  bool _hasFormChanged() {
    if (widget.produto == null) {
      return _nomeController.text.isNotEmpty ||
          _unidadeValue != null ||
          _tipoValue != null ||
          imagemAlterada;
    } else {
      return _nomeController.text != widget.produto?.nome ||
          _unidadeValue != widget.produto!.unidade.codigo ||
          _tipoValue != widget.produto!.tipoProduto?.codigo ||
          imagemAlterada;
    }
  }

  void _addItem(BuildContext context) async {
    final String nome = _nomeController.text;
    String? tipo = _tipoValue;
    String? unidade = _unidadeValue;

    // salva os valores dos campos do formulário para atualizar seu status
    _productFormKey.currentState!.save();

    if (_productFormKey.currentState!.validate()) {
      setState(() {
        DataManager.setIsLoading(true);
      });
      TipoProdutoModel tipoModel;
      UnidadeModel unidadeModel;

      if (tipo != null && tipo != '') {
        tipoModel = TipoProdutoModel(
          codigo: tipo,
          nome: tiposOptions[tipo]!,
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              'Por favor selecione o tipo do produto.',
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
        return;
      }

      if (unidade != null && unidade != '') {
        unidadeModel = UnidadeModel(
          codigo: unidade,
          nome: unidadesOptions[unidade]!,
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              'Por favor selecione a unidade.',
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
        return;
      }

      ImagemModel? imagem = null;

      if (imagemAlterada) {
        final imagemComprimida = compressAndResizeImage(_imagem);

        final extensao = imagemComprimida.path.split('.').last;

        Uint8List bytesImagem = await imagemComprimida.readAsBytes();

        String jsonImagem = base64.encode(bytesImagem);

        imagem = ImagemModel(
            imagem: jsonImagem,
            nomeArquivo: nome + unidadesOptions[unidade]!,
            extensao: extensao
        );
      }

      final produto = ProdutoModel(
          id: widget.produto?.id,
          nome: nome,
          unidade: unidadeModel,
          tipoProduto: tipoModel,
          imagem: imagem
      );

      final Map<String, dynamic> responseData;
      if (widget.produto == null) {
        responseData = await produtoController.postProduto(produto);
      }
      else {
        responseData = await produtoController.patchProduto(produto);
      }

      setState(() {
        DataManager.setIsLoading(false);
      });

      if (responseData["success"]) {
        Navigator.pop(context, true);
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"],
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }
    }
  }

  Future<void> _removeItem(BuildContext context) async {
    final Map<String, dynamic> responseData;

    if (widget.produto != null) {
      setState(() {
        DataManager.setIsLoading(true);
      });
      responseData = await produtoController.deleteProduto(widget.produto!);
    }
    else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Erro ao deletar o produto.',
            style: Theme.of(context).textTheme.headlineSmall!.white,
            textAlign: TextAlign.center,
          ),
          duration: AppTheme.messageDuration,
          backgroundColor: AppTheme.danger,
        ),
      );
      return;
    }

    setState(() {
      DataManager.setIsLoading(false);
    });
    
    if (responseData["success"]) {
      Navigator.pop(context, true);
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            responseData["message"], 
            style: Theme.of(context).textTheme.headlineSmall!.white,
            textAlign: TextAlign.center,
          ),
          duration: AppTheme.messageDuration,
          backgroundColor: AppTheme.danger,
        ),
      );
    }
  }

  Future _selecionarImagemDialog(BuildContext context) async {
    await showCupertinoModalPopup(
      context: context,
      builder: (context) =>
          CupertinoActionSheet(
            cancelButton: CupertinoActionSheetAction(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text('Cancelar', style: TextStyle(color: AppTheme.land)),
                  Icon(
                    Icons.close,
                    color: Colors.red,
                  ),
                ],
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            title: Text(
              'Selecionar imagem',
              style: TextStyle(color: AppTheme.land, fontSize: 20),
            ),
            actions: [
              CupertinoActionSheetAction(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text('Galeria', style: TextStyle(color: AppTheme.land)),
                    Icon(
                      Icons.image_search,
                      color: AppTheme.land,
                    ),
                  ],
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                  selecionarOuTirarFoto(ImageSource.gallery);
                },
              ),
              CupertinoActionSheetAction(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text('Câmera', style: TextStyle(color: AppTheme.land)),
                    Icon(
                      Icons.camera_alt_outlined,
                      color: AppTheme.land,
                    ),
                  ],
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                  selecionarOuTirarFoto(ImageSource.camera);
                },
              ),
            ],
          ),
    );
  }

  Future selecionarOuTirarFoto(ImageSource imageSource) async {
    final imagemEscolhida = await picker.pickImage(source: imageSource);

    setState(() {
      if (imagemEscolhida != null) {
        _imagemEscolhida = imagemEscolhida;

        _cortarImagem();
      } else
        print('Nenhuma imagem foi selecionada ou tirada.');
    });
  }

  Future _cortarImagem() async {
    final imagemCortada = await ImageCropper().cropImage(
      sourcePath: _imagemEscolhida!.path,
      uiSettings: [
        AndroidUiSettings(
          toolbarTitle: 'Cortar Imagem',
          toolbarColor: AppTheme.vanilla,
          toolbarWidgetColor: AppTheme.land,
          initAspectRatio: CropAspectRatioPreset.original,
          lockAspectRatio: true,
          aspectRatioPresets: [
            CropAspectRatioPreset.square,
          ],
        ),
        IOSUiSettings(
          title: 'Cortar Imagem',
          aspectRatioLockEnabled: true,
          aspectRatioPresets: [
            CropAspectRatioPreset.square,
          ],
        ),
      ],
    );

    setState(() {
      if (imagemCortada != null) {
        _imagem = File(imagemCortada.path);
        imagemAlterada = true;
        _onFormChanged();
      }
    });
  }

  File compressAndResizeImage(File file) {
    img.Image? image = img.decodeImage(file.readAsBytesSync());

    // Resize the image to have the longer side be 800 pixels
    int width;
    int height;

    if (image!.width > image.height) {
      width = 800;
      height = (image.height / image.width * 800).round();
    } else {
      height = 800;
      width = (image.width / image.height * 800).round();
    }

    img.Image resizedImage = img.copyResize(image, width: width, height: height);

    // Compress the image with JPEG format
    List<int> compressedBytes = img.encodeJpg(resizedImage, quality: 85); // Adjust quality as needed

    // Save the compressed image to a file
    File compressedFile = File(file.path.replaceFirst('.jpg', '_compressed.jpg'));
    compressedFile.writeAsBytesSync(compressedBytes);

    return compressedFile;
  }

  Future<bool> _showRemoveConfirmationAlert(context) async {
    return await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Tem certeza?'),
          content: Text(
              'Você esta preste a remover um produto. Tem certeza que quer removê-lo?'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.of(context).pop(false),
              child: Text('Não'),
            ),
            TextButton(
              onPressed: () => Navigator.of(context).pop(true),
              child: Text('Sim'),
            ),
          ],
        );
      },
    ) ?? false;
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Center(
      child: SingleChildScrollView(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Form(
              key: _productFormKey,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // Imagem
                    GestureDetector(
                      onTap: () async => await _selecionarImagemDialog(context),
                      child: RocaImageEdit(
                          imageUrl: widget.produto == null
                              ? ""
                              : widget.produto!.imagemUrl,
                          imagem: FileImage(_imagem),
                          imagemAlterada: imagemAlterada),
                    ),

                    // Nome
                    RocaTextField(
                      enabled: widget.edit ?? true,
                      controller: _nomeController,
                      labelText: 'Nome',
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Nome é obrigatório';
                        }
                        return null;
                      },
                    ),

                    // Unidade
                    RocaDropdown(
                      searchable: true,
                      hintText: 'Unidade',
                      options: (f, cs) => unidadesOptions.keys.toList(),
                      value: _unidadeValue,
                      itemBuilder: (context, item, isDisabled, isSelected) {
                        return ListTile(
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 8, horizontal: 12),
                          title: Text(
                            unidadesOptions[item]!,
                            style: TextStyle(
                                color: Colors.black, fontSize: 18),
                          ),
                        );
                      },
                      getLabel: (value) {
                        return removeDiacritics(unidadesOptions[value]!);
                      },
                      onChanged: (newValue) {
                        setState(() {
                          _unidadeValue = newValue;
                          _onFormChanged();
                        });
                      },
                      validator: (value) {
                        if (value == null && _unidadeValue == null) {
                          return 'Unidade é obrigatória';
                        }
                        return null;
                      },
                    ),

                    // Tipo de Produto
                    RocaDropdown(
                      hintText: 'Tipo de Produto',
                      options: (f, cs) => tiposOptions.keys.toList(),
                      value: _tipoValue,
                      getLabel: (value) {
                        return tiposOptions[value] ?? "";
                      },
                      onChanged: (newValue) {
                        setState(() {
                          _tipoValue = newValue;
                          _onFormChanged();
                        });
                      },
                      validator: (value) {
                        if (value == null && _tipoValue == null) {
                          return 'Tipo de produto é obrigatório';
                        }
                        return null;
                      },
                    ),

                    // Editar Produto
                    if (widget.edit == true)
                      RocaButton(
                          text: 'Salvar Alterações',
                          outline: false,
                          buttonColor: AppTheme.aspargus,
                          textStyle:
                              Theme.of(context).textTheme.headlineMedium!.white,
                          onTap: () {
                            _addItem(context);
                          }),

                    // Remover Produto
                    if (widget.edit == true)
                      RocaButton(
                          text: 'Remover Produto',
                          outline: false,
                          buttonColor: AppTheme.danger,
                          textStyle:
                              Theme.of(context).textTheme.headlineMedium!.white,
                          onTap: () async {
                            if (await _showRemoveConfirmationAlert(context))
                              _removeItem(context);
                          }
                      ),

                    // Cadastrar Produto
                    if (widget.edit == null)
                      RocaButton(
                          text: 'Cadastrar Produto',
                          outline: false,
                          buttonColor: AppTheme.aspargus,
                          textStyle:
                              Theme.of(context).textTheme.headlineMedium!.white,
                          onTap: () {
                            _addItem(context);
                          }),
                  ],
                ),
              ))
        ]),
      ),
    );

    return Responsive(
      title: widget.edit == null
          ? 'Cadastrar Produto'
          : (widget.edit == true ? 'Editar Produto' : 'Ver Produto'),
      body: body,
      backgroundColor: AppTheme.vanilla,
      hasSideMenu: false,
      appBarLandTheme: false,
    );
  }
}

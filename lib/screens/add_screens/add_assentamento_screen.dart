import 'package:flutter/material.dart';

import '../../app_data.dart';
import '../../constants.dart';
import '../../responsive.dart';
import '../../components/roca_text_input.dart';
import '../../components/roca_dropdown.dart';
import '../../components/roca_button.dart';

import '../../models/assentamento_model.dart';
import '../../controllers/cep_controller.dart';
import '../../controllers/assentamento_controller.dart';
import '../../services/cep_service.dart';
import '../../services/assentamento_service.dart';


class AddAssentamentoScreen extends StatefulWidget {
  final bool hasImage;
  final bool? edit;
  final AssentamentoModel? assentamento;

  const AddAssentamentoScreen({
    super.key, 
    required this.hasImage,
    this.edit,
    this.assentamento,
  });

  @override
  AddAssentamentoScreenState createState() => AddAssentamentoScreenState();
}

class AddAssentamentoScreenState extends State<AddAssentamentoScreen> {
  final assentamentoController = AssentamentoController(AssentamentoService());
  final cepController = CepController(CepService());


  final _assentamentoFormKey = GlobalKey<FormState>();
  late TextEditingController _nomeController = TextEditingController();
  late TextEditingController _sobreController = TextEditingController();
  String? _cidadeValue;
  late Map<String, String> cidadesOptions = {};
  int? _estadoValue;
  late List<String> estadosOptions = [];

  void fetchEstados() async {
    setState(() {
      DataManager.setIsLoading(true);
    });

    List<String> estados = [];

    cepController.getEstados().then((responseData) {
      setState(() {
        DataManager.setIsLoading(false);
      });

      if (!mounted) return;


      if (responseData["success"]) {
        final fetchedEstados = responseData["data"];

        for (EstadoModel estado in fetchedEstados) {
          estados.add(estado.nome); 
        }

        setState(() {
          estadosOptions = estados;
          if (widget.assentamento?.cep?.estado != null) {
            _estadoValue = estadosOptions.indexOf(widget.assentamento!.cep!.estado!.nome);
          }
        });
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }
    }).catchError((error) {

      setState(() {
        DataManager.setIsLoading(false);
      });

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            "Erro ao buscar estados.",
            style: Theme.of(context).textTheme.headlineSmall!.white,
            textAlign: TextAlign.center,
          ),
          duration: AppTheme.messageDuration,
          backgroundColor: AppTheme.danger,
        ),
      );
    });
      
  }



  Future<void> fetchCidadesPorEstado(String estado) async {
    setState(() {
      DataManager.setIsLoading(true);
    });

    Map<String, String> cidades = {};

    cepController.getCidadesPorEstado(estado).then((responseData) {
      setState(() {
        DataManager.setIsLoading(false);
      });

      if (!mounted) return;



      if (responseData["success"]) {
        final fetchedCeps = responseData["data"];

        for (CepModel cep in fetchedCeps) {
          cidades[cep.id] = cep.cidade;
        }

        setState(() {
          cidadesOptions = cidades;
          if (widget.assentamento?.cep?.id != null && cidadesOptions.containsKey(widget.assentamento!.cep!.id)) {
            _cidadeValue = widget.assentamento!.cep!.id;
          }
        });
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }
    }).catchError((error) {

      setState(() {
        DataManager.setIsLoading(false);
      });

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            "Erro ao buscar cidades.",
            style: Theme.of(context).textTheme.headlineSmall!.white,
            textAlign: TextAlign.center,
          ),
          duration: AppTheme.messageDuration,
          backgroundColor: AppTheme.danger,
        ),
      );
    });
  }

  @override
  void initState() {
    super.initState();

    fetchEstados();

    if (widget.assentamento?.cep?.estado != null) {
      fetchCidadesPorEstado(widget.assentamento!.cep!.estado!.nome);
    }

    _nomeController = TextEditingController(text: widget.assentamento?.nome);
    _sobreController = TextEditingController(text: widget.assentamento?.descricao);

    _addFormListeners();
  }

  void _addFormListeners() {
    _nomeController.addListener(_onFormChanged);
    _sobreController.addListener(_onFormChanged);
  }

  @override
  void dispose() {
    _removeFormListeners() ;
    _nomeController.dispose();
    _sobreController.dispose();
    super.dispose();
  }

  void _removeFormListeners() {
    _nomeController.removeListener(_onFormChanged);
    _sobreController.removeListener(_onFormChanged);
  }

  void _onFormChanged() {
    final hasUnsavedChanges = _hasFormChanged();
    DataManager.setHasUnsavedChanges(hasUnsavedChanges);
  }

  bool _hasFormChanged() {
    if (widget.assentamento == null) {
      return _nomeController.text.isNotEmpty
          || _sobreController.text.isNotEmpty
          || _estadoValue != null
          || _cidadeValue != null;
    } else {
      return _nomeController.text != widget.assentamento?.nome
          || _sobreController.text != widget.assentamento?.descricao
          || _estadoValue != estadosOptions.indexOf(widget.assentamento!.cep!.estado!.nome)
          || _cidadeValue != widget.assentamento!.cep!.id;
    }
  }

  void _addItem(BuildContext context)  async{
    final String nome = _nomeController.text;
    String? cidade = _cidadeValue;
    final String sobre = _sobreController.text;

    // salva os valores dos campos do formulário para atualizar seu status
    _assentamentoFormKey.currentState!.save();

    if (_assentamentoFormKey.currentState!.validate()) {
      
      CepModel cepModel;

      if (cidade != null && cidade != ''){
        cepModel = CepModel(id: cidade, cidade: cidadesOptions[cidade]!);
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('Por favor selecione a cidade.', style: Font.apply(Colors.white, FontStyle.regular, FontSize.h6)),
          ),
        );
        return;
      }
      setState(() {
        DataManager.setIsLoading(true);
      });
      final assentamento = AssentamentoModel(
        nome: nome,
        descricao: sobre,
        cep: cepModel,
      );

      final Map<String, dynamic> responseData;
      if (widget.assentamento == null) {
        responseData = await assentamentoController.postAssentamento(assentamento);
      }
      else {
        responseData = await assentamentoController.patchAssentamento(assentamento);
      }

      setState(() {
        DataManager.setIsLoading(false);
      });

      if (responseData["success"]) {
        Navigator.pop(context, true);
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }

    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Por favor preencha todos os campos obrigatórios.', 
            style: Theme.of(context).textTheme.headlineSmall!.white,
            textAlign: TextAlign.center,
          ),
          duration: AppTheme.messageDuration,
          backgroundColor: AppTheme.danger,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [ 
            Form(
              key: _assentamentoFormKey,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // Nome
                    RocaTextField(
                      controller: _nomeController, 
                      labelText: 'Nome',
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Nome é obrigatório';
                        }
                        return null;
                      },
                    ),

                    // Estado
                    RocaDropdown(
                      hintText: 'Estado',
                      options: (f, cs) => List.generate(estadosOptions.length, (index) => index),
                      value: _estadoValue,
                      getLabel: (value) {
                        return estadosOptions[value];
                      },
                      onChanged: (newValue) {
                        setState(() {
                          _estadoValue = newValue;
                          _cidadeValue = null;
                          cidadesOptions = {};
                          _onFormChanged();
                        });
                        if (newValue != null) {
                          fetchCidadesPorEstado(estadosOptions[newValue]);
                        }
                      },
                      validator: (value) {
                        if (value == null && _estadoValue == null) {
                          return 'Estado é obrigatório';
                        }
                        return null;
                      },
                    ),

                    // Cidade
                    RocaDropdown(
                      hintText: 'Cidade',
                      options: (f, cs) => cidadesOptions.keys.toList(),
                      value: _cidadeValue,
                      getLabel: (value) {
                        return cidadesOptions[value] ?? "";
                      },
                      onChanged: (newValue) {
                        setState(() {
                          _cidadeValue = newValue;
                          _onFormChanged();
                        });
                      },
                      validator: (value) {
                        if (value == null && _cidadeValue == null) {
                          return 'Cidade é obrigatório';
                        }
                        return null;
                      },
                    ),

                    // Sobre
                    RocaTextField(
                      controller: _sobreController, 
                      labelText: 'Sobre',
                    ),
                    
                    // Editar Assentamento
                    if (widget.edit == true) 
                      RocaButton(
                        text: 'Salvar Alterações', 
                        outline: false,
                        buttonColor: AppTheme.aspargus,
                        textStyle: Theme.of(context).textTheme.headlineMedium!.white,
                        onTap: () { _addItem(context);}
                      ),
                    
                    // Cadastrar Assentamento
                    if (widget.edit == null)
                      RocaButton(
                        text: 'Cadastrar Assentamento', 
                        outline: false,
                        buttonColor: AppTheme.aspargus,
                        textStyle: Theme.of(context).textTheme.headlineMedium!.white, 
                        onTap: () { _addItem(context);}
                      ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );

    return Responsive(
      title: widget.edit == null
        ? 'Cadastrar Assentamento'
        : (widget.edit == true ? 'Editar Assentamento' : 'Ver Assentamento'),
      body: body,
      backgroundColor: AppTheme.vanilla,
      hasSideMenu: false,
      appBarLandTheme: false,
    );
  }
}

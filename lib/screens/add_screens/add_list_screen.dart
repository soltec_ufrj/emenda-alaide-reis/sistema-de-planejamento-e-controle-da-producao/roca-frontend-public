import 'package:flutter/material.dart';

import '../../constants.dart';
import '../../app_data.dart';
import '../../responsive.dart';

import '../../models/lista_model.dart';
import '../../controllers/lista_controller.dart';
import '../../services/lista_service.dart';

import '../../components/roca_datepicker.dart';
import '../../components/roca_text_input.dart';
import '../../components/roca_button.dart';


class AddListScreen extends StatefulWidget {
  final bool hasImage;
  final bool? edit;
  final ListaModel? lista;

  const AddListScreen({
    super.key,
    required this.hasImage,
    this.edit,
    this.lista,
  });

  @override
  AddListScreenState createState() => AddListScreenState();
}

class AddListScreenState extends State<AddListScreen> {
  final listaController = ListaController(ListaService());

  final _listFormKey = GlobalKey<FormState>();
  late TextEditingController _nomeController;
  DateTime today = DateTime.now();
  DateTime _inicioDate = DateTime.now();
  DateTime? _fimDate;
  bool? status;

  @override
  void initState() {
    super.initState();
    _nomeController = TextEditingController(text: widget.lista?.nome);

    _inicioDate = widget.lista?.dataInicio ?? DateTime(_inicioDate.year, _inicioDate.month, _inicioDate.day);

    if (widget.lista?.dataFim != null) {
      _fimDate = widget.lista!.dataFim;
      status = _checkStatus();
    }

    _addFormListeners();
  }


  void _addFormListeners() {
    _nomeController.addListener(_onFormChanged);
  }

  @override
  void dispose() {
    _removeFormListeners() ;
    _nomeController.dispose();
    super.dispose();
  }

  void _removeFormListeners() {
    _nomeController.removeListener(_onFormChanged);
  }

  void _onFormChanged() {
    final hasUnsavedChanges = _hasFormChanged();
    DataManager.setHasUnsavedChanges(hasUnsavedChanges);
  }

  bool _hasFormChanged() {
    if (widget.lista == null) {
      return _nomeController.text.isNotEmpty
          || DateTime(_inicioDate.year, _inicioDate.month, _inicioDate.day) != DateTime(today.year, today.month, today.day)
          || _fimDate != null;
    } else {
      return _nomeController.text != widget.lista?.nome
          || _inicioDate != widget.lista?.dataInicio
          || _fimDate != widget.lista?.dataFim;
    }
  }

  bool _checkStatus () {
    if (_fimDate != null) {
      return _fimDate!.isAfter(today)
              && 
              (_inicioDate.isBefore(today)
                ||
                _inicioDate.isAtSameMomentAs(today)
              );
    }
    return true;

  }

  void _addItem(BuildContext context) async {
    final String nome = _nomeController.text;
    final DateTime dataInicio = _inicioDate;
    final DateTime? dataFim = _fimDate;

    // salva os valores dos campos do formulário para atualizar seu status
    _listFormKey.currentState!.save();

    if (_listFormKey.currentState!.validate()) {
      setState(() {
        DataManager.setIsLoading(true);
      });
      ListaModel lista = ListaModel(
        id: widget.lista?.id,
        nome: nome,
        dataInicio: dataInicio,
        dataFim: dataFim!,
        criador: DataManager.getLoggedUser()!.usuario.cpf,
      );

      final Map<String, dynamic> responseData;
      if (widget.lista == null) {
        responseData = await listaController.postLista(lista);
      }
      else {
        responseData = await listaController.patchLista(lista);
      }

      setState(() {
        DataManager.setIsLoading(false);
      });

      if (responseData["success"]) {
        Navigator.pop(context, {'lista': lista, 'changesSaved': true});
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"],
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Por favor preencha todos os campos.',
            style: Theme.of(context).textTheme.headlineSmall!.white,
            textAlign: TextAlign.center,
          ),
          duration: AppTheme.messageDuration,
          backgroundColor: AppTheme.danger,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [ 
            Form(
              key: _listFormKey,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // Nome
                    RocaTextField(
                      enabled: widget.edit ?? true,
                      controller: _nomeController,
                      labelText: 'Nome',
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Nome é obrigatório';
                        }
                        return null;
                      },
                    ),
                    
                    // Início
                    RocaDatePicker(
                      enabled: widget.edit ?? true,
                      label: 'Início',
                      initialDate: _inicioDate,
                      onDateSelected: (DateTime inicioDate) {
                        setState(() {
                          _inicioDate = DateTime(inicioDate.year, inicioDate.month, inicioDate.day, 0, 0, 0);
                          _onFormChanged();
                        });
                      },
                      validator: (value) {
                        if (value == null) {
                          return 'Início é obrigatório';
                        }
                        return null;
                      },
                    ),

                    // Fim
                    RocaDatePicker(
                      key: ValueKey(_fimDate),
                      enabled: widget.edit ?? true,
                      label: 'Fim',
                      initialDate: _fimDate,
                      onDateSelected: (DateTime fimDate) {
                        setState(() {
                          _fimDate = DateTime(fimDate.year, fimDate.month, fimDate.day, 23, 59, 59);
                          status = _checkStatus();
                          _onFormChanged();
                        });
                      },
                      validator: (value) {
                        if (value == null) {
                          return 'Fim é obrigatório';
                        }
                        return null;
                      },
                    ),

                    // Status e Botão Fechar Lista
                    if (status != null) 
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Text(
                              'Status: ${status! ? 'Aberta' : 'Fechada'} ${(_fimDate != null && _fimDate?.hour != 23 && _fimDate?.minute != 59) ? ' manualmente às ${_fimDate!.hour}:${_fimDate!.minute} hrs' : ''}',
                              style: Theme.of(context).textTheme.headlineSmall!.land,
                            ),
                      ),
                    if (widget.edit == true && status == true)
                      RocaButton(
                        text: 'Fechar Lista',
                        outline: false,
                        buttonColor: AppTheme.danger,
                        textStyle: Theme.of(context).textTheme.headlineMedium!.white,
                        onTap: () {
                          setState(() {
                            _fimDate = today;
                            status = false;
                            _onFormChanged();
                          });
                        },
                      ),

                    // Editar Lista
                    if (widget.edit == true)
                      RocaButton(
                        text: 'Salvar Alterações',
                        outline: false,
                        buttonColor: AppTheme.aspargus,
                        textStyle: Theme.of(context).textTheme.headlineMedium!.white,
                        onTap: () {
                          _addItem(context);
                        },
                      ),
                    
                    // Cadastrar Lista
                    if (widget.edit == null)
                      RocaButton(
                        text: 'Cadastrar Lista',
                        outline: false,
                        buttonColor: AppTheme.aspargus,
                        textStyle: Theme.of(context).textTheme.headlineMedium!.white,
                        onTap: () {
                          _addItem(context);
                        },
                      ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );

    return Responsive(
      title: widget.edit == null
        ? 'Cadastrar Lista'
        : (widget.edit == true ? 'Editar Lista' : 'Ver Lista'),
      body: body,
      backgroundColor: AppTheme.vanilla,
      hasSideMenu: false,
      appBarLandTheme: false,
    );
  }
}

import 'package:flutter/material.dart';

import '../../app_data.dart';
import '../../constants.dart';
import '../../responsive.dart';
import '../../components/roca_text_input.dart';
import '../../components/roca_button.dart';

import '../../controllers/user_controller.dart';
import '../../models/user_model.dart';
import '../../services/user_service.dart';
import '../coordenador_screens/coordenador_users_screen.dart';


class ChangePasswordScreen extends StatefulWidget {
  final UsuarioModel user;

  const ChangePasswordScreen({
    super.key, 
    required this.user,
  });

  @override
  ChangePasswordScreenState createState() => ChangePasswordScreenState();
}

class ChangePasswordScreenState extends State<ChangePasswordScreen> {
  final userController = UserController(UserService());
  
  final _passwordFormKey = GlobalKey<FormState>();
  late TextEditingController _senhaController = TextEditingController();
  late TextEditingController _confirmSenhaController = TextEditingController();


  void _changePassword(BuildContext context) async {
    final String senha = _senhaController.text;


    if (_passwordFormKey.currentState!.validate()) {

      setState(() {
        DataManager.setIsLoading(true);
      });

      final usuario = UsuarioModel(
        cpf: widget.user.cpf, 
        senha: senha
      );

      final Map<String, dynamic> responseData = await userController.changePassword(usuario);

      setState(() {
        DataManager.setIsLoading(false);
      });

      if (responseData["success"]) {
        DataManager.setHasUnsavedChanges(false);
        Navigator.of(context).pushNamed(CoordenadorUsersScreen.routeName);
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _addFormListeners();
  }

  void _addFormListeners() {
    _senhaController.addListener(_onFormChanged);
    _confirmSenhaController.addListener(_onFormChanged);
  }

  @override
  void dispose() {
    _removeFormListeners() ;
    _senhaController.dispose();
    _confirmSenhaController.dispose();
    super.dispose();
  }

  void _removeFormListeners() {
    _senhaController.removeListener(_onFormChanged);
    _confirmSenhaController.removeListener(_onFormChanged);
  }

  void _onFormChanged() {
    final hasUnsavedChanges = _hasFormChanged();
    DataManager.setHasUnsavedChanges(hasUnsavedChanges);
  }

  bool _hasFormChanged() {
    return _senhaController.text.isNotEmpty
        || _confirmSenhaController.text.isNotEmpty;
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [ 
            Form(
              key: _passwordFormKey,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Column(
                  children: [
                    // Senha
                    RocaTextField(
                      controller: _senhaController, 
                      labelText: 'Senha',
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Senha é obrigatório';
                        }
                        return null;
                      },
                    ),

                    // Confirmar Senha 
                    RocaTextField(
                      controller: _confirmSenhaController, 
                      labelText: 'Confirmar Senha',
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Confirmar Senha é obrigatório';
                        }
                        else if (value != _senhaController.text) {
                          return 'Senha precisa ser igual acima';
                        }
                        return null;
                      },
                    ),
                    
                    
                    // Alterar Senha
                    RocaButton(
                      text: 'Salvar Alterações', 
                      outline: false,
                      buttonColor: AppTheme.aspargus,
                      textStyle: Theme.of(context).textTheme.headlineMedium!.white,
                      onTap: () { _changePassword(context);}
                    ),
                    
                  ],
                ),
              )
            )
          ],
        ),
      ),
    );

    return Responsive(
      title: 'Alterar Senha',
      body: body,
      backgroundColor: AppTheme.vanilla,
      hasSideMenu: false,
      appBarLandTheme: false,
    );
  }
}

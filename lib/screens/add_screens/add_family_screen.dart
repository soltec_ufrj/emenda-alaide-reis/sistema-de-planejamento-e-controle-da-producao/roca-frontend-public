import 'package:flutter/material.dart';

import '../../app_data.dart';
import '../../constants.dart';
import '../../responsive.dart';
import '../../components/roca_dropdown.dart';
import '../../components/roca_text_input.dart';
import '../../components/roca_button.dart';

import '../../models/familia_model.dart';
import '../../models/assentamento_model.dart';
import '../../controllers/familia_controller.dart';
import '../../controllers/assentamento_controller.dart';
import '../../services/familia_service.dart';
import '../../services/assentamento_service.dart';


class AddFamilyScreen extends StatefulWidget {
  final bool hasImage;  
  final bool? edit;
  final FamiliaModel? familia;

  const AddFamilyScreen({
    super.key, 
    required this.hasImage,
    this.edit,
    this.familia,    
  });

  @override
  AddFamilyScreenState createState() => AddFamilyScreenState();
}

class AddFamilyScreenState extends State<AddFamilyScreen> {
  final familiaController = FamiliaController(FamiliaService());
  final assentamentoController = AssentamentoController(AssentamentoService());
  
  final _familyFormKey = GlobalKey<FormState>();
  late TextEditingController _nomeController = TextEditingController();
  int? _assentamentoValue;
  late TextEditingController _numeroLoteController = TextEditingController();
  late TextEditingController _enderecoController = TextEditingController();
  late List<String> assentamentosOptions = [];

  void fetchAssentamentos() async {
    setState(() {
      DataManager.setIsLoading(true);
    });
    
    List<String> assentamentos = [];

    assentamentoController.getAssentamentos().then((responseData) {
      setState(() {
        DataManager.setIsLoading(false);
      });

      if (!mounted) return;



      if (responseData["success"]) {
        final fetchedAssentamentos = responseData["data"];

        for (AssentamentoModel assentamento in fetchedAssentamentos) {
          assentamentos.add(assentamento.nome); 
        }

        setState(() {
          assentamentosOptions = assentamentos;
          if (widget.familia?.assentamento?.nome != null) {
            _assentamentoValue = assentamentosOptions.indexOf(widget.familia!.assentamento!.nome);
          }
        });
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }
    }).catchError((error) {

      setState(() {
        DataManager.setIsLoading(false);
      });

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            "Erro ao buscar assentamentos.",
            style: Theme.of(context).textTheme.headlineSmall!.white,
            textAlign: TextAlign.center,
          ),
          duration: AppTheme.messageDuration,
          backgroundColor: AppTheme.danger,
        ),
      );
    });
      
  }

  @override
  void initState() {
    super.initState();

    fetchAssentamentos();

    _nomeController = TextEditingController(text: widget.familia?.sobrenome);
    _numeroLoteController = TextEditingController(
      text: widget.familia?.numeroLote != null 
          ? widget.familia!.numeroLote.toString() 
          : '',
    );
    _enderecoController = TextEditingController(text: widget.familia?.endereco);

    _addFormListeners();
  }

  void _addFormListeners() {
    _nomeController.addListener(_onFormChanged);
    _numeroLoteController.addListener(_onFormChanged);
    _enderecoController.addListener(_onFormChanged);
  }

  @override
  void dispose() {
    _removeFormListeners() ;
    _nomeController.dispose();
    _numeroLoteController.dispose();
    _enderecoController.dispose();
    super.dispose();
  }

  void _removeFormListeners() {
    _nomeController.removeListener(_onFormChanged);
    _numeroLoteController.removeListener(_onFormChanged);
    _enderecoController.removeListener(_onFormChanged);
  }

  void _onFormChanged() {
    final hasUnsavedChanges = _hasFormChanged();
    DataManager.setHasUnsavedChanges(hasUnsavedChanges);
  }

  bool _hasFormChanged() {
    if (widget.familia == null) {
      return _nomeController.text.isNotEmpty
          || _numeroLoteController.text.isNotEmpty
          || _enderecoController.text.isNotEmpty
          || _assentamentoValue != null;
    } else {
      return _nomeController.text != widget.familia?.sobrenome
          || _numeroLoteController.text != widget.familia?.numeroLote.toString()
          || _enderecoController.text != widget.familia?.endereco
          || _assentamentoValue != assentamentosOptions.indexOf(widget.familia!.assentamento!.nome);
    }
  }


  void _addItem(BuildContext context) async {
    final String nome = _nomeController.text;
    int? assentamento = _assentamentoValue;
    final String numeroLote = _numeroLoteController.text;
    final String endereco = _enderecoController.text;

    // salva os valores dos campos do formulário para atualizar seu status
    _familyFormKey.currentState!.save();

    if (_familyFormKey.currentState!.validate()) {
      setState(() {
        DataManager.setIsLoading(true);
      });
      final assentamentoModel = AssentamentoModel(
        nome: assentamentosOptions[assentamento!],
      );

      final familia = FamiliaModel(
        sobrenome: nome,
        numeroLote: int.tryParse(numeroLote),
        endereco: endereco,
        assentamento: assentamentoModel,
      );

      final Map<String, dynamic> responseData;
      if (widget.familia == null) {
        responseData = await familiaController.postFamilia(familia);
      }
      else {
        responseData = await familiaController.patchFamilia(familia);
      }

      setState(() {
        DataManager.setIsLoading(false);
      });

      if (responseData["success"]) {
        Navigator.pop(context, true);
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }
      
    }
  }

  void _removeItem(BuildContext context) async {
    final Map<String, dynamic> responseData;

    if (widget.familia?.sobrenome != null) {
      setState(() {
        DataManager.setIsLoading(true);
      });
      responseData = await familiaController.deleteFamilia(widget.familia!.sobrenome);
    } else {
      _showSnackBar(context, 'Erro ao deletar familia.', AppTheme.danger);
      return;
    }

    setState(() {
      DataManager.setIsLoading(false);
    });

    if (responseData["success"]) {
      Navigator.pop(context, true);
    } else {
      _showSnackBar(context, responseData["message"], AppTheme.danger);
    }
  }

  void _showSnackBar(BuildContext context, String message, Color backgroundColor) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          message, 
          style: Theme.of(context).textTheme.headlineSmall!.copyWith(color: Colors.white),
          textAlign: TextAlign.center,
        ),
        duration: AppTheme.messageDuration,
        backgroundColor: backgroundColor,
      ),
    );
  }

  Future<bool> _showRemoveConfirmationAlert(context) async {
    return await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Tem certeza?'),
          content: Text('Você esta preste a remover uma família.\nTodos os usuários dessa família ficarão desativados até serem associados a uma família novamente.\nTem certeza que quer removê-la?'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.of(context).pop(false),
              child: Text('Não'),
            ),
            TextButton(
              onPressed: () => Navigator.of(context).pop(true),
              child: Text('Sim'),
            ),
          ],
        );
      },
    ) ?? false;
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Form(
              key: _familyFormKey,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // Nome
                    RocaTextField(
                      controller: _nomeController, 
                      labelText: 'Nome',
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Nome é obrigatório';
                        }
                        return null;
                      },
                    ),

                    // Assentamento
                    RocaDropdown(
                      hintText: 'Assentamento',
                      options: (f, cs) => List.generate(assentamentosOptions.length, (index) => index),
                      value: _assentamentoValue,
                      getLabel: (value) {
                        return assentamentosOptions[value];
                      },
                      onChanged: (newValue) {
                        setState(() {
                          _assentamentoValue = newValue;
                          _onFormChanged();
                        });
                      },
                      validator: (value) {
                        if (value == null && _assentamentoValue == null) {
                          return 'Assentamento é obrigatório';
                        }
                        return null;
                      },
                    ),

                    // Numero de Lote
                    RocaTextField(
                      enabled: widget.edit ?? true,
                      keyboardType: TextInputType.number,
                      controller: _numeroLoteController,
                      labelText: 'Número de Lote',
                    ),

                    // Endereço
                    RocaTextField(
                      controller: _enderecoController, 
                      labelText: 'Endereço',
                    ),

                    // Editar Família
                    if (widget.edit == true)  ...[
                      RocaButton(
                        text: 'Salvar Alterações', 
                        outline: false,
                        buttonColor: AppTheme.aspargus,
                        textStyle: Theme.of(context).textTheme.headlineMedium!.white,
                        onTap: () { _addItem(context);}
                      ),
                      RocaButton(
                        text: 'Remover Família', 
                        outline: false,
                        buttonColor: AppTheme.danger,
                        textStyle: Theme.of(context).textTheme.headlineMedium!.copyWith(color: Colors.white),
                        onTap: () async {
                          if (await _showRemoveConfirmationAlert(context))
                            _removeItem(context);
                        }
                      ),
                    ] else if (widget.edit == null) ...{
                      // Cadastrar Família
                      RocaButton(
                        text: 'Cadastrar Família', 
                        outline: false,
                        buttonColor: AppTheme.aspargus,
                        textStyle: Theme.of(context).textTheme.headlineMedium!.white,
                        onTap: () { _addItem(context);}
                      ),
                    }
                  ],
                ),
              )
            )
          ],
        ),
      ),
    );

    return Responsive(
      title: widget.edit == null
        ? 'Cadastrar Família'
        : (widget.edit == true ? 'Editar Família' : 'Ver Família'),
      body: body,
      backgroundColor: AppTheme.vanilla,
      hasSideMenu: false,
      appBarLandTheme: false,
    );
  }
}

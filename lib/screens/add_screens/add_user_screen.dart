import 'package:flutter/material.dart';
import 'package:roca/app_data.dart';

import 'package:diacritic/diacritic.dart';
import '../../constants.dart';
import '../../responsive.dart';
import '../../components/roca_dropdown.dart';
import '../../components/roca_text_input.dart';
import '../../components/roca_button.dart';

import '../../controllers/familia_controller.dart';
import '../../controllers/user_controller.dart';
import '../../models/familia_model.dart';
import '../../models/user_model.dart';
import '../../services/familia_service.dart';
import '../../services/user_service.dart';
import 'change_password_screen.dart';

class AddUserScreen extends StatefulWidget {
  final bool hasImage;
  final bool? edit;
  final PessoaModel? user;

  const AddUserScreen({
    super.key, 
    required this.hasImage,
    this.edit,
    this.user,
  });

  @override
  AddUserScreenState createState() => AddUserScreenState();
}

class AddUserScreenState extends State<AddUserScreen> {
  final userController = UserController(UserService());
  final familiaController = FamiliaController(FamiliaService());

  final _userFormKey = GlobalKey<FormState>();
  late TextEditingController _nomeController;
  int? _familiaValue;
  late TextEditingController _telefoneController;
  late TextEditingController _cpfController;
  late TextEditingController _senhaController;
  late TextEditingController _confirmSenhaController;
  bool? _adminValue;
  List<String> familiasOptions = [];

  @override
  void initState() {
    super.initState();

    _nomeController = TextEditingController(text: widget.user?.nome);
    _telefoneController = TextEditingController(text: widget.user?.telefone);
    _cpfController = TextEditingController(text: widget.user?.usuario.cpf);
    _senhaController = TextEditingController();
    _confirmSenhaController = TextEditingController();
    _adminValue = widget.user?.funcao?.codigo == "0";

    fetchFamilias();

    _addFormListeners();
  }

  void _addFormListeners() {
    _nomeController.addListener(_onFormChanged);
    _telefoneController.addListener(_onFormChanged);
    _cpfController.addListener(_onFormChanged);
    _senhaController.addListener(_onFormChanged);
    _confirmSenhaController.addListener(_onFormChanged);
  }

  @override
  void dispose() {
    _removeFormListeners() ;
    _nomeController.dispose();
    _telefoneController.dispose();
    _cpfController.dispose();
    _senhaController.dispose();
    _confirmSenhaController.dispose();
    super.dispose();
  }

  void _removeFormListeners() {
    _nomeController.removeListener(_onFormChanged);
    _telefoneController.removeListener(_onFormChanged);
    _cpfController.removeListener(_onFormChanged);
    _senhaController.removeListener(_onFormChanged);
    _confirmSenhaController.removeListener(_onFormChanged);
  }

  void fetchFamilias() async {
    setState(() {
      DataManager.setIsLoading(true);
    });
    

    familiaController.getFamilias().then((responseData) {
      setState(() {
        DataManager.setIsLoading(false);
      });

      if (!mounted) return;

      if (responseData["success"]) {
        final List<FamiliaModel> familias = responseData["data"];

        List<String> sobrenomes = familias.map((familia) => familia.sobrenome).toList();

        setState(() {
          familiasOptions = sobrenomes;
          if (widget.user?.familia?.sobrenome != null) {
            _familiaValue = familiasOptions.indexOf(widget.user!.familia!.sobrenome);
          }
        });
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }
    }).catchError((error) {

      setState(() {
        DataManager.setIsLoading(false);
      });

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            "Erro ao buscar famílias.",
            style: Theme.of(context).textTheme.headlineSmall!.white,
            textAlign: TextAlign.center,
          ),
          duration: AppTheme.messageDuration,
          backgroundColor: AppTheme.danger,
        ),
      );
    });
  }

  void _onFormChanged() {
    final hasUnsavedChanges = _hasFormChanged();
    DataManager.setHasUnsavedChanges(hasUnsavedChanges);
  }

  bool _hasFormChanged() {
    if (widget.user == null) {
      return _nomeController.text.isNotEmpty
          || _telefoneController.text.isNotEmpty
          || _cpfController.text.isNotEmpty
          || _senhaController.text.isNotEmpty
          || _confirmSenhaController.text.isNotEmpty
          || _adminValue != false
          || _familiaValue != null;
    } else {
      return _nomeController.text != widget.user?.nome
          || _telefoneController.text != widget.user?.telefone
          || _cpfController.text != widget.user?.usuario.cpf
          || _adminValue != (widget.user?.funcao?.codigo == "0")
          || (widget.user!.familia?.sobrenome == null && _familiaValue != null) // se usuario não tem familia e houve seleção de familia
          || (widget.user!.familia?.sobrenome != null && _familiaValue != familiasOptions.indexOf(widget.user!.familia!.sobrenome)); // se usuario tem familia e houve outra seleção de familia
    }
  }

  void _addItem(BuildContext context) async {
    if (_userFormKey.currentState!.validate()) {
      setState(() {
        DataManager.setIsLoading(true);
      });

      final unmaskedCPF = _cpfController.text.replaceAll(RegExp(r'\D'), '');
      final unmaskedTelefone = _telefoneController.text.replaceAll(RegExp(r'\D'), '');
      final String senha = _senhaController.text;

      final usuario = UsuarioModel(
        cpf: unmaskedCPF,
        senha: senha,
      );

      final familiaModel = FamiliaModel(sobrenome: familiasOptions[_familiaValue!]);

      final funcaoModel = FuncaoModel(
        codigo: _adminValue! ? '0' : '1',
        nome: _adminValue! ? 'Coordenadora' : 'Nucleada',
      );

      final user = PessoaModel(
        usuario: usuario, 
        nome: _nomeController.text, 
        telefone: unmaskedTelefone, 
        familia: familiaModel,
        funcao: funcaoModel,
      );

      final Map<String, dynamic> responseData;
      if (widget.user == null) {
        responseData = await userController.postUser(user);
      } else {
        responseData = await userController.patchUser(user);
      }

      setState(() {
        DataManager.setIsLoading(false);
      });

      if (responseData["success"]) {
        Navigator.pop(context, true);
      } else {
        _showSnackBar(context, responseData["message"], AppTheme.danger);
      }
    }
  }

  void _removeItem(BuildContext context) async {
    final Map<String, dynamic> responseData;

    if (widget.user?.usuario.cpf != null) {
      setState(() {
        DataManager.setIsLoading(true);
      });
      final unmaskedCPF = widget.user!.usuario.cpf.replaceAll(RegExp(r'\D'), '');
      responseData = await userController.deleteUser(unmaskedCPF);
    } else {
      _showSnackBar(context, 'Erro ao deletar o usuário.', AppTheme.danger);
      return;
    }

    setState(() {
      DataManager.setIsLoading(false);
    });

    if (responseData["success"]) {
      Navigator.pop(context, true);
    } else {
      _showSnackBar(context, responseData["message"], AppTheme.danger);
    }
  }

  void _showSnackBar(BuildContext context, String message, Color backgroundColor) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          message, 
          style: Theme.of(context).textTheme.headlineSmall!.white,
          textAlign: TextAlign.center,
        ),
        duration: AppTheme.messageDuration,
        backgroundColor: backgroundColor,
      ),
    );
  }

  Future<bool> _showSelfModificationAlert(context) async {
    if (widget.user?.usuario.cpf != DataManager.getLoggedUser()?.usuario.cpf) return true;

    return await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Modificação em si mesmo'),
          content: Text('Você não pode alterar coordenação de si mesmo nem se remover.\nSe realmente deseja fazer isso solicite um outro coordenador.'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text('OK'),
            ),
          ],
        );
      },
    ) ?? false;
  }

  Future<bool> _showRemoveConfirmationAlert(context) async {
    return await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Tem certeza?'),
          content: Text('Você esta preste a remover um usuário. Tem certeza que quer removê-lo?'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.of(context).pop(false),
              child: Text('Não'),
            ),
            TextButton(
              onPressed: () => Navigator.of(context).pop(true),
              child: Text('Sim'),
            ),
          ],
        );
      },
    ) ?? false;
  }

  final MaterialStateProperty<Icon?> switchIcon = MaterialStateProperty.resolveWith<Icon?>(
    (Set<MaterialState> states) {
      if (states.contains(MaterialState.selected)) {
        return const Icon(Icons.check);
      }
      return const Icon(Icons.close);
    },
  );

  @override
  Widget build(BuildContext context) {
    Widget body = Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [ 
            Form(
            key: _userFormKey,
              child: Column(
                children: [
                  RocaTextField(
                    enabled: widget.edit ?? true,
                    controller: _nomeController, 
                    labelText: 'Nome',
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Nome é obrigatório';
                      }
                      return null;
                    },
                  ),
                  RocaDropdown(
                    searchable: true,
                    hintText: 'Família',
                    options: (f, cs) => List.generate(familiasOptions.length, (index) => index),
                    value: _familiaValue,
                    itemBuilder: (context, item, isDisabled, isSelected) {
                      return ListTile(
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 8, horizontal: 12),
                        title: Text(
                          familiasOptions[item],
                          style: TextStyle(
                              color: Colors.black, fontSize: 18),
                        ),
                      );
                    },
                    getLabel: (value) {
                      return removeDiacritics(familiasOptions[value]);
                    },
                    onChanged: (newValue) {
                      setState(() {
                        _familiaValue = newValue;
                        _onFormChanged();
                      });
                    },
                    validator: (value) {
                      if (value == null && _familiaValue == null) {
                        return 'Família é obrigatório';
                      }
                      return null;
                    },
                  ),
                  RocaTextField(
                    enabled: widget.edit ?? true,
                    keyboardType: TextInputType.number,
                    controller: _telefoneController, 
                    labelText: 'Telefone',
                    inputFormatters: [AppTheme.phoneMask],
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Telefone é obrigatório';
                      }
                      return null;
                    },
                  ),
                  RocaTextField(
                    enabled: widget.edit ?? true,
                    keyboardType: TextInputType.number,
                    controller: _cpfController, 
                    labelText: 'CPF',
                    inputFormatters: [AppTheme.cpfMask],
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'CPF é obrigatório';
                      }
                      return null;
                    },
                  ),
                  if (widget.user == null) ...[
                    RocaTextField(
                      enabled: widget.edit ?? true,
                      controller: _senhaController, 
                      labelText: 'Senha',
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Senha é obrigatório';
                        }
                        return null;
                      },
                    ),
                    RocaTextField(
                      enabled: widget.edit ?? true,
                      controller: _confirmSenhaController, 
                      labelText: 'Confirmar Senha',
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Confirmar Senha é obrigatório';
                        }
                        else if (value != _senhaController.text) {
                          return 'Senha precisa ser igual acima';
                        }
                        return null;
                      },
                    ),
                  ] else ...{
                    RocaButton(
                      text: 'Alterar Senha', 
                      outline: false,
                      buttonColor: AppTheme.land,
                      textStyle: Theme.of(context).textTheme.headlineMedium!.copyWith(color: Colors.white),
                      onTap: () async {
                        if (await DataManager.getHasUnsavedChanges(context)){
                          DataManager.setHasUnsavedChanges(false);
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => ChangePasswordScreen(user: widget.user!.usuario)),
                          ).then((value)=> setState((){}));
                        }
                      }
                    ),
                  },
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Coordenador',
                        style: Theme.of(context).textTheme.headlineSmall!.copyWith(color: AppTheme.land),
                      ),
                      const SizedBox(width: 20),
                      Switch(
                        thumbIcon: switchIcon,
                        value: _adminValue ?? false,
                        onChanged: (bool value) async {
                          if (await _showSelfModificationAlert(context))
                            setState(() {
                              _adminValue = value;
                              _onFormChanged();
                            });
                        },
                      ),
                    ],
                  ),
                  if (widget.edit == true) ...[
                    RocaButton(
                      text: 'Salvar Alterações', 
                      outline: false,
                      buttonColor: AppTheme.aspargus,
                      textStyle: Theme.of(context).textTheme.headlineMedium!.copyWith(color: Colors.white),
                      onTap: () { _addItem(context); }
                    ),
                    RocaButton(
                      text: 'Remover Usuário', 
                      outline: false,
                      buttonColor: AppTheme.danger,
                      textStyle: Theme.of(context).textTheme.headlineMedium!.copyWith(color: Colors.white),
                      onTap: () async {
                        if (await _showSelfModificationAlert(context))
                          if (await _showRemoveConfirmationAlert(context))
                            _removeItem(context);
                      }
                    ),
                  ] else if (widget.edit == null) ...{
                    RocaButton(
                      text: 'Cadastrar Usuário', 
                      outline: false,
                      buttonColor: AppTheme.aspargus,
                      textStyle: Theme.of(context).textTheme.headlineMedium!.copyWith(color: Colors.white),
                      onTap: () { _addItem(context); }
                    ),
                  }
                ],
              ),
            ),
          ],
        ),
      )
    );

    return Responsive(
      title: widget.edit == null ? 'Cadastrar Usuário' : (widget.edit == true ? 'Editar Usuário' : 'Ver Usuário'),
      body: body,
      backgroundColor: AppTheme.vanilla,
      hasSideMenu: false,
      appBarLandTheme: false,
    );
  }
}

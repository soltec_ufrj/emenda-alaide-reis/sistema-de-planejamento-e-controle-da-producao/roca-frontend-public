import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:diacritic/diacritic.dart';

import '../../components/roca_image_edit.dart';
import '../../constants.dart';
import '../../app_data.dart';
import '../../responsive.dart';
import '../../components/roca_dropdown.dart';
import '../../components/roca_text_input.dart';
import '../../components/roca_button.dart';

import '../../models/familia_model.dart';
import '../../models/item_lista_model.dart';
import '../../models/produto_model.dart';
import '../../controllers/item_lista_controller.dart';
import '../../controllers/produto_controller.dart';
import '../../services/item_lista_service.dart';
import '../../services/produto_service.dart';

class AddListProductScreen extends StatefulWidget {
  final ItemListaModel? itemLista;
  final bool isEditable;
  final String listId;

  const AddListProductScreen({
    super.key,
    this.itemLista,
    required this.isEditable,
    required this.listId,
  });

  @override
  AddListProductScreenState createState() => AddListProductScreenState();
}

class AddListProductScreenState extends State<AddListProductScreen> {
  late List<ProdutoModel> fetchedProdutos = [];

  final _listProductFormKey = GlobalKey<FormState>();
  final produtoController = ProdutoController(ProdutoService());
  final itemListaController = ItemListaController(ItemListaService());
  late TextEditingController _quantidadeController = TextEditingController();
  ProdutoModel? _productValue;

  void fetchProdutos() async {
    setState(() {
      DataManager.setIsLoading(true);
    });

    produtoController.getProdutos().then((responseData) {
      setState(() {
        DataManager.setIsLoading(false);
      });

      if (!mounted) return;


      if (responseData["success"]) {
        fetchedProdutos = responseData["data"];

        setState(() {
          if (widget.itemLista?.produto.id != null) {
            _productValue = widget.itemLista!.produto;
          }
        });
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"],
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }
    }).catchError((error) {

      setState(() {
        DataManager.setIsLoading(false);
      });

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            "Erro ao buscar produtos",
            style: Theme.of(context).textTheme.headlineSmall!.white,
            textAlign: TextAlign.center,
          ),
          duration: AppTheme.messageDuration,
          backgroundColor: AppTheme.danger,
        ),
      );
    });
  }

  @override
  void initState() {
    super.initState();

    fetchProdutos();

    _quantidadeController = TextEditingController(
        text: widget.itemLista?.quantidade.toString() ?? '');

    _addFormListeners();
  }

  void _addFormListeners() {
    _quantidadeController.addListener(_onFormChanged);
  }

  @override
  void dispose() {
    _removeFormListeners();
    _quantidadeController.dispose();
    super.dispose();
  }

  void _removeFormListeners() {
    _quantidadeController.removeListener(_onFormChanged);
  }

  void _onFormChanged() {
    final hasUnsavedChanges = _hasFormChanged();
    DataManager.setHasUnsavedChanges(hasUnsavedChanges);
  }

  bool _hasFormChanged() {
    if (widget.itemLista == null) {
      return _quantidadeController.text.isNotEmpty || _productValue != null;
    } else {
      return _quantidadeController.text !=
              widget.itemLista?.quantidade.toString() ||
          _productValue != widget.itemLista?.produto.id;
    }
  }

  void _addItem(BuildContext context) async {
    String? produtoId = _productValue?.id;
    final String quantidade = _quantidadeController.text;

    _listProductFormKey.currentState!.save();

    if (_listProductFormKey.currentState!.validate()) {
      setState(() {
        DataManager.setIsLoading(true);
      });

      ProdutoModel produtoModel =
          fetchedProdutos.firstWhere((produto) => produto.id == produtoId);
      FamiliaModel familiaModel = FamiliaModel(
          sobrenome: DataManager.getLoggedUser()!.familia!.sobrenome);

      final itemLista = ItemListaModel(
        id: widget.itemLista?.id,
        familia: familiaModel,
        listaId: widget.listId,
        produto: produtoModel,
        quantidade: int.tryParse(quantidade)!,
      );

      final Map<String, dynamic> responseData;
      if (widget.itemLista == null) {
        responseData = await itemListaController.postItemLista(itemLista);
      } else {
        responseData = await itemListaController.patchItemLista(itemLista);
      }

      setState(() {
        DataManager.setIsLoading(false);
      });

      if (responseData["success"]) {
        Navigator.pop(context, true);
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"],
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }
    }
  }

  void _removeItem(BuildContext context) async {
    final Map<String, dynamic> responseData;

    if (widget.itemLista!.id != null) {
      setState(() {
        DataManager.setIsLoading(true);
      });
      responseData =
          await itemListaController.deleteItemLista(widget.itemLista!.id);
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Erro ao deletar o item.',
            style: Theme.of(context).textTheme.headlineSmall!.white,
            textAlign: TextAlign.center,
          ),
          duration: AppTheme.messageDuration,
          backgroundColor: AppTheme.danger,
        ),
      );
      return;
    }

    setState(() {
      DataManager.setIsLoading(false);
    });

    if (responseData["success"]) {
      Navigator.pop(context, true);
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            responseData["message"],
            style: Theme.of(context).textTheme.headlineSmall!.white,
            textAlign: TextAlign.center,
          ),
          duration: AppTheme.messageDuration,
          backgroundColor: AppTheme.danger,
        ),
      );
    }
  }

  Future<bool> _showRemoveConfirmationAlert(context) async {
    return await showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Tem certeza?'),
              content: Text(
                  'Você esta preste a remover um item da sua lista. Tem certeza que quer removê-lo?'),
              actions: <Widget>[
                TextButton(
                  onPressed: () => Navigator.of(context).pop(false),
                  child: Text('Não'),
                ),
                TextButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: Text('Sim'),
                ),
              ],
            );
          },
        ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Form(
              key: _listProductFormKey,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // Imagem do produto
                    RocaImageEdit(
                      imageUrl: getProdutoImagemUrl(widget.itemLista?.produto.imagemUrl),
                      imagem: FileImage(File('assets/img/placeholder.jpg')),
                      imagemAlterada: false,
                      isEditable: false,
                    ),

                    // Produto
                    widget.itemLista == null
                        ? RocaDropdown(
                            searchable: true,
                            hintText: 'Produto',
                            options: (f, cs) =>
                                buildDropdownMenuItems(fetchedProdutos),
                            dropdownBuilder: (context, selectedItem) {
                              if (selectedItem != null)
                                return Text(
                                  selectedItem.$2.nome,
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 18),
                                );
                              else
                                return Container();
                            },
                            itemBuilder:
                                (context, item, isDisabled, isSelected) {
                              return ListTile(
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 8, horizontal: 12),
                                leading: iconeImagemProduto(item.$1),
                                title: Text(
                                  item.$2.nome,
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 18),
                                ),
                              );
                            },
                            onChanged: (newValue) {
                              setState(() {
                                _productValue = newValue.$2;
                                _onFormChanged();
                              });
                            },
                            validator: (value) {
                              if (value == null && _productValue == null) {
                                return 'Produto é obrigatório';
                              }
                              return null;
                            },
                            getLabel: (item) {
                                return removeDiacritics(item.$2.nome);
                            },
                          )
                        : RocaTextField(
                            enabled: true,
                            readOnly: true,
                            controller: TextEditingController(
                                text: widget.itemLista?.produto.nome),
                            labelText: 'Produto',
                          ),

                    // Quantidade
                    RocaTextField(
                      enabled: widget.isEditable,
                      keyboardType: TextInputType.number,
                      controller: _quantidadeController,
                      labelText: 'Quantidade',
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Quantidade é obrigatório';
                        }
                        return null;
                      },
                    ),

                    // Texto de unidade do produto selecionado
                    if (_productValue != null)
                      Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              ' Unidade: ${fetchedProdutos.firstWhere((produto) => produto.id == _productValue?.id).unidade.nome}',
                              style:
                                  Theme.of(context).textTheme.titleLarge!.black,
                            ),
                          ],
                        ),
                      ),

                    // Editar Produto
                    if (widget.itemLista != null && widget.isEditable)
                      RocaButton(
                          text: 'Salvar Alterações',
                          outline: false,
                          buttonColor: AppTheme.aspargus,
                          textStyle:
                              Theme.of(context).textTheme.headlineMedium!.white,
                          onTap: () {
                            _addItem(context);
                          }),

                    // Remover Produto
                    if (widget.itemLista != null && widget.isEditable)
                      RocaButton(
                          text: 'Remover Produto',
                          outline: false,
                          buttonColor: AppTheme.danger,
                          textStyle:
                              Theme.of(context).textTheme.headlineMedium!.white,
                          onTap: () async {
                            if (await _showRemoveConfirmationAlert(context))
                              _removeItem(context);
                          }),

                    // Adicionar Produto
                    if (widget.itemLista == null)
                      RocaButton(
                          text: 'Adicionar Produto',
                          outline: false,
                          buttonColor: AppTheme.aspargus,
                          textStyle:
                              Theme.of(context).textTheme.headlineMedium!.white,
                          onTap: () {
                            _addItem(context);
                          }),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );

    return Responsive(
      title: widget.itemLista == null
          ? 'Adicionar Produto'
          : (widget.isEditable ? 'Editar Produto' : 'Ver Produto'),
      body: body,
      backgroundColor: AppTheme.vanilla,
      hasSideMenu: false,
      isCoordenador: false,
      appBarLandTheme: false,
    );
  }

  List buildDropdownMenuItems(List<ProdutoModel> produtos) {
    List items = [];
    for (ProdutoModel produto in produtos) {
      items.add(
        (produto.imagemUrl, produto),
      );
    }
    return items;
  }

  Widget iconeImagemProduto(String imagemUrl) {
    return Container(
        height: 50,
        width: 50,
        child: CachedNetworkImage(
            key: UniqueKey(),
            cacheManager: CacheManager(
              Config(
                'cacheKey',
                stalePeriod: const Duration(days: 1),
              ),
            ),
            imageUrl: imagemUrl,
            placeholder: (context, url) =>
                const Center(child: CircularProgressIndicator()),
            errorWidget: (context, url, error) => Container(
                    decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  image: DecorationImage(
                    image: AssetImage('assets/img/placeholder.jpg'),
                    fit: BoxFit.cover,
                  ),
                )),
            imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                ))));
  }

  String getProdutoImagemUrl(String? url) {
    if (url == null)
      if (_productValue != null)
        return fetchedProdutos.firstWhere((produto) => produto.id == _productValue?.id).imagemUrl;
      else return "";
    else return url;
  }
}

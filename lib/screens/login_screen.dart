import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../constants.dart';
import '../app_data.dart';
import '../data_storage_manager.dart';
import '../responsive.dart';
import '../components/roca_button.dart';
import '../components/roca_text_input.dart';

import '../models/user_model.dart';
import '../controllers/user_controller.dart';
import '../services/user_service.dart';

import '../screens/choose_profile_screen.dart';
import '../screens/nucleado_screens/nucleado_list_screen.dart';


class LoginScreen extends StatefulWidget {
  static const routeName = '/login';
  static const title = 'Entrar Conta';

  const LoginScreen({ super.key });

  @override
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  final _userController = UserController(UserService());

  final _loginFormKey = GlobalKey<FormState>();
  final TextEditingController _cpfController = TextEditingController();
  final TextEditingController _senhaController = TextEditingController();

  bool _isObscure = true;
  bool _rememberMe = false; // bool do checkbox
  bool _credentialsSaved = false; // controla se tem credenciais salvas

  @override
  void initState() {
    super.initState();
    _loadSavedCredentials();
  }

  Future<void> _loadSavedCredentials() async {
    final prefs = DataStorageManager();
    _cpfController.text = await prefs.getCPF() ?? '';
    _senhaController.text = await prefs.getSenha() ?? '';
    final rememberMe = await prefs.getRememberMe();
    setState(() {
      _rememberMe = rememberMe ?? false;
      _credentialsSaved = _rememberMe && _cpfController.text.isNotEmpty && _senhaController.text.isNotEmpty;
    });
  }

  Future<void> _saveCredentials() async {
    final prefs = DataStorageManager();
    if (_rememberMe) {
      await prefs.setCPF(_cpfController.text);
      await prefs.setSenha(_senhaController.text);
    } else {
      await prefs.limparDados();
    }
    await prefs.rememberMe(_rememberMe);
  }



  Future<void> _logIn(BuildContext context) async {
    final String _cpf = _cpfController.text;
    final String _senha = _senhaController.text;

    if (_loginFormKey.currentState!.validate()) {
      setState(() {
        DataManager.setIsLoading(true); // sobrepõe a tela de carregamento pois o login pode demorar por ser a primeira requisição
      });

      final unmaskedCPF = _cpf.replaceAll(RegExp(r'\D'), '');
      final usuario = UsuarioModel(cpf: unmaskedCPF, senha: _senha);

      final Map<String, dynamic> responseData = await _userController.login(usuario);

      if (responseData["success"]) {
        DataManager.setLoggedUser(responseData["pessoa"]);
        await _saveCredentials();

        // checa se usuário logado é coordenador ou não
        if (DataManager.getLoggedUser()?.funcao?.codigo == "0") {
          Navigator.of(context).pushNamed(ChooseProfileScreen.routeName);
        }
        else{
          Navigator.of(context).pushNamed(NucleadoListsScreen.routeName);
        }
        
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              responseData["message"], 
              style: Theme.of(context).textTheme.headlineSmall!.white,
              textAlign: TextAlign.center,
            ),
            duration: AppTheme.messageDuration,
            backgroundColor: AppTheme.danger,
          ),
        );
      }

      setState(() {
        DataManager.setIsLoading(false);
      });
    } 
    else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Todos os campos devem ser preenchidos.', 
            style: Theme.of(context).textTheme.headlineSmall!.white,
            textAlign: TextAlign.center,
          ),
          duration: AppTheme.messageDuration,
          backgroundColor: AppTheme.danger,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [ 
        Form(
          key: _loginFormKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              // Campo de CPF
              RocaTextField(
                keyboardType: TextInputType.number,
                controller: _cpfController, 
                labelText: 'CPF',
                inputFormatters: [AppTheme.cpfMask],
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'CPF é obrigatório';
                  }
                  return null;
                },
              ),

              // Campo de Senha
              RocaTextField(
                obscureText: _isObscure,
                controller: _senhaController, 
                labelText: 'Senha',
                icon: _credentialsSaved
                ? null // se credenciais estão salvas, não deixa mudar visibilidade
                : IconButton(
                  icon: Icon(
                    _isObscure ? Icons.visibility_off : Icons.visibility ,
                  ),
                  onPressed: () {
                    setState(() {
                      _isObscure = !_isObscure;
                    });
                  },
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Senha é obrigatória';
                  }
                  return null;
                },
                onChanged: (value) {
                  if (_credentialsSaved)
                    _senhaController.text = '';

                  setState(() {
                    _credentialsSaved = false;
                  });
                }
              ),

              // Lembra as credenciais
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Transform.scale(
                    
                    scale: 1.5,
                    child: Checkbox(
                      side: BorderSide(
                        color: AppTheme.land, 
                        width: 2
                      ),
                      value: _rememberMe,
                      onChanged: (value) {
                        setState(() {
                          _rememberMe = value!;
                        });
                      },
                    ),
                  ),
                  AutoSizeText(
                    'Lembrar de mim', 
                    maxLines: 1,
                    wrapWords: false, 
                    style: Theme.of(context).textTheme.headlineSmall!.land,
                  ),
                ],
              ),
              
              // Botão Login
              RocaButton(
                text: 'Entrar na Conta', 
                outline: false,
                buttonColor: AppTheme.aspargus,
                textStyle: Theme.of(context).textTheme.headlineMedium!.white,
                onTap: () { _logIn(context);}
              ),

              // const Row(
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   children: <Widget>[
              //     Text("Equeceu a senha? "),
              //     Text("Clique aqui", style: TextStyle(
              //       fontWeight: FontWeight.w600, fontSize: 18
              //     ),),
              //   ],
              // )
            ],
          ),
        ),
      ],
    );
    
    return Responsive(
      title: LoginScreen.title,
      body: body,
      backgroundColor: AppTheme.vanilla,
      hasSideMenu: false,
      appBarLandTheme: false,
    );
  }
}
import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';

import '../constants.dart';
import '../responsive.dart';
import '../components/roca_button.dart';

import './homepage_screen.dart';


class PageNotFound extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget body = Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // Logo
          Expanded(
            flex: 2,
            child: Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.contain,
                  image: AssetImage('assets/img/logo.png'),
                ),
              ),
            ),
          ),

          // Texto and Botão
          Expanded(
            flex: 2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 50.0),
                  child: Center(
                    child: AutoSizeText(
                      maxLines: 1,
                      wrapWords: false,
                      "Ops! A página que você procura não foi encontrada.",
                      style: Theme.of(context).textTheme.displaySmall!.copyWith(color: AppTheme.land),
                    ),
                  ),
                ),
                Center(
                  child: RocaButton(
                    text: 'Voltar para a página inicial',
                    outline: false,
                    buttonColor: AppTheme.land,
                    textStyle: Theme.of(context).textTheme.headlineMedium!.copyWith(color: AppTheme.vanilla),
                    onTap: () {
                      Navigator.of(context).pushNamed(HomepageScreen.routeName);
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );

    return Responsive(
      body: body,
      backgroundColor: AppTheme.vanilla,
      hasSideMenu: false,
    );
  }
}

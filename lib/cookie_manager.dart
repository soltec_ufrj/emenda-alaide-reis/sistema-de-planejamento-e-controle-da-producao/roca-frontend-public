import 'package:http/http.dart' as http;

class CookieManager {
  Map<String, String> _cookies = {};

  void setCookies(http.Response cookies) {
    _cookies = _extractCookies(cookies);
  }

  String getCookies() {
    return _cookies.entries.map((e) => '${e.key}=${e.value}').join('; ');
  }

  void clearCookies() {
    _cookies.clear();
  }

  Map<String, String> _extractCookies(http.Response response) {
    final headers = response.headers;
    final cookies = headers['set-cookie'];
    if (cookies != null) {
      final cookieList = cookies.split(',');
      final cookieMap = <String, String>{};
      for (final cookie in cookieList) {
        final cookieParts = cookie.split(';')[0].split('=');
        if (cookieParts.length == 2) {
          cookieMap[cookieParts[0].trim()] = cookieParts[1].trim();
        }
      }
      return cookieMap;
    }
    return {};
  }
}
import 'package:auto_size_text/auto_size_text.dart';
import 'package:roca/constants.dart';
import 'package:flutter/material.dart';

enum ButtonState { idle, loading, disabled }

class RocaButton extends StatefulWidget{
  final String text;
  final TextStyle? textStyle;
  final bool outline;
  final ButtonState state;
  final Icon? leadingIcon;
  final Color? buttonColor;
  final Function()? onTap;
  // final Color color, textColor;
  // final double? height;

  const RocaButton({
    super.key, 
    required this.text,
    this.textStyle,
    required this.outline,
    this.state = ButtonState.idle,
    this.leadingIcon,
    this.buttonColor,
    this.onTap,
    // this.color = kPrimaryColor,
    // this.textColor = Colors.white,
    // this.height
  });


  @override
  State<RocaButton> createState() => _RocaButtonState();
}

class _RocaButtonState extends State<RocaButton> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: AppTheme.buttonVerticalPadding),
      child: SizedBox(
        height: AppTheme.buttonHeight,
        width: AppTheme.buttonWidth,
        child: widget.outline ?
        OutlinedButton(
          style: ButtonStyle(
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(AppTheme.buttonBorderRadius),
              )
            ),
            side: MaterialStateProperty.all(BorderSide(
              color: widget.buttonColor ?? Colors.transparent, // Use a default color if buttonColor is null
              width: AppTheme.buttonBorderWidth,
              style: BorderStyle.solid)
            )
          ),
          onPressed: () {
            widget.onTap?.call();
          },
          child: AutoSizeText(
            widget.text, 
            maxLines: 1,
            wrapWords: false, 
            style: widget.textStyle ?? Theme.of(context).textTheme.labelLarge
          ),
        )
        : ElevatedButton(
          style: ButtonStyle(
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(AppTheme.buttonBorderRadius),
              )
            ),
            backgroundColor: widget.buttonColor != null
                ? MaterialStateProperty.all<Color>(widget.buttonColor!) // Handle nullable case
                : null,
          ),
          onPressed: () {
            widget.onTap?.call();
          },
          child: Wrap(
            children: [
            if (widget.leadingIcon != null)
              widget.leadingIcon!,
              const SizedBox(
                  width:10,
              ),
            AutoSizeText(
              widget.text, 
              maxLines: 1,
              wrapWords: false, 
              style: widget.textStyle ?? Theme.of(context).textTheme.labelLarge
            ),
            ],
          ),
        )
      ),
    );
    
    
    
    // return InkWell(
    //   onTap: widget.onTap,
    //   child: Container(
    //     height: AppTheme.buttonHeight,
    //     decoration: BoxDecoration(
    //       borderRadius: BorderRadius.circular(AppTheme.buttonBorderRadius),
    //       border: widget.outline ? Border.all(
    //         color: defaultColor.first, 
    //         width: AppTheme.buttonBorderWidth,
    //       )
    //       : null,
    //       color: defaultColor.length < 2 && !widget.outline ? defaultColor.first : null,
    //       gradient: defaultColor.length > 1 && !widget.outline ? LinearGradient(
    //         colors: defaultColor,
    //       )
    //       : null,
    //     ),
    //     child: widget.state == ButtonState.loading
    //     ? const Center(child: CircularProgressIndicator())
    //     : Row(
    //       mainAxisAlignment: MainAxisAlignment.center,
    //       children: [
    //         if(widget.leadingIcon != null)
    //           Padding(
    //             padding: const EdgeInsets.only(
    //               right: AppTheme.elementSpacing * 0.25, 
    //             )
    //             , child: widget.leadingIcon,
    //           ),
    //         Text(widget.text, style: widget.textStyle != null ? widget.textStyle : Theme.of(context).textTheme.labelLarge,),
    //       ],
    //     ),
    //   ),
    // );
  }
}

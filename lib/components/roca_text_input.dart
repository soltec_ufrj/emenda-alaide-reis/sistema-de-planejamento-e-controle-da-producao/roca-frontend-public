import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:roca/constants.dart';

class RocaTextField extends StatelessWidget {
  final bool enabled;
  final bool readOnly;
  final List<TextInputFormatter>? inputFormatters;
  final TextEditingController controller;
  final String labelText;
  final TextInputType keyboardType;
  final bool obscureText;
  final BorderRadius borderRadius;
  final Color borderColor;
  final Color fillColor;
  final String? Function(String?)? validator;
  final Widget? icon;
  final Function(String?)? onChanged;

  const RocaTextField({
    super.key,
    this.enabled = true,
    this.readOnly = false,
    this.inputFormatters, 
    required this.controller,
    required this.labelText,
    this.keyboardType = TextInputType.text,
    this.obscureText = false,
    this.borderRadius = AppTheme.inputBorderRadius,
    this.borderColor = AppTheme.inputBorderColor,
    this.fillColor = AppTheme.inputFillColor,
    this.validator,
    this.icon,
    this.onChanged
  });

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(
        maxWidth: AppTheme.inputMaxWidth,
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: AppTheme.inputVerticalPadding),
        child: TextFormField( 
          enabled: enabled,
          readOnly: readOnly,
          onChanged: onChanged,
          inputFormatters: inputFormatters,
          controller: controller,
          keyboardType: keyboardType,
          obscureText: obscureText,
          decoration: InputDecoration(
            labelText: labelText,
            filled: true,
            fillColor: fillColor,
            border: OutlineInputBorder(
              borderRadius: borderRadius,
              borderSide: BorderSide(
                color: borderColor,
                width: AppTheme.inputBorderWidth,
              ),
            ),
            suffixIcon: icon,
          ),
          validator: validator,
          onTapOutside: (event) {
            FocusManager.instance.primaryFocus?.unfocus(); // desfoca os campos de input quando clicar fora
          },
        ),
      )
    );
  }
}
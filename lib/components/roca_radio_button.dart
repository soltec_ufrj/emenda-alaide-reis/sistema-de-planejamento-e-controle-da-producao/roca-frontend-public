import 'package:flutter/material.dart';
import 'package:roca/constants.dart';

class CustomRadioButton extends StatefulWidget {
  final bool value;
  final ValueChanged<bool>? onChanged;
  final IconData icon;
  // final IconData uncheckedIcon;

  const CustomRadioButton({
    super.key, 
    required this.value,
    required this.onChanged,
    required this.icon,
    // required this.uncheckedIcon,
  });

  @override
  State<CustomRadioButton> createState() => _CustomRadioButtonState();

}

class _CustomRadioButtonState extends State<CustomRadioButton> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
            color: widget.value ? Colors.black : Colors.transparent,
            width: 3,
          ),
          borderRadius: BorderRadius.circular(8),
        ),
        child: InkWell(
          onTap: () {
            if (widget.onChanged != null) {
              widget.onChanged!(!widget.value);
            }
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                size: 40,
                widget.icon,
                color: widget.value ? Colors.black : AppTheme.land,
              ),
            ],
          ),
        )
      )
    );
  }
}


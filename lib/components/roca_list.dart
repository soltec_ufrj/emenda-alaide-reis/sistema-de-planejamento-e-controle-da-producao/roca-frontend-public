import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:roca/constants.dart';

class RocaListItem extends StatefulWidget {
  final String? thumbnail;
  final String title;
  final String subtitle;
  final String color;
  final bool hasImage;
  final bool onlyVisible;
  final List<Widget>? children;

  const RocaListItem({
    super.key,
    required this.thumbnail,
    required this.title,
    required this.subtitle,
    required this.color,
    required this.hasImage,
    required this.onlyVisible,
    this.children,
  });

  @override
  RocaListItemState createState() => RocaListItemState();
}

class RocaListItemState extends State<RocaListItem> with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);

    Color? backgroundColor;

    switch (widget.color) {
      case 'green':
        backgroundColor = AppTheme.aspargus;
        break;
      case 'orange':
        backgroundColor = AppTheme.sunrise;
        break;
      case 'gray':
        backgroundColor = AppTheme.silver;
        break;
      case 'white':
        backgroundColor = Colors.white;
        break;
      case 'red':
        backgroundColor = AppTheme.danger;
        break;
      default:
        backgroundColor = Colors.transparent;
        break;
    }

    Widget mainContent = Padding(
      padding: const EdgeInsets.all(AppTheme.listItemSpacing),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          if (widget.hasImage)
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: AppTheme.listItemImageSize,
                  height: AppTheme.listItemImageSize,
                  padding: const EdgeInsets.all(4.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: AspectRatio(
                      aspectRatio: 1.0,
                      child: widget.thumbnail != null
                          ? CachedNetworkImage(
                              key: UniqueKey(),
                              cacheManager: CacheManager(
                                Config(
                                  'cacheKey',
                                  stalePeriod: const Duration(days: 1),
                                ),
                              ),
                              imageUrl: widget.thumbnail!,
                              placeholder: (context, url) =>
                                  const Center(child: CircularProgressIndicator()),
                              errorWidget: (context, url, error) => Image.asset(
                                'assets/img/placeholder.jpg',
                                fit: BoxFit.cover,
                              ),
                            )
                          : Image.asset(
                              'assets/img/placeholder.jpg',
                              fit: BoxFit.cover,
                            ),
                    ),
                  ),
                ),
              ],
            ),
          if (widget.hasImage) const SizedBox(width: 20),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  children: [
                    Expanded(
                      child: AutoSizeText(
                        widget.title,
                        maxLines: 2,
                        wrapWords: false,
                        style: widget.color == 'white'
                            ? Theme.of(context).textTheme.headlineMedium!.land
                            : Theme.of(context).textTheme.headlineMedium!.white,
                      ),
                    )
                  ],
                ),
                if (!widget.hasImage)
                  const Divider(
                    thickness: 2,
                    color: AppTheme.land,
                  ),
                Row(
                  children: [
                    Expanded(
                      child: AutoSizeText(
                        widget.subtitle,
                        maxLines: 2,
                        wrapWords: false,
                        style: widget.color == 'white'
                            ? Theme.of(context).textTheme.headlineMedium!.land
                            : Theme.of(context).textTheme.headlineSmall!.white,
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
          const SizedBox(width: 20),
          if ((widget.color == 'orange' && !widget.onlyVisible) ||
              (widget.color != 'white' && widget.color != 'orange'))
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: widget.onlyVisible
                    ? const Icon(Icons.visibility_outlined,
                        color: AppTheme.land, size: 40)
                    : const Icon(Icons.edit, color: AppTheme.land, size: 40),
              ),
            ),
        ],
      ),
    );

    if (widget.children != null && widget.children!.isNotEmpty) {
      return ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Theme(
          data: Theme.of(context).copyWith(
            iconTheme: IconThemeData(
              size: AppTheme.appbarIconSize,
            ),
          ),
          child: ExpansionTile(
            iconColor: Colors.white,
            collapsedIconColor: Colors.white,
            backgroundColor: backgroundColor,
            collapsedBackgroundColor: backgroundColor,
            title: mainContent,
            children: widget.children!,
          ),
        ),
      );
    } else {
      return Container(
        decoration: BoxDecoration(
          color: backgroundColor,
          borderRadius: BorderRadius.circular(10),
        ),
        child: mainContent,
      );
    }
  }

  @override
  bool get wantKeepAlive => true;
}

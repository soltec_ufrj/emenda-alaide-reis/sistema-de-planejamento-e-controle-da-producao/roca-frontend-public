
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:roca/app_data.dart';
import 'package:roca/constants.dart';
import 'package:roca/controllers/user_controller.dart';
import 'package:roca/screens/choose_profile_screen.dart';
import 'package:roca/screens/coordenador_screens/coordenador_assentamentos_screen.dart';
import 'package:roca/screens/coordenador_screens/coordenador_families_screen.dart';
import 'package:roca/screens/coordenador_screens/coordenador_products_screen.dart';
import 'package:roca/screens/coordenador_screens/coordenador_users_screen.dart';
import 'package:roca/screens/homepage_screen.dart';
import 'package:roca/screens/nucleado_screens/nucleado_list_screen.dart';
import 'package:roca/screens/coordenador_screens/coordenador_lists_screen.dart';
import 'package:roca/screens/nucleado_screens/profile_screen.dart';
import 'package:roca/services/user_service.dart';


class RocaSidebar extends StatelessWidget{
  final bool isCoordenador;
  final userController = UserController(UserService());

  RocaSidebar({
    super.key,
    required this.isCoordenador,
  });

  void _logout (BuildContext context) async {
    final Map<String, dynamic> responseData = await userController.logout(DataManager.getLoggedUser()!.usuario);

    if (responseData["success"]) {
      Navigator.of(context).pushNamedAndRemoveUntil(HomepageScreen.routeName, (Route<dynamic> route) => false);
    }
    else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            responseData["message"], 
            style: Theme.of(context).textTheme.headlineSmall!.white,
            textAlign: TextAlign.center,
          ),
          duration: AppTheme.messageDuration,
          backgroundColor: AppTheme.danger,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: isCoordenador ? AppTheme.land : AppTheme.vanilla,
      child: 
      isCoordenador 
      ? Column(
        children: [
          Container(
            height: 200,
            padding: const EdgeInsets.only(top: 25, left:20),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [ 
                  Row(
                    children: [
                      Stack(
                        children: [
                          Container(
                            width: 60,
                            height: 60,
                            decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                              color: AppTheme.vanilla,
                            ),
                          ),
                          Positioned(
                            top: 5,
                            left: 5,
                            child: Image.asset(
                              'assets/img/logo.png',
                              width: 50, 
                              height: 50, 
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(width: 30), // Adjust spacing between logo and text
                      Text(
                        'Roça',
                        style: TextStyle(
                          color: isCoordenador ? AppTheme.vanilla : AppTheme.land,
                          fontSize: 24,
                        ),
                      ),
                    ],
                  ),
                ]
              ),
            )
          ),
          ListTile(
            leading: Icon(
              FontAwesomeIcons.appleWhole,
              color: AppTheme.vanilla,
            ),
            title: AutoSizeText(
              'PRODUTOS',
              maxLines: 2,
              minFontSize: 2,
              wrapWords: false,
              style: AppTheme.sidebarTextVanilla,
            ),
            onTap: () {Navigator.of(context).pushNamed(CoordenadorProductsScreen.routeName);},
          ),
          ListTile(
            leading: Icon(
              Icons.list_alt,
              color: AppTheme.vanilla ,
            ),
            title: AutoSizeText(
              'LISTAS',
              maxLines: 2,
              minFontSize: 2,
              wrapWords: false,
              style: AppTheme.sidebarTextVanilla,
            ),
            onTap: () {Navigator.of(context).pushNamed(CoordenadorListsScreen.routeName);},
          ),
          ListTile(
            leading: Icon(
              Icons.person,
              color: AppTheme.vanilla,
            ),
            title: AutoSizeText(
              'USUÁRIOS',
              maxLines: 2,
              minFontSize: 2,
              wrapWords: false,
              style: AppTheme.sidebarTextVanilla,
            ),
            onTap: () {Navigator.of(context).pushNamed(CoordenadorUsersScreen.routeName);},
          ),
          ListTile(
            leading: Icon(
              Icons.home,
              color: AppTheme.vanilla,
            ),
            title: AutoSizeText(
              'FAMÍLIAS',
              maxLines: 2,
              minFontSize: 2,
              wrapWords: false,
              style: AppTheme.sidebarTextVanilla,
            ),
            onTap: () {Navigator.of(context).pushNamed(CoordenadorFamiliesScreen.routeName);},
          ),
          ListTile(
            leading: Icon(
              Icons.park,
              color: AppTheme.vanilla,
            ),
            title: AutoSizeText(
              'ASSENTAMENTOS',
              maxLines: 2,
              minFontSize: 2,
              wrapWords: false,
              style: AppTheme.sidebarTextVanilla,
            ),
            onTap: () {Navigator.of(context).pushNamed(CoordenadorAssentamentosScreen.routeName);},
          ),
          ListTile(
            leading: Icon(
              Icons.swap_horiz,
              color: AppTheme.vanilla,
            ),
            title: AutoSizeText(
              'MUDAR DE PERFIL',
              maxLines: 2,
              minFontSize: 2,
              wrapWords: false,
              style: AppTheme.sidebarTextVanilla,
            ),
            onTap: () {Navigator.of(context).pushNamed(ChooseProfileScreen.routeName);},
          ),
          ListTile(
            leading: Icon(
              Icons.logout,
              color: AppTheme.vanilla,
            ),
            title: AutoSizeText(
              'SAIR',
              maxLines: 2,
              minFontSize: 2,
              wrapWords: false,
              style: AppTheme.sidebarTextVanilla,
            ),
            onTap: () {
              _logout(context);
            },
          ),
        ],
      )
      : Column(
        children: [
          Column(
            children: [
              Container(
                height: 200,
                padding: const EdgeInsets.only(top: 25, left:20),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [ 
                      Row(
                        children: [
                          Image.asset(
                            'assets/img/logo.png', // Replace with your logo asset path
                            width: 50, // Adjust the width as needed
                            height: 50, // Adjust the height as needed
                          ),
                          const SizedBox(width: 30), // Adjust spacing between logo and text
                          const Text(
                            'Roça',
                            style: TextStyle(
                              color: AppTheme.land,
                              fontSize: 24,
                            ),
                          ),
                        ],
                      ),
                    ]
                  ),
                )
              )
            ],
          ),
          ListTile(
            leading: const Icon(
              Icons.list_alt,
            ),
            title: AutoSizeText(
              'LISTAS',
              maxLines: 2,
              minFontSize: 2,
              wrapWords: false,
              style: AppTheme.sidebarTextLand,
            ),
            onTap: () {Navigator.of(context).pushNamed(NucleadoListsScreen.routeName);},
          ),
          ListTile(
            leading: const Icon(
              Icons.account_circle,
            ),
            title: AutoSizeText(
              'SEU PERFIL',
              maxLines: 2,
              minFontSize: 2,
              wrapWords: false,
              style: AppTheme.sidebarTextLand,
            ),
            onTap: () async {
              final result = await Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ProfileScreen(user: DataManager.getLoggedUser()!)),
              );

              if (result == true) {
                DataManager.setHasUnsavedChanges(false);
              }
            },
          ),
          if (DataManager.getLoggedUser()?.funcao?.codigo == "0")
            ListTile(
              leading: const Icon(
                Icons.swap_horiz,
              ),
              title: AutoSizeText(
                'MUDAR DE PERFIL',
                maxLines: 2,
                minFontSize: 2,
                wrapWords: false,
                style: AppTheme.sidebarTextLand,
              ),
              onTap: () {Navigator.of(context).pushNamed(ChooseProfileScreen.routeName);},
            ),
          ListTile(
            leading: const Icon(
              Icons.logout,
            ),
            title: AutoSizeText(
              'SAIR',
              maxLines: 2,
              minFontSize: 2,
              wrapWords: false,
              style: AppTheme.sidebarTextLand,
            ),
            onTap: () {
              _logout(context);
            },
          ),
        ],
      ),
    );
  }

}

import 'package:auto_size_text/auto_size_text.dart';
import 'package:roca/constants.dart';
import 'package:flutter/material.dart';
// import 'package:auto_size_text/auto_size_text.dart';


class RocaAppBar extends StatelessWidget implements PreferredSizeWidget{
  final String title;
  final Function()? onTap;
  final bool isCoordenador;
  final bool appBarLandTheme;
  final Widget? leadingIcon;
  

  const RocaAppBar({
    super.key, 
    required this.title,
    this.onTap,
    required this.isCoordenador,
    required this.appBarLandTheme,
    this.leadingIcon,
  });

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: appBarLandTheme ? AppTheme.land : AppTheme.vanilla,
      automaticallyImplyLeading: false,
      flexibleSpace: Container(
        alignment: Alignment.center,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: AppTheme.appbarPadding, vertical: AppTheme.appbarPadding),
          child:Row(
            children: [
              if (leadingIcon != null) 
                leadingIcon!,
              if (leadingIcon == null) const SizedBox(width: AppTheme.appbarIconSize + AppTheme.appbarPadding),
              Expanded(
                child: AutoSizeText(
                  title,
                  maxLines: 2,
                  wrapWords: false, 
                  minFontSize: 2,
                  style: appBarLandTheme
                      ? Theme.of(context).textTheme.headlineLarge?.copyWith(color: AppTheme.vanilla)
                      : Theme.of(context).textTheme.headlineLarge?.copyWith(color: AppTheme.land),
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(AppTheme.appbarHeight);
}

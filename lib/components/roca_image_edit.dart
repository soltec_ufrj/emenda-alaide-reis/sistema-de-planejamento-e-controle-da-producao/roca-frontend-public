import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

import 'package:roca/constants.dart';

class RocaImageEdit extends StatelessWidget {
  final String imageUrl;
  final bool imagemAlterada;
  final bool isEditable;
  final FileImage imagem;

  const RocaImageEdit(
      {super.key,
      this.imageUrl = "",
      this.isEditable = true,
      required this.imagemAlterada,
      required this.imagem});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 2,
      height: MediaQuery.of(context).size.width / 2,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: AspectRatio(
            aspectRatio: 1.0,
            child: CachedNetworkImage(
                key: UniqueKey(),
                cacheManager: CacheManager(
                  Config(
                    'cacheKey',
                    stalePeriod: const Duration(days: 1),
                  ),
                ),
                imageUrl: imageUrl,
                placeholder: (context, url) =>
                    const Center(child: CircularProgressIndicator()),
                errorWidget: (context, url, error) =>
                    imageContainer(AssetImage('assets/img/placeholder.jpg')),
                imageBuilder: (context, imageProvider) =>
                    imageContainer(imageProvider))
            ),
      ),
    );
  }

  Widget imageContainer(ImageProvider<Object> imageProvider) {
    return Container(
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        image: DecorationImage(
          image: imagemAlterada ? imagem as ImageProvider : imageProvider,
          fit: BoxFit.cover,
        ),
      ),
      child: isEditable
          ? Align(
              alignment: Alignment.bottomRight,
              child: Container(
                padding: const EdgeInsets.all(5.0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: Icon(
                  Icons.camera_alt,
                  color: AppTheme.land,
                  size: AppTheme.iconSize,
                ),
              ),
            )
          : Container(),
    );
  }
}

import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:roca/constants.dart';

class RocaDropdown extends FormField {
  final bool searchable;
  final String hintText;
  final dynamic options;
  final dynamic value;
  final String Function(dynamic)? getLabel;
  final void Function(dynamic)? onChanged;
  final bool showSelectedItems;
  final Widget Function(BuildContext, dynamic)? dropdownBuilder;
  final dynamic itemBuilder;

  RocaDropdown({
    super.key,
    this.searchable = false,
    required this.hintText,
    required this.options,
    this.getLabel,
    this.value,
    this.onChanged,
    this.showSelectedItems = true,
    this.dropdownBuilder,
    this.itemBuilder,
    super.validator,
  }) : super(
    builder: (FormFieldState state) {
      return ConstrainedBox(
        constraints: const BoxConstraints(
          maxWidth: AppTheme.inputMaxWidth,
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: AppTheme.inputVerticalPadding),
          child: DropdownSearch<dynamic>(
            decoratorProps: DropDownDecoratorProps(
              decoration: InputDecoration(
                filled: true,
                fillColor: AppTheme.inputFillColor,
                labelText: hintText,
                errorText: state.errorText,
                border: OutlineInputBorder(
                  borderRadius: AppTheme.inputBorderRadius,
                  borderSide: BorderSide(
                    color: state.errorText != null
                        ? Colors.red
                        : AppTheme.inputBorderColor,
                    width: 1.0,
                  ),
                ),
              ),
            ),
            popupProps: PopupProps.menu(
              showSearchBox: searchable,
              showSelectedItems: showSelectedItems,
              fit: FlexFit.loose,
              emptyBuilder: (context, searchEntry) => Center(child: Text('Busca não encontrada.', style:TextStyle(color: Colors.black))),
              itemBuilder: itemBuilder
            ),
            items: options,
            itemAsString: getLabel,
            compareFn: (item, selectedItem) {
              return item == selectedItem;
            },
            onChanged: onChanged != null
                ? (newValue) {
                  state.didChange(newValue);
                  onChanged(newValue);
                }
                : null,
            selectedItem: value,
            dropdownBuilder: dropdownBuilder,
          )
        ),
      );
    },
  );
}

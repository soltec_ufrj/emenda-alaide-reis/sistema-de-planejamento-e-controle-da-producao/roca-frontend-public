import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:roca/constants.dart';

class RocaGridItem extends StatefulWidget {
  const RocaGridItem({
    super.key,
    required this.thumbnail,
    required this.title,
    required this.subtitle,
    required this.color,
    required this.hasImage,
    required this.onlyVisible,
  });

  final String? thumbnail;
  final String title;
  final String subtitle;
  final String color;
  final bool hasImage;
  final bool onlyVisible;

  @override
  RocaGridItemState createState() => RocaGridItemState();

}


class RocaGridItemState extends State<RocaGridItem> with AutomaticKeepAliveClientMixin {



  @override
  Widget build(BuildContext context) {
    super.build(context);

    Color? backgroundColor;
    
    switch (widget.color) {
      case 'green':
        backgroundColor = AppTheme.aspargus;
        break;
      case 'orange':
        backgroundColor = AppTheme.sunrise;
        break;
      case 'gray':
        backgroundColor = AppTheme.silver;
        break;
      default:
        backgroundColor = Colors.transparent; // Handle other colors or no color
        break;
    }
    return Card(
        color: backgroundColor,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: LayoutBuilder(
          builder: (context, constraints) {
            final cardHeight = constraints.maxHeight;
            final imageHeight = cardHeight / 1.8;

            return  Stack(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    if (widget.hasImage)
                      ConstrainedBox(
                          constraints: BoxConstraints(
                            maxHeight: imageHeight,
                          ),
                          child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: widget.thumbnail != null
                          ? CachedNetworkImage(
                              key: UniqueKey(),
                              cacheManager: CacheManager(
                                Config(
                                  'cacheKey',
                                  stalePeriod: const Duration(days: 1),
                                ),
                              ),
                              imageUrl: widget.thumbnail!,
                              placeholder: (context, url) => const Center(
                                                                      child: CircularProgressIndicator()
                                                                    ),
                              errorWidget: (context, url, error) => Image.asset(
                                                                      'assets/img/placeholder.jpg',
                                                                      fit: BoxFit.cover,
                                                                    ),
                          )
                          : Image.asset(
                            'assets/img/placeholder.jpg',
                            fit: BoxFit.cover,
                          ),
                      ),
                    ),
                    if (widget.hasImage) const SizedBox(height: 8),
                    // Título
                    AutoSizeText(
                      widget.title,
                      maxLines: widget.hasImage ? 1 : 2,
                      minFontSize: 2,
                      wrapWords: false,
                      style: Theme.of(context).textTheme.headlineMedium!.white,
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                    ),
                    if (!widget.hasImage) 
                      const Divider(
                        thickness: 2,
                        color: AppTheme.land,
                      ),
                    AutoSizeText(
                      widget.subtitle,
                      maxLines: 1,
                      minFontSize: 2,
                      wrapWords: false,
                      style: Theme.of(context).textTheme.headlineSmall!.white,
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
                if ((widget.color == 'orange' && !widget.onlyVisible) || (widget.color != 'white' && widget.color != 'orange'))
                  // Edit Icon
                  Positioned(
                    bottom: 0,
                    right: 0,
                    child: widget.onlyVisible ? const Icon(Icons.visibility_outlined, color: AppTheme.land, size: 40) : const Icon(Icons.edit, color: AppTheme.land, size: 40),
                  ),
              ],
            );
          },
        ),
      ),
    );
  }
  
  @override
  bool get wantKeepAlive => true;
}


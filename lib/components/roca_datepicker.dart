import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:roca/constants.dart';

class RocaDatePicker extends StatefulWidget {
  final String label;
  final DateTime? initialDate;
  final void Function(DateTime)? onDateSelected;
  final bool? enabled;
  final String? Function(String?)? validator; 

  const RocaDatePicker({
    super.key, 
    required this.label, 
    this.initialDate, 
    this.onDateSelected,
    this.enabled,
    this.validator, 
  });

  @override
  State<StatefulWidget> createState() {
    return _RocaDatePickerState();
  }
}

class _RocaDatePickerState extends State<RocaDatePicker> {
  late TextEditingController _dateInputController;

  @override
  void initState() {
    super.initState();
    _dateInputController = TextEditingController();
    if (widget.initialDate != null) {
      _dateInputController.text = DateFormat('dd/MM/yy').format(widget.initialDate!);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(
        maxWidth: AppTheme.inputMaxWidth,
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: AppTheme.inputVerticalPadding),
        child: TextFormField(
          enabled: widget.enabled,
          controller: _dateInputController,
          decoration: InputDecoration(
            suffixIcon: const Icon(Icons.calendar_today),
            suffixIconConstraints: const BoxConstraints(minWidth: 40),
            filled: true,
            fillColor: AppTheme.inputFillColor,
            labelText: widget.label,
            border: const OutlineInputBorder(
              borderRadius: AppTheme.inputBorderRadius,
              borderSide: BorderSide(
                color: AppTheme.inputBorderColor,
                width: 1.0,
              ),
            ),
          ),
          readOnly: true,
          onTap: () async {
            DateTime? pickedDate = await showDatePicker(
              context: context,
              initialDate: widget.initialDate,
              firstDate: DateTime(2000),
              lastDate: DateTime(2101),
            );

            if (pickedDate != null) {
              String formattedDate = DateFormat('dd/MM/yy').format(pickedDate);
              _dateInputController.text = formattedDate;
              if (widget.onDateSelected != null) {
                widget.onDateSelected!(pickedDate);
              }
            }
          },
          validator: widget.validator,
        ),
      ),
    );
  }
}

import 'dart:convert';

import 'imagem_model.dart';


List<ProdutoModel> produtoModelFromJson(String str) =>
    List<ProdutoModel>.from(json.decode(str).map((x) => ProdutoModel.fromJson(x)));

String produtoModelToJson(ProdutoModel data) =>
    json.encode(data.toJson());

class ProdutoModel {
  ProdutoModel({
    this.id,
    required this.nome,
    required this.unidade,
    this.tipoProduto,
    this.imagem,
    this.imagemUrl = "",
  });

  String? id;
  String nome;
  UnidadeModel unidade;
  TipoProdutoModel? tipoProduto;
  ImagemModel? imagem;
  String imagemUrl;

  factory ProdutoModel.fromJson(Map<String, dynamic> json) =>
      ProdutoModel(
        id: json["id"],
        nome: json["nome"],
        unidade: UnidadeModel.fromJson(json["unidade"]),
        imagemUrl: json["imagem_url"] != null ? json["imagem_url"] : "",
        tipoProduto: json["tipo_produto"] != null ? TipoProdutoModel.fromJson(
            json["tipo_produto"]) : null,
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {

      "produto": {
        "id": id,
        "nome": nome,
      },
      "unidade": unidade.toJson(),
      "tipo_produto": tipoProduto?.toJson(),
    };

    if (imagem != null)
      data.addAll(imagem!.toJson());

    return data;
  }
}

class TipoProdutoModel {
  TipoProdutoModel({
    required this.codigo,
    required this.nome,
  });

  String codigo;
  String nome;


  factory TipoProdutoModel.fromJson(Map<String, dynamic> json) => TipoProdutoModel(
        codigo: json["codigo"],
        nome: json["nome"],

      );

  Map<String, dynamic> toJson() => {
    "codigo": codigo,
    "nome": nome,
  };
}

class UnidadeModel {
  UnidadeModel({
    required this.codigo,
    required this.nome,
  });

  String codigo;
  String nome;


  factory UnidadeModel.fromJson(Map<String, dynamic> json) => UnidadeModel(
        codigo: json["codigo"],
        nome: json["nome"],

      );

  Map<String, dynamic> toJson() => {
    "codigo": codigo,
    "nome": nome,
  };
}
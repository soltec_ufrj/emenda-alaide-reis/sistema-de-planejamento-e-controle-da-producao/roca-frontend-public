import 'dart:convert';

import 'package:roca/models/familia_model.dart';

List<PessoaModel> pessoaModelFromJson(String str) =>
    List<PessoaModel>.from(json.decode(str).map((x) => PessoaModel.fromJson(x)));

String pessoaModelToJson(PessoaModel data) =>
    json.encode(data.toJson());

class PessoaModel {
  PessoaModel({
    required this.usuario,
    this.funcao,
    required this.nome,
    required this.telefone,
    this.familia
  });

  UsuarioModel usuario;
  FuncaoModel? funcao;
  String nome;
  String telefone;
  FamiliaModel? familia;

  factory PessoaModel.fromJson(Map<String, dynamic> json) => PessoaModel(
        usuario: UsuarioModel.fromJson(json["usuario"]),
        funcao: FuncaoModel.fromJson(json["funcao"]),
        nome: json["nome"],
        telefone: json["telefone"],
        familia: json["familia"] != null ? FamiliaModel.fromJson(json["familia"]) : null,
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {
      "funcao": funcao?.toJson(),
      "pessoa": {
        "nome": nome,
        "telefone": telefone,
      },
    };

    data.addAll(usuario.toJson());
    if (familia != null)
      data.addAll(familia!.toJson());

    return data;
  }
}

List<FuncaoModel> funcaoModelFromJson(String str) =>
    List<FuncaoModel>.from(json.decode(str).map((x) => FuncaoModel.fromJson(x)));

String funcaoModelToJson(List<FuncaoModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FuncaoModel {
  FuncaoModel({
    required this.codigo,
    required this.nome,
  });

  String codigo;
  String nome;


  factory FuncaoModel.fromJson(Map<String, dynamic> json) => FuncaoModel(
        codigo: json["codigo"],
        nome: json["nome"],

      );

  Map<String, dynamic> toJson() => {
    "codigo": codigo,
    "nome": nome,
  };
}

List<UsuarioModel> usuarioModelFromJson(String str) =>
    List<UsuarioModel>.from(json.decode(str).map((x) => UsuarioModel.fromJson(x)));

String usuarioModelToJson(UsuarioModel data) =>
    json.encode(data.toJson());

class UsuarioModel {
  UsuarioModel({
    required this.cpf,
    this.senha,
  });

  String cpf;
  String? senha;


  factory UsuarioModel.fromJson(Map<String, dynamic> json) => UsuarioModel(
        cpf: json["cpf"],
        senha: json["senha"],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {
      "usuario": {
        "cpf": cpf,
        "senha": senha,
      }
    };

    return data;
  }
}
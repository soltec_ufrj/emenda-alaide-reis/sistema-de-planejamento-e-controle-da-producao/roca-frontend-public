import 'dart:convert';

import 'package:roca/models/familia_model.dart';
import 'package:roca/models/produto_model.dart';

List<ItemListaModel> itemListaModelFromJson(String str) =>
    List<ItemListaModel>.from(json.decode(str).map((x) => ItemListaModel.fromJson(x)));

String itemListaModelToJson(ItemListaModel data) =>
    json.encode(data.toJson());

class ItemListaModel {
  ItemListaModel({
    this.id,
    this.familia, // opcional por conta do getExportProdutos
    required this.listaId,
    required this.produto,
    required this.quantidade,
  });

  String? id;
  FamiliaModel? familia;
  String listaId;
  ProdutoModel produto;
  int quantidade;


  factory ItemListaModel.fromJson(Map<String, dynamic> json) => ItemListaModel(
    id: json["id"],
    produto: ProdutoModel.fromJson(json["produto"]),
    quantidade: json["quantidade_disponivel"],
    familia: json["familia"],
    listaId: json["listaId"],
  );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {
      "item_lista": {
        "id": id,
        "quantidade_disponivel": quantidade,
      },
      "lista": {
          "id": listaId,
      },
      "produto": produto.toJson()["produto"],
    };
    if (familia != null) {
      data.addAll(familia!.toJson());
    }

    return data;
  }
}


import 'dart:convert';

import 'package:roca/models/assentamento_model.dart';

List<FamiliaModel> familiaModelFromJson(String str) =>
    List<FamiliaModel>.from(json.decode(str).map((x) => FamiliaModel.fromJson(x)));

String familiaModelToJson(FamiliaModel data) =>
    json.encode(data.toJson());

class FamiliaModel {
  FamiliaModel({
    required this.sobrenome,
    this.numeroLote,
    this.endereco,
    this.assentamento,
  });

  String sobrenome;
  int? numeroLote;
  String? endereco;
  AssentamentoModel? assentamento;

  factory FamiliaModel.fromJson(Map<String, dynamic> json) => FamiliaModel(
        sobrenome: json["sobrenome"],
        numeroLote: json["numero_lote"],
        endereco: json["endereco"],
        assentamento: json["assentamento"] != null ? AssentamentoModel.fromJson(json["assentamento"]) : null,
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {
      "familia": {
        "sobrenome": sobrenome,
        "numero_lote": numeroLote,
        "endereco": endereco,
      },
    };

    if (assentamento != null) {
      data.addAll(assentamento!.toJson());
    }

    return data;
  }
}


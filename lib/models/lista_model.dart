import 'dart:convert';

List<ListaModel> listaModelFromJson(String str) =>
    List<ListaModel>.from(json.decode(str).map((x) => ListaModel.fromJson(x)));

String listaModelToJson(ListaModel data) =>
    json.encode(data.toJson());

class ListaModel {
  ListaModel({
    this.id,
    required this.nome,
    required this.dataInicio,
    required this.dataFim,
    required this.criador,
  });

  String? id;
  String nome;
  DateTime dataInicio;
  DateTime dataFim;
  String criador;

  factory ListaModel.fromJson(Map<String, dynamic> json) => ListaModel(
        id: json["id"],
        nome: json["nome"],
        dataInicio: DateTime.parse(json["data_inicio"]),
        dataFim: DateTime.parse(json["data_fim"]),
        criador: json["criador"]["usuario"]["cpf"],
      );

  Map<String, dynamic> toJson() => {
        "lista": {
          "id": id,
          "nome": nome,
          "data_inicio": dataInicio.toString(),
          "data_fim": dataFim.toString(),
        },
        "criador": criador,
      };

  bool isOpen () {
    DateTime today = DateTime.now();

    bool isOpen = dataFim.isAfter(today)
                  && 
                  (dataInicio.isBefore(today)
                    ||
                    dataInicio.isAtSameMomentAs(today)
                  );
    return isOpen;
  }
}


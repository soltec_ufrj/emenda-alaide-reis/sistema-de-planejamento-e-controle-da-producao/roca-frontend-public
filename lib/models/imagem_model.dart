import 'dart:convert';

String imagemModelToJson(ImagemModel data) => json.encode(data.toJson());

class ImagemModel {
  ImagemModel({
    required this.imagem,
    this.extensao,
    this.nomeArquivo,
  });

  String imagem;
  String? extensao;
  String? nomeArquivo;

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {
      "imagem": {
        "arquivo": imagem,
        "nomeArquivo": nomeArquivo! + "." + extensao!,
      },
    };

    return data;
  }
}

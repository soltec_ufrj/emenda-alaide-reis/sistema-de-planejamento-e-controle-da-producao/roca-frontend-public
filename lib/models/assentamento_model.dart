import 'dart:convert';

List<AssentamentoModel> assentamentoModelFromJson(String str) =>
    List<AssentamentoModel>.from(json.decode(str).map((x) => AssentamentoModel.fromJson(x)));

String assentamentoModelToJson(AssentamentoModel data) =>
    json.encode(data.toJson());

class AssentamentoModel {
  AssentamentoModel({
    required this.nome,
    this.descricao,
    this.cep,
  });

  String nome;
  String? descricao;
  CepModel? cep;


  factory AssentamentoModel.fromJson(Map<String, dynamic> json) => AssentamentoModel(
    nome: json["nome"],
    descricao: json["descricao"],
    cep: json["cep"] != null ? CepModel.fromJson(json["cep"]) : null,
  );

  Map<String, dynamic> toJson() => {
    "assentamento": {
      "nome": nome,
      "descricao": descricao,
    },
    "cep": cep?.toJson(),
  };
}

List<CepModel> cepModelFromJson(String str) =>
    List<CepModel>.from(json.decode(str).map((x) => CepModel.fromJson(x)));

String cepModelToJson(List<CepModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CepModel {
  CepModel({
    required this.id,
    this.estado,
    required this.cidade,
  });

  String id;
  EstadoModel? estado;
  String cidade;


  factory CepModel.fromJson(Map<String, dynamic> json) => CepModel(
        id: json["id"],
        estado: json["estado"] != null ? EstadoModel.fromJson(json["estado"]) : null,
        cidade: json["cidade"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "estado": estado?.toJson(),
        "cidade": cidade,
      };
}

List<EstadoModel> estadoModelFromJson(String str) =>
    List<EstadoModel>.from(json.decode(str).map((x) => EstadoModel.fromJson(x)));

String  estadoModelToJson(List<EstadoModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class EstadoModel {
  EstadoModel({
    required this.id,
    required this.nome,
    required this.sigla,
    required this.pais,
  });

  String id;
  String nome;
  String sigla;
  String pais;

  factory EstadoModel.fromJson(Map<String, dynamic> json) => EstadoModel(
    id: json["id"],
    nome: json["nome"],
    sigla: json["sigla"],
    pais: json["pais"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "nome": nome,
    "sigla": sigla,
    "pais": pais,
  };
}
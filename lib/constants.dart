import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

// const kPrimaryColor = Colors.orange;
// const kPrimaryLightColor = Colors.black12;

// class Configuration{
//   var configurl = '192.168.1.5'; //localhost ipv4 address
//   String authorization = "eyJhbGciOiJIUzI1NiJ9.eyJSb2xlIjoiQWRtaW4iLCJJc3N1ZXIiOiJJc3N1ZXIiLCJVc2VybmFtZSI6IkphdmFJblVzZSIsImV4cCI6MTY1NTAyNTA4NiwiaWF0IjoxNjU1MDI1MDg2fQ.-bVHN12jc3J5Rqe1abTCnH6IQHk0UvKKGsWfMMWSScM";
// }

// Define the extension on TextStyle
extension TextStyleExtension on TextStyle {
  TextStyle get white => copyWith(color: Colors.white);
  TextStyle get black => copyWith(color: Colors.black);
  TextStyle get land => copyWith(color: AppTheme.land);
  TextStyle get vanilla => copyWith(color: AppTheme.vanilla);
  TextStyle get danger => copyWith(color: AppTheme.danger);

  TextStyle withColor(Color color) => copyWith(color: color);
}

class ApiConstants {
  static String baseUrl = 'https://roca-backend.onrender.com/';
  // static String baseUrl = 'https://roca-backend-2cjk.onrender.com/';  // dev
  // static String baseUrl = 'http://10.0.2.2:3000/'; // localhost
  static String loginEndpoint = 'api/login/';
  static String logoutEndpoint = 'api/logout/';
  static String signUpEndpoint = 'api/usuario/signup/';

  static String userEndpoint = 'api/usuario/';
  static String changePasswordEndpoint = 'api/usuario/changepassword/';

  static String familiaEndpoint = 'api/familia/';
  static String listaEndpoint = 'api/lista/';
  static String produtoEndpoint = 'api/produto/';
  static String tipoProdutoEndpoint = 'api/tipo_produto/';
  static String unidadeEndpoint = 'api/unidade/';
  static String itemListaEndpoint = 'api/item_lista/';


  static String assentamentoEndpoint = 'api/assentamento/';
  static String cepEndpoint = 'api/cep/';
  static String estadoEndpoint = 'api/estado/';

  static String exportProdutos = 'api/produtos_lista/';

  static Duration timeoutDuration = Duration(seconds: 60);

  static String msgErroInesperado = 'Erro inesperado. Por favor, entre em contato com SOLTEC/UFRJ.';
  static String msgErroTimeout = 'Não foi possível conectar. Tente novamente.';
  static String msgErroSemInternet = 'Verifique sua conexão com a internet e tente novamente.';
}


class AppTheme {
  // appbar
  static const double appbarHeight = 100;
  static const double appbarPadding = 15;
  static const double appbarIconSize = 40;
  
  // masks
  static final phoneMask = MaskTextInputFormatter(mask: "(##) #####-####");
  static final cpfMask = MaskTextInputFormatter(mask: "###.###.###-##");

  // spaces
  static const double cardPadding = 28;
  static const double elementSpacing = cardPadding / 0.5;
  static const double iconSize = 40;
  static const double listItemSpacing = 8;
  static const double listItemImageSize = 100;

  // colors
  static const Color land = Color(0xff654833);
  static const Color vanilla = Color(0xffECE4B7);
  static const Color aspargus = Color(0xff7fb069);
  static const Color sunrise = Color(0xffe6aa68);
  static const Color silver = Color(0xffaeaeae);
  static const Color danger = Color(0xffd36135);


  // button
  static const double buttonHeight = 80;
  static const double buttonWidth = buttonHeight * 8;
  static const double buttonBorderRadius = 15;
  static const double buttonBorderWidth = 4.0;
  static const double buttonFontSize = 25;
  static const double buttonVerticalPadding = 15;


  // static const TextStyle buttonTextStyleOutline = TextStyle(fontSize: 25, color: Colors.white);
  // static const TextStyle buttonTextStyle = TextStyle(fontSize: 25, color: Colors.white);

  // inputs
  static const BorderRadius inputBorderRadius = BorderRadius.all(Radius.circular(10.0));
  static const double inputBorderWidth = 1;
  static const Color inputBorderColor = Colors.grey;
  static const Color inputFillColor = Colors.white;
  static const double inputMaxWidth = 800;
  static const double inputVerticalPadding = 10;
  
  static const Duration messageDuration = Duration(seconds: 4);


  static TextTheme textTheme = TextTheme(
    displayLarge: GoogleFonts.roboto(
        fontSize: 97, fontWeight: FontWeight.w300, letterSpacing: -1.5),
    displayMedium: GoogleFonts.roboto(
        fontSize: 61, fontWeight: FontWeight.w300, letterSpacing: -0.5),
    displaySmall: GoogleFonts.roboto(
        fontSize: 48, fontWeight: FontWeight.w400),
    headlineLarge: GoogleFonts.roboto(
        fontSize: 40, fontWeight: FontWeight.w400, letterSpacing: 0.25),
    headlineMedium: GoogleFonts.roboto(
        fontSize: 34, fontWeight: FontWeight.w400, letterSpacing: 0.25),
    headlineSmall: GoogleFonts.roboto(
      fontSize: 24, fontWeight: FontWeight.w400),
    titleLarge: GoogleFonts.roboto(
        fontSize: 20, fontWeight: FontWeight.w500, letterSpacing: 0.15),
    titleMedium: GoogleFonts.roboto(
        fontSize: 16, fontWeight: FontWeight.w400, letterSpacing: 0.15),
    titleSmall: GoogleFonts.roboto(
        fontSize: 14, fontWeight: FontWeight.w500, letterSpacing: 0.1),
    bodyLarge: GoogleFonts.roboto(
        fontSize: 16, fontWeight: FontWeight.w400, letterSpacing: 0.5),
    bodyMedium: GoogleFonts.roboto(
        fontSize: 14, fontWeight: FontWeight.w400, letterSpacing: 0.25),
    bodySmall: GoogleFonts.roboto(
        fontSize: 14, fontWeight: FontWeight.w500, letterSpacing: 1.25),
    labelLarge: GoogleFonts.roboto(
        fontSize: 12, fontWeight: FontWeight.w400, letterSpacing: 0.4),
    labelMedium: GoogleFonts.roboto(
        fontSize: 10, fontWeight: FontWeight.w400, letterSpacing: 1.5),
    labelSmall: GoogleFonts.roboto(
        fontSize: 8, fontWeight: FontWeight.w400, letterSpacing: 1.5),
  );

  // Sidebar tem estilo especifico de texto por conta do maior espacamento
  // entre as letras
  static final TextStyle sidebarTextVanilla = GoogleFonts.roboto(
    fontSize: 16, 
    fontWeight: FontWeight.w400, 
    letterSpacing: 7,
    color: AppTheme.vanilla,
  );
  static final TextStyle sidebarTextLand = GoogleFonts.roboto(
    fontSize: 16, 
    fontWeight: FontWeight.w400, 
    letterSpacing: 7,
    color: AppTheme.land,
  );

  static final ThemeData defaultTheme = ThemeData(
    textTheme: textTheme,
  );

  static TextStyle applyColorToTextStyle(TextStyle textStyle, Color color) {
    return textStyle.copyWith(color: color);
  }

}


//This font class will return TextStyle with color, weight, size with optional text-decoration
class Font{
  static apply(Color? color, FontStyle style, FontSize size, [TextDecoration? decoration]){
    return GoogleFonts.poppins(color: color, fontWeight: style.value, fontSize: size.value, decoration: decoration);
  }
}

//This enum is defined for text size in the form of headings
enum FontSize{
  h1, h2, h3, h4, h5, h6, h7, h8, h9, h10;
}

//This enum is defined for font-weight
enum FontStyle{
  semiBold, bold, extraBold, medium, regular
}

//This will return value of text size
extension FontSizes on FontSize{
  double get value{
    switch (this) {
      case FontSize.h1:
        return 96;
      case FontSize.h2:
        return 60;
      case FontSize.h3:
        return 48;
      case FontSize.h4:
        return 34;
      case FontSize.h5:
        return 24;
      case FontSize.h6:
        return 20;
      case FontSize.h7:
        return 16;
      case FontSize.h8:
        return 14;
      case FontSize.h9:
        return 12;
      case FontSize.h10:
        return 10;
    }
  }
}

//This will return style for text
extension FontStyles on FontStyle{
  FontWeight get value{
    switch (this) {
      case FontStyle.regular:
        return FontWeight.normal;

      case FontStyle.bold:
        return FontWeight.bold;

      case FontStyle.semiBold:
        return FontWeight.w700;

      case FontStyle.medium:
        return FontWeight.w600;

      case FontStyle.extraBold:
        return FontWeight.w900;
    }
  }
}

// Define a DeviceType enum
enum DeviceType { mobile, tablet, desktop }

// Function to determine device type
DeviceType getDeviceType(MediaQueryData mediaQuery) {
  final width = mediaQuery.size.width;

  if (width < 600) {
    return DeviceType.mobile;
  } else if (width < 1100) {
    return DeviceType.tablet;
  } else {
    return DeviceType.desktop;
  }
}
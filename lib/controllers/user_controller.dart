import 'package:roca/models/user_model.dart';
import 'package:roca/services/user_service.dart';

class UserController {
  final UserService _api;

  UserController(this._api);

  Future<Map<String, dynamic>> logout(UsuarioModel user) async {
    return _api.logout(user);
  }

  Future<Map<String, dynamic>> login(UsuarioModel usuario) async {
    return _api.login(usuario);
  }

  Future<Map<String, dynamic>> signUp (PessoaModel user) async {
    return _api.signUp(user);
  }

  Future<Map<String, dynamic>> getUsers () async {
    return _api.getUsers();
  }

  Future<Map<String, dynamic>> postUser (PessoaModel user) async {
    return _api.postUser(user);
  }

  Future<Map<String, dynamic>> patchUser (PessoaModel user) async {
    return _api.patchUser(user);
  }

  Future<Map<String, dynamic>> deleteUser (String userCPF) async {
    return _api.deleteUser(userCPF);
  }

  Future<Map<String, dynamic>> changePassword (UsuarioModel user) async {
    return _api.changePassword(user);
  }

}

import 'package:roca/models/lista_model.dart';
import 'package:roca/services/lista_service.dart';

class ListaController {
  final ListaService _api;

  ListaController(this._api);

  Future<Map<String, dynamic>> getListas () async {
    return _api.getListas();
  }

  Future<Map<String, dynamic>> postLista (ListaModel lista) async {
    return _api.postLista(lista);
  }

  Future<Map<String, dynamic>> patchLista (ListaModel lista) async {
    return _api.patchLista(lista);
  }

}

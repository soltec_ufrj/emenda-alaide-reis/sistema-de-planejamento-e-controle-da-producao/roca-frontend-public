import 'package:roca/models/item_lista_model.dart';
import 'package:roca/services/item_lista_service.dart';

class ItemListaController {
  final ItemListaService _api;

  ItemListaController(this._api);

  Future<Map<String, dynamic>> getItensListaFamilia (String listaID, String familiaSobrenome) async {
    return _api.getItensListaFamilia(listaID, familiaSobrenome);
  }

  Future<Map<String, dynamic>> getItensLista (String listaID) async {
    return _api.getItensLista(listaID);
  }

  Future<Map<String, dynamic>> postItemLista (ItemListaModel itemLista) async {
    return _api.postItemLista(itemLista);
  }

  Future<Map<String, dynamic>> patchItemLista (ItemListaModel itemLista) async {
    return _api.patchItemLista(itemLista);
  }

  Future<Map<String, dynamic>> deleteItemLista (String? itemListaID) async {
    return _api.deleteItemLista(itemListaID);
  }

  Future<Map<String, dynamic>> getExportProdutos (String listaID) async {
    return _api.getExportProdutos(listaID);
  }

  Future<Map<String, dynamic>> getExportFamilias (String listaID) async {
    return _api.getExportFamilias(listaID);
  }


}

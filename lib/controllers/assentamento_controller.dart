import 'package:roca/models/assentamento_model.dart';
import 'package:roca/services/assentamento_service.dart';

class AssentamentoController {
  final AssentamentoService _api;

  AssentamentoController(this._api);

  Future<Map<String, dynamic>> getAssentamentos () async {
    return _api.getAssentamentos();
  }

  Future<Map<String, dynamic>> postAssentamento (AssentamentoModel assentamento) async {
    return _api.postAssentamento(assentamento);
  }

  Future<Map<String, dynamic>> patchAssentamento (AssentamentoModel assentamento) async {
    return _api.patchAssentamento(assentamento);
  }

}

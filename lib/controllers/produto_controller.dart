import 'package:roca/models/produto_model.dart';
import 'package:roca/services/produto_service.dart';

class ProdutoController {
  final ProdutoService _api;

  ProdutoController(this._api);

  Future<Map<String, dynamic>> getTipos () async {
    return _api.getTipos();
  }

  Future<Map<String, dynamic>> getUnidades () async {
    return _api.getUnidades();
  }

  Future<Map<String, dynamic>> getProdutos () async {
    return _api.getProdutos();
  }

  Future<Map<String, dynamic>> postProduto (ProdutoModel produto) async {
    return _api.postProduto(produto);
  }

  Future<Map<String, dynamic>> patchProduto (ProdutoModel produto) async {
    return _api.patchProduto(produto);
  }

  Future<Map<String, dynamic>> deleteProduto (ProdutoModel produto) async {
    return _api.deleteProduto(produto);
  }

}

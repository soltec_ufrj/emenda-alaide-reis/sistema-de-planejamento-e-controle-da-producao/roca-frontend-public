import 'package:roca/models/familia_model.dart';
import 'package:roca/services/familia_service.dart';

class FamiliaController {
  final FamiliaService _api;

  FamiliaController(this._api);

  Future<Map<String, dynamic>> getFamilias () async {
    return _api.getFamilias();
  }

  Future<Map<String, dynamic>> postFamilia (FamiliaModel familia) async {
    return _api.postFamilia(familia);
  }

  Future<Map<String, dynamic>> patchFamilia (FamiliaModel familia) async {
    return _api.patchFamilia(familia);
  }

  Future<Map<String, dynamic>> deleteFamilia (String familiaSobrenome) async {
    return _api.deleteFamilia(familiaSobrenome);
  }
}

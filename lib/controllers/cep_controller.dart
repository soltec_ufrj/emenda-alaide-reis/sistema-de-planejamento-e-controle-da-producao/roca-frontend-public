import 'package:roca/models/assentamento_model.dart';
import 'package:roca/services/cep_service.dart';

class CepController {
  final CepService _api;

  
  CepController(this._api);


  Future<Map<String, dynamic>> getEstados () async {
    return _api.getEstados();
  }

  Future<Map<String, dynamic>> getCidadesPorEstado (String estado) async {
    return _api.getCidadesPorEstado(estado);
  }
}

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DataStorageManager {
  final _storage = const FlutterSecureStorage();

  Future<String?> getCPF() async {
    String? cpf = await _storage.read(key: "cpf");

    return cpf;
  }

  Future<void> setCPF(String cpf) async {
    await _storage.write(key: "cpf", value: cpf);
  }

  Future<String?> getSenha() async {
    return await _storage.read(key: "senha");
  }

  Future<void> setSenha(String senha) async {
    await _storage.write(key: "senha", value: senha);
  }

  Future<void> rememberMe(bool rememberMe) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("rememberMe", rememberMe);
  }

  Future<bool?> getRememberMe() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getBool("rememberMe");
  }

  Future<void> limparDados() async {
    await _storage.delete(key: "cpf");
    await _storage.delete(key: "senha");
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';

import './constants.dart';
import './app_data.dart';

import './screens/page_not_found_screen.dart';
import './screens/homepage_screen.dart';
import './screens/signup_screen.dart';
import './screens/login_screen.dart';
import './screens/choose_profile_screen.dart';
import './screens/nucleado_screens/nucleado_list_screen.dart';
import './screens/coordenador_screens/coordenador_products_screen.dart';
import './screens/coordenador_screens/coordenador_lists_screen.dart';
import './screens/coordenador_screens/coordenador_users_screen.dart';
import './screens/coordenador_screens/coordenador_families_screen.dart';
import './screens/coordenador_screens/coordenador_assentamentos_screen.dart';


void main() => runApp(const RocaApp());

class RocaApp extends StatelessWidget {
  const RocaApp({super.key});

  @override
  Widget build(BuildContext context) {
    final dataManager = DataManager();

    return MaterialApp(
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('pt', 'BR'),
      ],
      title: 'Roca',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        scaffoldBackgroundColor: const Color(0xffECE4B7),
        useMaterial3: true,
        textTheme: AppTheme.textTheme,
        primaryColor: AppTheme.land,
        colorScheme: ColorScheme(
          brightness: Brightness.light,
          primary: AppTheme.land,
          onPrimary: Colors.white,
          secondary: AppTheme.vanilla,
          onSecondary: Colors.black,
          error: AppTheme.danger,
          onError: Colors.black,
          surface: Colors.white,
          onSurface: Colors.black,
        )
      ),
      home: ChangeNotifierProvider.value(
        value: dataManager, // Passar a instância de DataManager
        child: const HomepageScreen(),
      ),
      routes: {
        SignupScreen.routeName: (_) => const SignupScreen(),
        LoginScreen.routeName: (_) => const LoginScreen(),
        ChooseProfileScreen.routeName: (_) => ChooseProfileScreen(),
        NucleadoListsScreen.routeName: (_) => const NucleadoListsScreen(),
        CoordenadorProductsScreen.routeName: (_) => const CoordenadorProductsScreen(),
        CoordenadorListsScreen.routeName: (_) => const CoordenadorListsScreen(),
        CoordenadorUsersScreen.routeName: (_) => const CoordenadorUsersScreen(),
        CoordenadorFamiliesScreen.routeName: (_) => const CoordenadorFamiliesScreen(),
        CoordenadorAssentamentosScreen.routeName: (_) => const CoordenadorAssentamentosScreen(),
      },
      onUnknownRoute: (settings) {  
        return MaterialPageRoute(builder: (_) => PageNotFound());
      },
    );
  }
}
